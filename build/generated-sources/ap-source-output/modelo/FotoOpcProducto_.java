package modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Foto;
import modelo.OpcionProducto;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-03T22:34:10")
@StaticMetamodel(FotoOpcProducto.class)
public class FotoOpcProducto_ { 

    public static volatile SingularAttribute<FotoOpcProducto, Boolean> visible;
    public static volatile SingularAttribute<FotoOpcProducto, Foto> foto;
    public static volatile SingularAttribute<FotoOpcProducto, OpcionProducto> opcionProducto;
    public static volatile SingularAttribute<FotoOpcProducto, Integer> id;

}