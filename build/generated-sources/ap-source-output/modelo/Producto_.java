package modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.OpcionProducto;
import modelo.RelProductoHash;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-03T22:34:10")
@StaticMetamodel(Producto.class)
public class Producto_ { 

    public static volatile SingularAttribute<Producto, Integer> id;
    public static volatile SingularAttribute<Producto, String> nombre;
    public static volatile ListAttribute<Producto, OpcionProducto> opcionesList;
    public static volatile ListAttribute<Producto, RelProductoHash> relshashtagsList;
    public static volatile SingularAttribute<Producto, Boolean> esPromo;

}