package modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-03T22:34:10")
@StaticMetamodel(Configuracion.class)
public class Configuracion_ { 

    public static volatile SingularAttribute<Configuracion, String> protocolo;
    public static volatile SingularAttribute<Configuracion, String> subCarpetaImagenes;
    public static volatile SingularAttribute<Configuracion, String> puerto;
    public static volatile SingularAttribute<Configuracion, String> ip;
    public static volatile SingularAttribute<Configuracion, String> nombreProyecto;
    public static volatile SingularAttribute<Configuracion, Integer> id;
    public static volatile SingularAttribute<Configuracion, String> nombre;
    public static volatile SingularAttribute<Configuracion, Boolean> enabled;
    public static volatile SingularAttribute<Configuracion, String> varSessionHTTP;

}