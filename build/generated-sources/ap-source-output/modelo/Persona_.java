package modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Direccion;
import modelo.Foto;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-03T22:34:10")
@StaticMetamodel(Persona.class)
public class Persona_ { 

    public static volatile SingularAttribute<Persona, Boolean> esReferido;
    public static volatile SingularAttribute<Persona, String> pass;
    public static volatile SingularAttribute<Persona, Boolean> esCliente;
    public static volatile SingularAttribute<Persona, Direccion> direccion;
    public static volatile SingularAttribute<Persona, String> codigoReferencial;
    public static volatile SingularAttribute<Persona, String> nombre;
    public static volatile SingularAttribute<Persona, Foto> foto;
    public static volatile SingularAttribute<Persona, String> apellido;
    public static volatile SingularAttribute<Persona, String> observaciones;
    public static volatile SingularAttribute<Persona, Boolean> esOperador;
    public static volatile SingularAttribute<Persona, Integer> id;
    public static volatile SingularAttribute<Persona, String> telefono;
    public static volatile SingularAttribute<Persona, String> email;

}