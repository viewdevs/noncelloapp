package modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.CambioEstadoPedido;
import modelo.DetallePedido;
import modelo.Direccion;
import modelo.Persona;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-03T22:34:09")
@StaticMetamodel(Pedido.class)
public class Pedido_ { 

    public static volatile SingularAttribute<Pedido, Double> precioCongelado;
    public static volatile SingularAttribute<Pedido, Date> fecha;
    public static volatile SingularAttribute<Pedido, Persona> cliente;
    public static volatile SingularAttribute<Pedido, Direccion> direccionEntrega;
    public static volatile ListAttribute<Pedido, CambioEstadoPedido> cambiosEstadoList;
    public static volatile SingularAttribute<Pedido, Persona> referido;
    public static volatile SingularAttribute<Pedido, Integer> id;
    public static volatile ListAttribute<Pedido, DetallePedido> detallesList;

}