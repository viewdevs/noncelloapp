package modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Hashtag;
import modelo.Producto;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-03T22:34:10")
@StaticMetamodel(RelProductoHash.class)
public class RelProductoHash_ { 

    public static volatile SingularAttribute<RelProductoHash, Integer> id;
    public static volatile SingularAttribute<RelProductoHash, Producto> producto;
    public static volatile SingularAttribute<RelProductoHash, Hashtag> hashtag;
    public static volatile SingularAttribute<RelProductoHash, Boolean> activo;

}