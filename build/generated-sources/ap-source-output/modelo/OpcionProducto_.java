package modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.FotoOpcProducto;
import modelo.Producto;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-03T22:34:10")
@StaticMetamodel(OpcionProducto.class)
public class OpcionProducto_ { 

    public static volatile SingularAttribute<OpcionProducto, String> descripcion;
    public static volatile SingularAttribute<OpcionProducto, Double> precio;
    public static volatile SingularAttribute<OpcionProducto, Producto> productoPadre;
    public static volatile SingularAttribute<OpcionProducto, Boolean> visible;
    public static volatile SingularAttribute<OpcionProducto, Integer> id;
    public static volatile SingularAttribute<OpcionProducto, String> subEncabezado;
    public static volatile SingularAttribute<OpcionProducto, String> nombre;
    public static volatile ListAttribute<OpcionProducto, FotoOpcProducto> fotosOpcProductosList;

}