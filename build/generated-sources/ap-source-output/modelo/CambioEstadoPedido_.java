package modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.EstadoPedidoDisponible;
import modelo.Pedido;
import modelo.Persona;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-03T22:34:10")
@StaticMetamodel(CambioEstadoPedido.class)
public class CambioEstadoPedido_ { 

    public static volatile SingularAttribute<CambioEstadoPedido, Date> cuando;
    public static volatile SingularAttribute<CambioEstadoPedido, Pedido> pedido;
    public static volatile SingularAttribute<CambioEstadoPedido, Integer> id;
    public static volatile SingularAttribute<CambioEstadoPedido, EstadoPedidoDisponible> estadoPedidoDisponible;
    public static volatile SingularAttribute<CambioEstadoPedido, Persona> operador;

}