<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Noncello - Catalogo de Productos</title>
        <%@include file="../comun/comun.jsp" %>
    </head>
    <body ng-app="app" ng-controller="productos" ng-init="init()" ng-cloak>
        
        <!-- 0 - HEADER COMUN: -->
        <%@include file="../comun/header-comun.jsp" %>
            
      
        <!-- 2 -LISTADO PRODUCTOS DE LA CATEGORIA: -->
        <%@include file="partes/listado-productos.jsp" %>
        
        
        <!-- 99 - FOOTER COMUN: -->
        <%@include file="../comun/footer-comun.jsp" %>
        
        
        <!-- MODAL DE PRODUCTOS:-->
        <%@include file="modal-productos.jsp" %>
        
            
    </body>
    <script>
    app = angular.module('app', []);
    
    <%@include file="../../funciones/limitToFilter.js" %>
    
    
    app.controller('productos', function($scope) 
    {
        
        <%@include file="../../funciones/Menu-fn.js" %>
        <%@include file="../../funciones/Hashtag-fn.js" %>
        <%@include file="../../funciones/Pedido-fn.js" %>
        <%@include file="../../funciones/Producto-fn.js" %>
        <%@include file="../../funciones/fn-comunes.js" %>
                
        $scope.quieroBarraLateral = true;
                
        
        $scope.init = function()
        {
            console.log("init");
            
            fkHashRecibido = $scope.dameParametroGet("fkHash");
            
            console.log("fkHashRecibido " + fkHashRecibido);
            if(fkHashRecibido != null)
            {
                $scope.getHashtag(fkHashRecibido);
                $scope.findProductosByHash(fkHashRecibido);
                $scope.fotoCollapsible = $scope.hashtag.fullFoto;
                $scope.h1Collapsible = $scope.hashtag.hash;
                $scope.h2Collapsible = $scope.hashtag.subTexto;
                
                
                $scope.findMenus();
            }
            $scope.abrirProducto = function(producto)
            {
                console.log("abriendo producto: " + JSON.stringify(producto));
                $scope.productoSeleccionado = producto;
                
                if($scope.productoSeleccionado.opcionesList != null)
                {
                    if($scope.productoSeleccionado.opcionesList.length > 0)
                    {
                        $scope.opcionProductoSeleccionada = $scope.productoSeleccionado.opcionesList[0];
                    }
                }
                
                
                $scope.$evalAsync();
                $("#modal-producto").modal();
                $("#combo-opciones").focus();
            }
            
            
            $scope.quieroBarraLateral = false;
//            $scope.findMenus();
            $scope.findHashtags();
            
            $scope.$evalAsync();
        }
        $scope.disminuirCantidadCuantosLlevoAux = function(opcionProductoSeleccionada)
        {
            console.log("diminuyo");
            if(opcionProductoSeleccionada.cantidadLlevo > 1)
            {
                opcionProductoSeleccionada.cantidadLlevo --;
            }
            $scope.$evalAsync();
        }
        $scope.aumentarCantidadCuantosLlevoAux = function(opcionProductoSeleccionada)
        {
            console.log("aumento");
            opcionProductoSeleccionada.cantidadLlevo ++;
            $scope.$evalAsync();
        }
        
    });
    </script>
</html>