 <div class="col-xs-12">
     
     <h3 class="h-marron" style='margin-top: 0px;'>Datos Cliente</h3>
        
        <!-- CONTENEDOR NOMBRE Y APELLIDO: -->
        <div class="contenedor-nombre-y-apellido col-xs-12 sin-padding">
            <div class="col-xs-6 sin-padding-left">
                <div class="form-group">
                    <label>Nombre</label>
                    <input type='text' id="input-nombre" class='form-control' ng-model='miCarrito.pedido.cliente.nombre' placeholder="Nombre..">
                </div>
            </div>
            <div class="col-xs-6 sin-padding-right">
                <div class="form-group">
                    <label>Apellido</label>
                    <input type='text' id="input-apellido" class='form-control' ng-model='miCarrito.pedido.cliente.apellido' placeholder='Apellido'>
                </div>
            </div>
        </div>
        
        <!-- EMAIL: -->
        <div class="contenedor-email col-xs-12 sin-padding">
            
            <div class="col-xs-6 sin-padding-left">
                <div class="form-group">
                    <label>Email</label>
                    <div class="input-group">
                        <span class="input-group-addon">@</span>
                        <input type='email' id="input-email" class='form-control' ng-model='miCarrito.pedido.cliente.email' placeholder="nombre@gmail.com">
                    </div>
                </div>
            </div>
            <div class="col-xs-6 sin-padding-right">
                <div class="form-group">
                    <label>Telefono</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-earphone"></span>
                        </span>
                        <input type='tel' id="input-telefono" class='form-control' ng-model='miCarrito.pedido.cliente.telefono' placeholder="294 154 530584">
                    </div>
                </div>
            </div> 
        </div>

<!--        <div class="form-group">
            <label>Fecha Pedido:</label>
            <h5>
                {{miCarrito.pedido.fechaBonita}}
            </h5>
        </div>-->

        <div class="form-group">
            <label>Codigo del Referido (Para Descuento)</label>
            <input type='number' min="1" max="9999" class='form-control' ng-model='miCarrito.pedido.codigoReferido' placeholder="4783">
<!--            <select class="form-control" ng-model="miPedido.referido"
                ng-options="referido as referido.nombre for referido in arrReferidos track by referido.nombre" >
            </select>-->
        </div>
     
    <!--<hr>-->
    <div class='h-marron' style="margin-bottom: 10px;">
        <span class='h-marron' ng-show='miCarrito.pedido.retiroSucursal'>
            Retiro en Sucursal
        </span>
        <span class='h-marron' ng-hide='miCarrito.pedido.retiroSucursal'>
            Entrega a Domicilio
        </span>
        <label class="switch" style="position: absolute; right: 30px;" ng-click="traerDireccionEmpty()">
            <input type="checkbox" ng-model='miCarrito.pedido.retiroSucursal'> 
            <span class="slider round"></span>
        </label>
    </div>
    
    
    
    <div class='form-group' style='text-align: right;margin-bottom: 0px'>
        
<!--        <label ng-show='retiroSucursal'>Retiro en Sucursal</label>
        <label ng-hide='retiroSucursal'>Entrega a Domicilio</label>-->
    </div>
    <!--<hr>-->
    <div class="col-xs-12 sin-padding" ng-hide='miCarrito.pedido.retiroSucursal'>
        
        <!--CALLE:-->
        <div class="col-xs-6 sin-padding-left">
            <div class="form-group ">
                <label>Calle</label>
                <input type="text" id="input-calle" ng-model="miCarrito.pedido.direccionEntrega.calle" class="form-control">
            </div>
        </div>
        
        <!-- NUMERO: -->
        <div class="col-xs-6 sin-padding-right">
            <div class="form-group ">
                <label>Numero</label>
                <input type="text" id="input-numeroCalle"ng-model="miCarrito.pedido.direccionEntrega.numero" class="form-control">
            </div>
        </div>
        
        <!--HOTEL: -->
        <div class="col-xs-12 sin-padding">
            <div class="form-group ">
                <label>Hotel (Opcional)</label>
                <input type="text" ng-model="miCarrito.pedido.direccionEntrega.hotel" class="form-control">
            </div>
        </div>
        
        <!-- PISO : -->
        <div class="col-xs-6 sin-padding-left">
            <div class="form-group ">
                <label>Piso (Opcional)</label>
                <input type="text" ng-model="miCarrito.pedido.direccionEntrega.piso" class="form-control">
            </div>
        </div>
        
        <!-- HABITACION: -->
        <div class="col-xs-6 sin-padding-right">
            <div class="form-group ">
                <label>Habitacion (Opcional)</label>
                <input type="text" ng-model="miCarrito.pedido.direccionEntrega.habitacion" class="form-control">
            </div>
        </div>
        
            <!--{{miPedido.direccionEntrega}}-->
    </div>
</div>
<style>
    .h-precio-total
    {
        border-radius: 50px;
        background-color: var(--marron);
        color: white;
        text-align: center;
        padding: 12px;
    }
    .h-marron
    {
        background-color: var(--sambayon);
        color: white;
        text-shadow: 2px 2px 2px black;
        text-align: center;
        padding: 12px;
        cursor: normal;
        font-size: 24px;
    }
</style>