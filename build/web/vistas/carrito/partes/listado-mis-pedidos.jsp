<div class="contenedor-mis-pedidos col-xs-12 sin-padding" ng-hide="cargandoCarrito">
    
    <!--<hr>-->
    <h3 class="h-marron">Mi carrito</h3>
    <!--<hr>-->
    
    <div class="item-mis-pedidos col-xs-12">
        <div class="item-detalles-mis-pedidos col-xs-12 sin-padding" ng-repeat="detalle in miCarrito.pedido.detallesList">
            <div class="col-xs-3">
                <div class="img-item-pedido center-block " style="background-image:url('{{detalle.opcionProducto.fotoPrincipal.foto.urlFull}}')" >
                    {{detalle.opcionProducto.fotoPrincipal.urlFull}}
                </div>
            </div>
            <div class='col-xs-6'>
                <h4>{{detalle.opcionProducto.nombre}}</h4>
                <span class="boton-minimal  glyphicon glyphicon-minus" ng-click="disminuirCantYAgregarMiCarrito(detalle)"></span><!-- g-click="disminuirCantidadCuantosLlevo(detalle)"-->
                <span style="padding: 4px;">{{detalle.cantidad}}</span>
               <span class="boton-minimal glyphicon glyphicon-plus" ng-click='aumentarCantYAgregarMiCarrito(detalle)'></span> <!-- ng-click="aumentarCantidadCuantosLlevo(detalle)"-->
                
                <button class="btn btn-default" ng-click="quitarDetalleAMiPedido(detalle.id)">
                    <span class="glyphicon glyphicon-trash"></span>
                </button>
            </div>
            <div class='precio-item-detalle col-xs-3'>
                {{"$" + (detalle.opcionProducto.precio * detalle.cantidad)}}
                <br>
                <h3 clasS="h-detalle-precio-item">{{"$" + detalle.opcionProducto.precio + " x " + detalle.cantidad }}</h3>
            </div>
            
        </div>
        
        <div class="item-cambio-estado-mis-pedidos" ng-repeat="cambioEstado in miPedido.cambiosEstadoList">
            <!--cambio estado{{cambioEstado}}<br><br>-->
        </div>
        
        <HR>
        <!--{{miPedido}}--> 
        <br>
    </div>
</div>

<!-- IMG LOADING:-->
<div class="col-xs-4 col-xs-offset-4" ng-show="cargandoCarrito">
    <img class="img-loading2 img img-responsive center-block col-xs-12 sin-padding"  src="http://viewdevscompany.com:8081/upload/noncello/backgrounds/loading.gif" >
    <h3 clasS="h">Cargando..</h3>
</div>


<style>
    .item-mis-pedidos
    {
    }
    .item-detalles-mis-pedidos
    {
        border: solid 1px lightgrey;
        margin-bottom: 12px;
        border-radius: 6px;
    }
    .item-cambio-estado-mis-pedidos
    {
        /*border: solid 1px green;*/
    }
    .img-item-pedido
    {
        width: 80px;
        height: 80px;
        background-position: center center;
        background-size: contain;
        background-repeat: no-repeat;
        
    }
    .boton-minimal
    {
        background-color: lightgrey;
        padding: 4px;
        border-radius: 5px;
    }
    .precio-item-detalle
    {
        background-color: var(--marron);
        color: white;
        padding: 12px;
        text-align: center;
        font-size: 24px;
        font-weight: bold;
    }
    .h-detalle-precio-item
    {
        background-color: var(--marron);
        color: white;
        text-align: center;
        font-size: 14px;
    }
</style>