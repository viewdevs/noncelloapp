<!-- MODAL : -->
<div id="modal-datos-molestos" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content col-xs-12 ">
            
            <!-- HEADER MODAL: -->
            <div class="modal-header row ">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Datos necesarios para finalizar la compra</h4>
            </div>
            
            <!-- CUERPO - MODAL: -->
            <div class="modal-body row sin-padding">
                <%@include file="partes/datos-carrito.jsp" %>
            </div>
            
            <!-- FOOTER - MODAL: -->
            <div class="modal-footer row">
                <div class="col-xs-6 sin-padding-left">
                    <button type="button" class="btn btn-default col-xs-12 sin-padding red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-ok rojo"></span>
                        Cancelar
                    </button>
                </div>
                <div class="col-xs-6 sin-padding-right">
                    <button type="button" class="btn btn-default col-xs-12 sin-padding green" ng-click='concretarLaVenta()'>
                        <span class="glyphicon glyphicon-ok verde"></span>
                        Confirmar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .green
    {
        color: green;
    }
    .red
    {
        color: red;
    }
</style>