<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Noncello - Mi pedido</title>
        <%@include file="../comun/comun.jsp" %>
    </head>
    <body ng-app="app" ng-controller="carrito" ng-init="init()" ng-cloak>
        
        <%@include file="../comun/header-comun.jsp" %>
        
            <!-- DATOS CARRITO: -->
            
        
            <!-- LISTADO MIS PEDIDOS: -->
            <%@include file="partes/listado-mis-pedidos.jsp" %>
            
            
            <div class="col-xs-12 ">
                <h3 class="h-precio-total" ng-click='finalizarCompra()'>
                    Finalizar Compra <br>{{ "$" + miCarrito.pedido.calcularTotal}} 
                </h3>
            </div>
        
        <%@include file="../comun/footer-comun.jsp" %>
        
        <!-- MODAL DATOS MOLESTOS: -->
        <%@include file="modal-datos-molestos.jsp" %>
    </body>
    <style>
        .error
        {
            border: solid 1px red;
        }
    </style>
    <script>
    app = angular.module('app', []);
    
    app.controller('carrito', function($scope) 
    {
        <%@include file="../../funciones/Menu-fn.js" %>
        <%@include file="../../funciones/Pedido-fn.js" %>
        <%@include file="../../funciones/Persona-fn.js" %>
        <%@include file="../../funciones/Direccion-fn.js" %>
        <%@include file="../../funciones/fn-comunes.js" %>
                
        $scope.quieroBarraLateral = true;
                
        $scope.retiroSucursal = true;
        $scope.init = function()
        {
            console.log("init");
            
            $scope.findMenus();
            $scope.findReferidos();
//            $scope.dameMisPedidos();
            $scope.h1Collapsible = "Mis Pedidos";
            $scope.h2Collapsible = "Te llevamos el pedido a donde estes!";
            
            $scope.$evalAsync();
        }
        $scope.disminuirCantYAgregarMiCarrito = function(detalle)
        {
            console.log("disminuirCantYAgregarMiCarrito");
            if(detalle.cantidad > 1)
            {
                detalle.cantidad --;
            }
            
            $scope.agregarOpcionProductoMiCarrito(detalle.opcionProducto , detalle.cantidad , false);
            
        }
        $scope.aumentarCantYAgregarMiCarrito = function(detalle)
        {
            console.log("aumentarCantYAgregarMiCarrito");
            detalle.cantidad ++;
            
            $scope.agregarOpcionProductoMiCarrito(detalle.opcionProducto , detalle.cantidad , false);
            
            $scope.$evalAsync();
        }
        $scope.finalizarCompra = function()
        {
            $scope.openModalDatosMoletos();
        }
        $scope.validarConcretarVenta = function()
        {
            console.log("validarConcretarVenta" + $scope.miCarrito.pedido.cliente.nombre);
            validacionTotal = false;
            
            // 1 - VALIDAR NOMBRE:
            nombreValido = false;
            if($scope.miCarrito.pedido.cliente.nombre != null)
            {
                if($scope.miCarrito.pedido.cliente.nombre.length > 2)
                {
                    nombreValido = true;
                }
            }
            // 2 - PINTAR ROJO
            if(!nombreValido)
            {
                $("#input-nombre").addClass('error');
            }
            else
            {
                $("#input-nombre").removeClass('error');
            }
            
            // 1 - VALIDAR APELLIDO:
            apellidoValido = false;
            if($scope.miCarrito.pedido.cliente.apellido != null)
            {
                if($scope.miCarrito.pedido.cliente.apellido.length > 2)
                {
                    apellidoValido = true;
                }
            }
            // 2 -  PINTAR APELLIDO:
            if(!apellidoValido)
            {
                $("#input-apellido").addClass('error');
            }
            else
            {
                $("#input-apellido").removeClass('error');
            }
            
            // 1 - VALIDAR EMAIL
            emailValido = false;
            if($scope.miCarrito.pedido.cliente.email != null)
            {
                if($scope.miCarrito.pedido.cliente.email.length > 2)
                {
                    emailValido = true;
                }
            }
            // 2 -  PINTAR APELLIDO:
            if(!emailValido)
            {
                $("#input-email").addClass('error');
            }
            else
            {
                $("#input-email").removeClass('error');
            }
            
            
            // 1 - VALIDAR TELEFONO
            telefonoValido = false;
            if($scope.miCarrito.pedido.cliente.telefono != null)
            {
                if($scope.miCarrito.pedido.cliente.telefono.length > 2)
                {
                    telefonoValido = true;
                }
            }
            // 2 -  PINTAR TELEFONO
            if(!telefonoValido)
            {
                $("#input-telefono").addClass('error');
            }
            else
            {
                $("#input-telefono").removeClass('error');
            }
            
            
            // 1 - VALIDAR CALLE SI NO ES RETIRO SUCURSAL:
            if($scope.miCarrito.pedido.retiroSucursal)
            {
                calleValida = true;
                numeroCalleValida = true;
            }
            else
            {
                // CALLE VALIDA :
                calleValida = false;
                if($scope.miCarrito.pedido.direccionEntrega.calle != null)
                {
                    if($scope.miCarrito.pedido.direccionEntrega.calle.length > 2)
                    {
                        calleValida = true;
                    }
                }
                // 2 -  PINTAR CALLE
                if(!calleValida)
                {
                    $("#input-calle").addClass('error');
                }
                else
                {
                    $("#input-calle").removeClass('error');
                }
                
                // NUMERO CALLE VALIDO:
                numeroCalleValida = false;
                if($scope.miCarrito.pedido.direccionEntrega.numero != null)
                {
                    if($scope.miCarrito.pedido.direccionEntrega.numero.length > 2)
                    {
                        numeroCalleValida = true;
                    }
                }
                // 2 -  PINTAR NUMERO CALLE
                if(!calleValida)
                {
                    $("#input-numeroCalle").addClass('error');
                }
                else
                {
                    $("#input-numeroCalle").removeClass('error');
                }
            }
            
            
            
            
            
            
            
            
            console.log("nombreValido:" + nombreValido);
            if(!$scope.miCarrito.pedido.retiroSucursal)
            {
                validacionTotal = nombreValido && apellidoValido && telefonoValido && emailValido && calleValida && numeroCalleValida;
            }
            else
            {
                validacionTotal = nombreValido && apellidoValido && telefonoValido && emailValido;
            }
            
            if(!validacionTotal)
            {
                alert("Debe llenar los campos minimos..");
            }
            
            return validacionTotal;
        }
        $scope.concretarLaVenta = function()
        {
            console.log("concretarLaVenta");
            
            if($scope.validarConcretarVenta())
            {
                $scope.miCarrito.pedido.fecha = null;
                $.ajax(
                {
                    url:"../../concretarLaVenta",
                    type: 'POST',
                    data:
                    {
                        "strPedidoFinal": JSON.stringify($scope.miCarrito.pedido),
                        "retiroSucursal": $scope.retiroSucursal
                    },
                    beforeSend: function (xhr) 
                    {
                        $scope.cargando = true;
                    },
                    success: function (resultado, textStatus, jqXHR) 
                    {
                        $scope.arr = resultado;
                        $scope.cargando = false;
                        $scope.$evalAsync();
                    }

                });
                
            }
            
        }
            
        $scope.openModalDatosMoletos = function()
        {
            $("#modal-datos-molestos").modal();
        }
 
        $scope.traerDireccionEmpty = function()
        {
            console.log("$scope.retiroSucursal:" + $scope.retiroSucursal);
//            if($scope.retiroSucursal == true)
//            {
//                $scope.retiroSucursal = false;
//            }
//            else
//            {
//                $scope.retiroSucursal = true;
//            }
            $scope.$evalAsync();
            $scope.getDireccionEmpty();
            
            $scope.miCarrito.pedido.direccionEntrega = $scope.direccionEmpty;
        }
//        $scope.disminuirCantidadCuantosLlevo = function(detalle)
//        {
//            console.log("diminuyo");
//            if(detalle.cantidad > 1)
//            {
//                detalle.cantidad --;
//            }
//            $scope.$evalAsync();
//        }
//        $scope.aumentarCantidadCuantosLlevo = function(detalle)
//        {
//            console.log("aumento");
//            detalle.cantidad ++;
//            
//            $scope.agregarDetalleAMiPedido(detalle.opcionProducto);
//            
//            $scope.$evalAsync();
//        }
    });
    </script>
</html>