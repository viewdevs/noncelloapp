<div class="listado-productos flex-container  col-xs-12 ">
    
    <!-- ITEM PROMO:-->
    <div class="separador-item-producto col-xs-12 " ng-repeat="producto in arrPromos" 
         ng-class="{'true':'sin-padding-left','false':'sin-padding-right'}[$index%2 == 0 && !xs]">
        
        <!--col-xs-12 col-sm-6 col-md-4 col-lg-3-->
        
        <div class="item-promo col-xs-12 sin-padding" ng-click="abrirPromo(producto)">
            
            <!-- FOTO GRANDE PRODUCTO: -->
            <div class="foto-producto-grande col-xs-12 sin-padding foto" style="background-image:url('{{producto.fotoPrincipal.foto.urlFull}}')">
                
                <!-- FOTO POSTA: -->
                <div class="foto-producto col-xs-12 foto" style="background-image:url('{{producto.fotoPrincipal.foto.urlFull}}')">
                </div>
                
                <!-- SOMBRA FOTO GRANDE: -->
                <div class="sombra-foto-producto-grande"></div>
            </div>
            
            <!-- CONTENEDOR HEADERS PRODUCTO: -->
            <div class="contenedor-headers-productos col-xs-12 "  >
                <h3 class='h-item-producto col-xs-12 sin-padding'>{{producto.nombre}}</h3>
                <h3 class='h-sub-item-producto col-xs-12 sin-padding'>{{producto.subEncabezado}}</h3>
            </div>
            
            
            <!-- FOOTER DEL PRODUCTO: -->
            <div class='footer-producto col-xs-12 sin-padding'>
                
                <div class="col-xs-12 col-sm-6 ">
                    <h3 class='h-precio-producto'>{{"$" + producto.minPrecio}}</h3>
                </div>
                <div class="col-xs-12 col-sm-6 ">
                    <button class='btn-mas'>
                        <span class='glyphicon glyphicon-plus'></span>
                        Ver mas
                    </button>
                </div>
            </div>
        </div>
    </div>

</div>
<style>
    .btn-mas
    {
        border-radius: 50px;
        /*background-color: #9DE0DE;*/
        background-color: var(--naranja);
        color: white;
        border: solid 0px transparent;
        position: absolute;
        right: 15px;
        top: 25px;
        font-family: 'Montserrat', sans-serif;
    }
    .listado-productos
    {
        padding-top: 25px;
        padding-bottom: 25px;
    }
    .flex-container 
    {
        display: flex;
        flex-wrap: wrap;
        align-items: center;
        justify-content: center;
    }
    .separador-item-producto
    {
        width: 265px;
        min-height: 350px;
        padding: 12px;
        
    }
    .sombra-foto-producto-grande
    {
        background-color: rgba(0,0,0,0.25);
        z-index: 2;
        height: 220px;
    }
    .item-promo
    {
        height: 380px;
        background-color: white;
        border: solid 1px lightgrey;
        cursor: pointer;
        border-radius: 6px;
        overflow:  hidden;
        margin-bottom: 25px;
    }
    .contenedor-headers-productos
    {
        background-color:  white;
    }
    .foto-producto-grande
    {
       height: 220px;
        overflow: hidden;
        /*border: solid 1px yellow;*/
        z-index: 1;
    }
    .foto-producto
    {
        height: 220px;
        /*border: solid 1px red;*/
        z-index: 3;
    }
    .h-item-producto
    {
        color: #34495e;
        text-align: left;
        font-family: 'Montserrat', sans-serif;
        /*font-family: 'Permanent Marker', cursive;*/
        font-size: 24px;
        /*border: solid 1px red;*/
        text-overflow: ellipsis;
        height: 40px;
        overflow: hidden;
        word-wrap: break-word;
        white-space: nowrap;
        overflow: hidden;
        margin-top: 25px;
    }
    .h-sub-item-producto
    {
        margin-top: 0px;
        color: grey;
        text-align: left;
        font-size: 14px;
        font-family: 'Montserrat', sans-serif;
        font-weight: 300;
        
        
    }
</style>
<style>
    .h-precio-producto
    {
        color:var(--verde5);
        text-align: left;
        font-family: 'Lato', sans-serif;
    }
</style>