<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Noncello - Mejores Promos</title>
        <%@include file="../comun/comun.jsp" %>
    </head>
    <body ng-app="app" ng-controller="promos" ng-init="init()" ng-cloak>
        
         <%@include file="../comun/header-comun.jsp" %>
            
            <%@include file="partes/listado-promos.jsp" %>
            
        <%@include file="../comun/footer-comun.jsp" %>
          
         <!-- MODAL DE PROMOS-->
         <%@include file="modal-promos.jsp" %>
        
            
    </body>
    <script>
    app = angular.module('app', []);
    
    <%@include file="../../funciones/limitToFilter.js" %>
    
    app.controller('promos', function($scope) 
    {
        <%@include file="../../funciones/Menu-fn.js" %>
        <%@include file="../../funciones/Producto-fn.js" %>
        <%@include file="../../funciones/fn-comunes.js" %>
                
        
        $scope.init = function()
        {
            console.log("init");
            
            $scope.findMenus();
            
            $scope.findMenus();
            $scope.findPromos();
            
            $scope.$evalAsync();
        }
        $scope.abrirPromo = function(producto)
        {
            console.log("abriendo promo " + JSON.stringify(producto));
            $scope.productoSeleccionado = producto;

            if($scope.productoSeleccionado.opcionesList != null)
            {
                if($scope.productoSeleccionado.opcionesList.length > 0)
                {
                    $scope.opcionProductoSeleccionada = $scope.productoSeleccionado.opcionesList[0];
                }
            }


            $scope.$evalAsync();
            $("#modal-promos").modal();
        }
            
            
    });    
    
    </script>
</html>