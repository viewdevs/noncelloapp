<div class="contenedor-menus-home col-xs-12 sin-padding">
    
    <div class='sepearador-item-menu' ng-repeat="menu in arrMenus"> 
        <div class="item-menu foto"  ng-click="irA(menu.url)" style="background-image:url('{{menu.fullImg}}')">
        </div>
        <h3 class='h'>
            {{menu.nombre}}
        </h3>
    </div>
</div>
<style>
    .contenedor-menus-home
    {
        display: flex;
        flex-wrap: wrap;
        /*justify-content: space-between;*/
        /*align-items: stretch;*/
        align-items: center;
        padding: 25px;
        align-items: center;
        justify-content: center;
        /*align-items: baseline;*/
    }
    .sepearador-item-menu
    {
        height: 270px;
        width: calc(50% - 20px);
        margin-top: 20px;
        border: solid 1px lightgrey;
        margin-right: 20px;
        border-radius: 6px;
        overflow: hidden;
    }
    .item-menu
    {
        height: 200px;
        width: 100%;
    }
</style>