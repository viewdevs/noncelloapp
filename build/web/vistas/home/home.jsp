<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="../comun/comun.jsp" %>
    </head>
    <body ng-app="app" ng-controller="home" ng-init="init()" ng-cloak >
        
        <!-- 0 - HEADER COMUN: -->
        <%@include file="../comun/header-comun.jsp" %>
       
            <!-- 1 - MENUS DE BRUNO : -->
            <%@include file="partes/menus2.jsp" %>
            <%--<%@include file="partes/menu-bruno.jsp" %>--%>
        
        <!-- 99 - FOOTER COMUN: -->
        <%@include file="../comun/footer-comun.jsp" %>
            
    </body>
    <script>
    app = angular.module('app', []);
    
    app.controller('home', function($scope) 
    {
        <%@include file="../../funciones/Menu-fn.js" %>
        <%@include file="../../funciones/fn-comunes.js" %>
        <%@include file="../../funciones/Pedido-fn.js" %>
       
    
       
        $scope.init = function()
        {
            console.log("init");

            $scope.findMenus();
            
            $scope.$evalAsync();
        }
        $scope.clickLupaBuscar = function()
        {
            $scope.quieroBuscar = true;
            $scope.$evalAsync();
        }
    });
    </script>
</html>