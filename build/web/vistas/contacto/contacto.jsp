<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Noncello - Contactanos</title>
        <%@include file="../comun/comun.jsp" %>
    </head>
    <body ng-app="app" ng-controller="home" ng-init="init()" ng-cloak>
        
        <%@include file="../comun/header-comun.jsp" %>
            
            <div class="col-xs-12">
                <h3>CONTACTO</h3>
            </div>
        
            <!-- FORMULARIO DE CONTACTO: -->
            <div class="col-xs-12">
                <%@include file="partes/formulario-contacto.jsp" %> 
            </div>
            
            <!-- MAPA: -->
            <div class="col-xs-6">
                <%--<%@include file="partes/mapa.jsp" %>--%>
            </div>
        
            
        <%@include file="../comun/footer-comun.jsp" %>
          
        
            
    </body>
    <script>
    app = angular.module('app', []);
    
    app.controller('home', function($scope) 
    {
        <%@include file="../../funciones/Menu-fn.js" %>
        <%@include file="../../funciones/fn-comunes.js" %>
        <%@include file="../../funciones/Pedido-fn.js" %>
                
        $scope.quieroBarraLateral = true;
                
        $scope.fotoCollapsible = 'http://viewdevscompany.com:8081/upload/noncello/backgrounds/map.png';
        $scope.h1Collapsible = "Como Llegar?";
//        $scope.h2Collapsible ="Como Llegar?";
//        $scope.h1Collapsible = "";
        $scope.h2Collapsible ="";
        $scope.linkCollapsible = "https://www.google.com/maps/place/Noncello+chocolates/@-41.1353446,-71.3105926,17z/data=!4m5!3m4!1s0x0:0xe386436d59410d90!8m2!3d-41.1349827!4d-71.3074334?hl=es";
        $scope.init = function()
        {
            console.log("init");
            
            $scope.findMenus();
            
            $scope.$evalAsync();
        }
    });
    </script>
</html>