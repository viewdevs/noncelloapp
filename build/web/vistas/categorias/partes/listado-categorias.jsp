<div class="listado-hash col-xs-12 ">
    
    <!-- ITEM HASHS:-->
    <div class="item-hash col-xs-12 sin-padding" ng-repeat="hash in arrHashtags"
         ng-click="abrirCategoriaDeProductos(hash.id)"
         style="background-image:url('{{hash.fullFoto}}')">
        <div class='sombra-item-hash col-xs-12 sin-padding'>
            <h3 class='h-item-hash col-xs-12'>{{hash.hash}}</h3>
            <h3 class='h-sub-item-hash'>{{hash.subTexto}}</h3>
        </div>
    </div>

</div>
<style>
    .listado-hash
    {
        padding-bottom: 50px;
    }
    .item-hash
    {
        min-height: 250px;
        background-size: cover;
        
        background-position:  center center;
        background-repeat: no-repeat;
        margin-top: 25px;
        cursor: pointer;
    }
    .sombra-item-hash
    {
        background-color: rgba(0,0,0,0.30);
        min-height: 250px;
    }
    .h-item-hash
    {
        margin-top: 50px;
        color: white;
        text-align: center;
        font-family: 'Permanent Marker', cursive;
        font-size: 48px;
        margin-top: 85px;
    }
    .h-sub-item-hash
    {
        margin-top: 25px;
        color: white;
        text-align: center;
        font-size: 24px;
        font-family: 'Permanent Marker', cursive;
    }
</style>