<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Noncello - Catalogo de Productos</title>
        <%@include file="../comun/comun.jsp" %>
    </head>
    <body ng-app="app" ng-controller="categorias" ng-init="init()" ng-cloak>
        
        <!-- 0 - HEADER COMUN: -->
        <%@include file="../comun/header-comun.jsp" %>
        
            <!-- 1 - LISTADO DE CATEGORIAS: -->
            <%@include file="partes/listado-categorias.jsp" %>
        
        <!-- 99 - FOOTER COMUN: -->
        <%@include file="../comun/footer-comun.jsp" %>
            
    </body>
    <script>
    app = angular.module('app', []);
    
    app.controller('categorias', function($scope) 
    {
        <%@include file="../../funciones/fn-comunes.js" %>
        <%@include file="../../funciones/Menu-fn.js" %>
        <%@include file="../../funciones/Hashtag-fn.js" %>
        <%@include file="../../funciones/Pedido-fn.js" %>
                
        $scope.quieroBarraLateral = true;
                
        
        $scope.init = function()
        {
            console.log("init");
            
            $scope.h1Collapsible = "Nuestras Categorias";
            $scope.h2Collapsible = "de Productos";
            $scope.quieroBarraLateral = false;
            $scope.findMenus();
            $scope.findHashtagsActivos();
            
            $scope.$evalAsync();
        }
    });
    </script>
</html>