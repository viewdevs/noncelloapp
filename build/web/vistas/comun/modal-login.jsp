<!-- MODAL : -->
<div id="modal-login" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content col-xs-12">
            
            <!-- HEADER MODAL: -->
            <div class="modal-header row">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Ingresar</h4>
            </div>
            
            <!-- CUERPO - MODAL: -->
            <div class="modal-body row">
                <div class="form-group" ng-hide="cargandoLogin">
                    <label>Email</label>
                    <input type="email" ng-model="emailLogin" class="form-control" placeholder="usuario@email.com">
                </div>
                <div class="form-group"  ng-hide="cargandoLogin">
                    <label>Clave</label>
                    <input type="password" ng-model="passLogin" class="form-control" >
                </div>
                
                <!--IMG LOADING:-->
                <div class=" col-xs-12 sin-padding" ng-show="cargandoLogin">
                    <div class="foto img-loading img img" style="background-image: url('http://barivende.com:8081/upload/noncello/backgrounds/loading.gif')">
                    </div>
                </div>
                
                <!--OLVIDE LA PASS-->
                <div class="col-xs-12 sin-padding">
                    <a href="" class="center-block">Olvid� mi clave</a>
                    <a href="" class="center-block" ng-click="abrirModalAltaCliente()">Soy Nuevo</a>
                </div>
            </div>
            
            <!-- FOOTER - MODAL: -->
            <div class="modal-footer col-xs-12 sin-padding">
                
                <button type="button" class="btn-confirmar-modal btns-modal  col-xs-12 sin-padding" ng-click="logearPersona()" ng-disabled="cargandoLogin">
                    <span class="glyphicon glyphicon-ok verde"></span>
                    Ingresar
                </button>
                
                <button type="button" class="btn-rechazar-modal btns-modal col-xs-12 sin-padding" data-dismiss="modal">
                    <span class="glyphicon glyphicon-remove rojo"></span>
                    Cancelar
                </button>

                
            </div>
        </div>
    </div>
</div>
<style>
    .img-loading
    {
        width: 100%;
        height: 250px;
    }
</style>