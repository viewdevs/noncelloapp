<div class="barra-lateral col-xs-12 sin-padding" ng-show="quieroBarraLateral"> <!---->
    
    <div class="cuestiones-usuario-barra-lateral col-xs-12 sin-padding" style="background-color:var(--fondo2)">
        
        <!-- BANER BARRA LATERAL:-->
        <div class="banner-barra-lateral col-xs-12 sin-padding">
           
            <!-- IMG USUARIO LOGEADO:-->
            <div class="img-usr-logeado center-block img-circle" style="background-image: url('{{miCarrito.pedido.cliente.foto.urlFull}}')">
            </div>

            <!--NOMBRE Y APELLIDO PERSONA LOGEADA:-->
            <h3 class='h-nombre-usr-logeado' ng-hide="personaLogeada.id == -1">

                
                {{codigoSesion}}
                <br>
                {{miCarrito.pedido.cliente.nombre}}
                {{miCarrito.pedido.cliente.apellido}}
            {{personaLogeada.nombre}} 
            {{personaLogeada.apellido}} 
            </h3>

            <!--CONTENEDOR BOTONES:-->
            <div class='contenedor-botones-login col-xs-12' style='text-align: center'>

                <!--BTN INGRESAR-->
                <button class="btn-ingresar-barra-lateral btn btn-default " ng-show="personaLogeada.id == -1" ng-click="abrirModalLogin()">
                    <span class="glyphicon glyphicon-user"></span>
                    Ingresar
                </button>

                <!--BTN SOY NUEVO-->
                <button class="btn-ingresar-barra-lateral btn btn-default " ng-show="personaLogeada.id == -1" ng-click="abrirModalAltaCliente()">
                    <span class="glyphicon glyphicon-asterisk"></span>
                    Soy Nuevo
                </button>
            </div>

            <!--EMAIL LOGEADO:-->
            <h5 class='h-email-usr-logeado'>{{personaLogeada.email}} </h5>

            <!--BOTON EXIT-->
            <button class='btn btn-default' ng-click="exit()"  ng-hide="personaLogeada.id == -1" style='position: fixed; top: 60px; left: 250px;'>
                <span class='glyphicon glyphicon-off'></span>
            </button>
        </div>
    
    
        <!--<hr class="hr-barra-lateral col-xs-12 sin-padding">-->
        
        <!-- SEPARADOR CARRITO BARRA LATERAL:-->
        <div class='barra-carrito-barra-lateral col-xs-12 sin-padding' ng-click='clickCarrito()' style="background-color: var(--sambayon)">
            <h3 class='h-banner-mi-carrito' >Mi Carrito</h3>
            <h3 class='h-precio-carrito-bl' >
                <div >
                    {{"$" + miCarrito.pedido.calcularTotal}}
                </div>
            </h3>
        </div>
    </div>
    
    <!-- MENUS BARRA LATERAL: -->
    <div class='contenedor-menus-lateral col-xs-12 sin-padding'>
        
        <div class='item-menu-lateral' ng-repeat='menu in arrMenus' ng-click='irA(menu.url)'  ng-class="{'true':'item-menu-lateral-seleccionado'}[menu.selected]" >
            <img class="img-menu-lateral" ng-src='{{menu.fullIcono}}' >
            {{menu.nombre}} 
            <!--- {{menu.selected}}-->
        </div>
    </div>
</div>

<div class="wpp-follower">
    <img class="img-wpp" 
         ng-click="irNew('https://api.whatsapp.com/send?phone=5492944934030')"
         src="http://viewdevscompany.com:8081/upload/noncello/wpp-01.png">
    <!--ng-click="irNew('https://api.whatsapp.com/send?phone=91XXXXXXXXXX&text=http://www.noncello.com.ar')"-->
</div>
<style>
    .wpp-follower
    {
        position: fixed;
        /*background-color: red;*/
        bottom : 50px;
        right: 50px;
        width: 70px;
        height: 70px;
    }
    .img-wpp
    {
        right: 100%;
        width: 100%;
        cursor: pointer;
    }
</style>
<style>
    .barra-lateral
    {
        position: fixed;
        background-color: white;
        top: 50px;
        height: calc(100% - 50px);
        width: 300px;
        z-index: 2;
        border: solid 1px lightgrey;
        
/*        box-shadow: 1px 1px 25px black;
        z-index: 1;*/
        /*border: solid 1px blue;*/
    }
    .cuestiones-usuario-barra-lateral
    {
    }
    .banner-barra-lateral
    {
        width: 100%;
        height: 170px;
        background-color: var(--fondo2);
    }
    .img-usr-logeado
    {
        height: 80px;
        width: 80px;
        margin-top: 25px;
        background-position: center center;
        background-repeat: no-repeat;
        background-size: contain;
        
    }
    
    .h-nombre-usr-logeado
    {
        color: white;
        text-align: center;
        margin-top: 5px;
        text-shadow: 1px 1px 1px black;
        
    }
    .h-email-usr-logeado
    {
        color: white;
        text-align: center;
        margin-top: 0px;
        text-shadow: 1px 1px 1px black;
    }
    .barra-carrito-barra-lateral
    {
        background-color: var(--marron);
        cursor: pointer;
    }
    .h-precio-carrito-bl
    {
        margin-top: 0px;
        text-align: center;
/*        color: #1abc9c;*/
        color: white;
        padding: 12px;
        margin-bottom: 0px;
        background-color: var(--marron);
        text-shadow: 1px 1px 1px black;
    }
    .h-banner-mi-carrito
    {
        text-align: center;
        color: white;
        font-size: 24px;
        margin-top: 12px;
        margin-bottom: 0px;
        padding-bottom: 5px;
        padding-top: 12px;
        background-color: var(--marron);
        text-shadow: 1px 1px 1px black;
    }
    .item-menu-lateral
    {
        padding: 8px;
        border-bottom: solid 1px lightgrey;
        cursor: pointer;
    }
    .item-menu-lateral-seleccionado
    {
        background-color: var(--fondo2);
        color: white;
    }
    .img-menu-lateral
    {
        height: 50px;
        width: 50px;
        margin-right: 12px;
    }
    .btn-ingresar-barra-lateral
    {
        margin-top: 15px;
    }
    .contenedor-menus-lateral
    {
        
    }
    .hr-barra-lateral
    {
        background-color: var(--fondo2);
        width: 33%;
        left: 33%;
        margin-bottom: 10px;
        margin-top: 10px;
    }
</style>