<meta name="viewport" content="width=device-width, user-scalable=no">
<meta name="theme-color" content="#5e2f0f">
<!--<meta name="theme-color" content="#2c3e50">-->

<!-- FAVICON : -->
<link rel="icon" href="http://viewdevscompany.com:8081/upload/noncello/backgrounds/favicon.png" />

<!-- CDN REMOTO: -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>

<!-- FONTS:-->
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<link rel="stylesheet" href='../res/fonts/fuentes.css'>
<link rel="stylesheet" href='../res/frameworks/animate.css'>
<link rel="stylesheet" href='../res/frameworks/snackbar.css'>
<link href="https://fonts.googleapis.com/css?family=Raleway:400,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
<!--font-family: 'Lato', sans-serif;-->

<link href="https://fonts.googleapis.com/css?family=Permanent+Marker&display=swap" rel="stylesheet"><!--font-family: 'Permanent Marker', cursive;-->
<link href="https://fonts.googleapis.com/css?family=Permanent+Marker&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,300,300i,400&display=swap" rel="stylesheet">
<!--font-family: 'Montserrat', sans-serif;-->
<link href="https://fonts.googleapis.com/css?family=Numans&display=swap" rel="stylesheet"> 
<!--font-family: 'Numans', sans-serif;-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
<!--font-family: 'Open Sans', sans-serif;-->
<!--@font-face 
{
  font-family: misses;
  src: url(../res/fonts/Misses.otf);
}-->

<link rel="stylesheet" href="../res/colores/colores.css">

<style>
    *
    {
         font-family: 'Lato', sans-serif;
        /*font-family: 'Permanent Marker', cursive;*/
    }
    .h
    {
        text-align: center;
        font-size: 20px;
        font-family: 'Montserrat', sans-serif;
    }
    .head
    {
        font-weight: bold;
        text-align: center;
        font-family: 'Montserrat', sans-serif;
    }
    .btn-bold
    {
        font-weight: bold;
    }
    .sin-padding
    {
        padding-left:0px;
        padding-right: 0px;
    }
    .sin-padding-left
    {
        padding-left:0px;
    }
    .sin-padding-right
    {
        padding-right: 0px;
    }
    .transparencia
    {
        background-color: rgba(0,0,0,0.25);
        margin-top:0px;
        margin-left: 0px;
        margin-right: 0px;
        position: absolute;
        top:0px;
        left: 0px;
        right: 0px;
        bottom: 0px;
        height: 100%;
        width: 100%;
    }
    .btn-redondito
    {
        border-radius: 25px;
    }
    .izq
    {
        text-align: left;
    }
    .der
    {
        text-align: right;
    }
    .foto
    {
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center center;
    }
    .foto-contain
    {
        background-size: contain;
        background-repeat: no-repeat;
        background-position: center center;
    }
    .foto-contain-top
    {
        background-size: contain;
        background-repeat: no-repeat;
        background-position: top center;
    }
    .h-separador
    {
        /*background-color: var(--primario);*/
        background-color: #f1c40f;
        color: white;
        margin: 0px;
        font-size: 24px;
        font-family: 'Permanent Marker', cursive;
        padding: 20px;
        margin-bottom: 35px;
        font-size: 24px;
    }
</style>
<style>
    .btns-modal
    {
        color: white;
        text-align: center;
        font-size: 24px;
        padding: 12px;
        border: solid 0px transparent;
        font-family: 'Open Sans', sans-serif;
    }
    .btn-confirmar-modal
    {
        background-color: var(--verde);
        margin-bottom:12px;
    }
    .btn-rechazar-modal
    {
        background-color: lightgrey;
    }
    .btn-ver-mas
    {
        font-weight: normal;
        font-family: 'Open Sans', sans-serif;
        text-align: center;
    }
</style>