<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Noncello - Nosotros</title>
        <%@include file="../comun/comun.jsp" %>
    </head>
    <body ng-app="app" ng-controller="referidos" ng-init="init()" ng-cloak>
        
        <%@include file="../comun/header-comun.jsp" %>
            
            <!-- EMPRESA: -->
            <%@include file="partes/empresa.jsp" %>
            
            <!-- CHOCOLATES: -->
            <%@include file="partes/chocolates.jsp" %>
            
            <!-- PACKAGING -->
            <%@include file="partes/packaging.jsp" %>
            
            <!-- REPOSTERIA -->
            <%@include file="partes/reposteria.jsp" %>
            
            <!-- HELADERIA -->
            <%@include file="partes/heladeria.jsp" %>
            
        <%@include file="../comun/footer-comun.jsp" %>
          
        
            
    </body>
    <style>
        *
        {
            font-family: 'Montserrat', sans-serif;
            
        }
        h5
        {
            font-weight: bold;
        }
    </style>
    <script>
    app = angular.module('app', []);
    
    app.controller('referidos', function($scope) 
    {
        <%@include file="../../funciones/Menu-fn.js" %>
        <%@include file="../../funciones/fn-comunes.js" %>
        <%@include file="../../funciones/Pedido-fn.js" %>
                
        $scope.quieroBarraLateral = true;
                
        
        $scope.init = function()
        {
            console.log("init");
            
            $scope.findMenus();
//            $scope.urlBG = $scope.dameBg('brownie.jpg');
//            $scope.urlLogo = $scope.dameBg('logo-noncello.png');
//            $scope.usrLogeado = $scope.dameBg('user.png');
            
            $scope.$evalAsync();
        }
    });
    </script>
</html>