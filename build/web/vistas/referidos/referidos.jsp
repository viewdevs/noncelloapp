<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Noncello - Listado de Referidos</title>
        <%@include file="../comun/comun.jsp" %>
    </head>
    <body ng-app="app" ng-controller="referidos" ng-init="init()" ng-cloak>
        
        <%@include file="../comun/header-comun.jsp" %>

            <!--LISTADO DE REFERIDOS:-->
            <%@include file="partes/listado-referidos.jsp" %>
            
        <%@include file="../comun/footer-comun.jsp" %>
          
        
            
    </body>
    <script>
    app = angular.module('app', []);
    
    app.controller('referidos', function($scope) 
    {
        <%@include file="../../funciones/Menu-fn.js" %>
        <%@include file="../../funciones/Persona-fn.js" %>
        <%@include file="../../funciones/fn-comunes.js" %>
            <%@include file="../../funciones/Pedido-fn.js" %>
                
        $scope.quieroBarraLateral = true;
                
        $scope.h1Collapsible = "REFERIDOS";
        $scope.h2Collapsible = "Listado";
        
        $scope.init = function()
        {
            console.log("init");
            
            $scope.findMenus();
            $scope.findReferidos();
            
            $scope.$evalAsync();
        }
    });
    </script>
</html>