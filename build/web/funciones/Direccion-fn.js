$scope.arrDireccions = [];
$scope.findDireccions = function()
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../findDireccions",
        data:
        {

        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.arrDireccions = resultado;
            $scope.cargando = false;
            $scope.$evalAsync();
        }
    });
}


//AJAX SAVE
$scope.guardarDireccion = function()
{
    guardo = false;
    $scope.cargando = false;
    
    $.ajax(
    {
        url:"../../guardarDireccion",
        async: false,
        data:
        {
            "strDireccion": JSON.stringify($scope.direccion)
        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            console.log("res WS -> guardarDireccion : " + resultado);
            $scope.cargando = false;
            guardo = resultado;
            
            if(guardo)
            {
                $scope.snack("Guardado con exito!");
                $scope.findDireccions();
            }
            $scope.$evalAsync();
        }

    });
    
    return guardo;
}
//AJAX GET
$scope.direccion = {};
$scope.getDireccion = function(idDireccion)
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getDireccion",
       async: false,
       data:
       {
            "idDireccion":idDireccion
       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.direccion = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}

//AJAX GET - EMPTY
$scope.direccionEmpty = {};
$scope.getDireccionEmpty = function()
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getDireccionEmpty",
       async: false,
       data:
       {

       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.direccionEmpty = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}
//VALIDAR
$scope.validarDireccion = function()
{
   $scope.todoOK = false;
   $scope.comprobarHotel = $scope.direccion.hotel != null && $scope.direccion.hotel.length > 1;
   if($scope.comprobarHotel)
   {
       $scope.direccion.hotel = $scope.pasaraMayusPrimerLetra($scope.direccion.hotel);
       $scope.$evalAsync();
   }
   
   $scope.comprobarCalle = $scope.direccion.calle != null && $scope.direccion.calle.length > 1;
   if($scope.comprobarCalle)
   {
       $scope.direccion.calle = $scope.pasaraMayusPrimerLetra($scope.direccion.calle);
       $scope.$evalAsync();
   }
   
   $scope.comprobarNumero = $scope.direccion.numero != null && $scope.direccion.numero.length > 1;
   if($scope.comprobarNumero)
   {
       $scope.direccion.numero = $scope.pasaraMayusPrimerLetra($scope.direccion.numero);
       $scope.$evalAsync();
   }
   
   $scope.comprobarPiso = $scope.direccion.piso != null && $scope.direccion.piso.length > 1;
   if($scope.comprobarPiso)
   {
       $scope.direccion.piso = $scope.pasaraMayusPrimerLetra($scope.direccion.piso);
       $scope.$evalAsync();
   }
   
   $scope.comprobarHabitacion = $scope.direccion.habitacion != null && $scope.direccion.habitacion.length > 1;
   if($scope.comprobarHabitacion)
   {
       $scope.direccion.habitacion = $scope.pasaraMayusPrimerLetra($scope.direccion.habitacion);
       $scope.$evalAsync();
   }
   
   $scope.comprobarObservacion = $scope.direccion.observacion != null && $scope.direccion.observacion.length > 1;
   if($scope.comprobarObservacion)
   {
       $scope.direccion.observacion = $scope.pasaraMayusPrimerLetra($scope.direccion.observacion);
       $scope.$evalAsync();
   }
   
   //COMPROBACION FINAL
   if( $scope.comprobarHotel && $scope.comprobarCalle && $scope.comprobarNumero && $scope.comprobarPiso && $scope.comprobarHabitacion && $scope.comprobarObservacion )
   {
       console.log("TODO VALIDADO");
       $scope.todoOK = true;
       $scope.$evalAsync();
   }
		
}
