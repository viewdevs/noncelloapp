$scope.arrPersonas = [];
$scope.findPersonas = function()
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../findPersonas",
        data:
        {

        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.arrPersonas = resultado;
            $scope.cargando = false;
            $scope.$evalAsync();
        }
    });
}
$scope.findReferidos = function()
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../findReferidos",
        data:
        {

        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.arrReferidos = resultado;
            $scope.cargando = false;
            $scope.$evalAsync();
        }
    });
}


//AJAX SAVE
$scope.guardarPersona = function()
{
    guardo = false;
    $scope.cargando = false;
    
    $.ajax(
    {
        url:"../../guardarPersona",
        async: false,
        data:
        {
            "strPersona": JSON.stringify($scope.persona)
        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            console.log("res WS -> guardarPersona : " + resultado);
            $scope.cargando = false;
            guardo = resultado;
            
            if(guardo)
            {
                $scope.snack("Guardado con exito!");
                $scope.findPersonas();
            }
            $scope.$evalAsync();
        }

    });
    
    return guardo;
}
//AJAX GET
$scope.persona = {};
$scope.getPersona = function(idPersona)
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getPersona",
       async: false,
       data:
       {
            "idPersona":idPersona
       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.persona = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}

//VALIDAR
$scope.validarPersona = function()
{
   $scope.todoOK = false;
   $scope.comprobarNombre = $scope.persona.nombre != null && $scope.persona.nombre.length > 1;
   if($scope.comprobarNombre)
   {
       $scope.persona.nombre = $scope.pasaraMayusPrimerLetra($scope.persona.nombre);
       $scope.$evalAsync();
   }
   
   $scope.comprobarApellido = $scope.persona.apellido != null && $scope.persona.apellido.length > 1;
   if($scope.comprobarApellido)
   {
       $scope.persona.apellido = $scope.pasaraMayusPrimerLetra($scope.persona.apellido);
       $scope.$evalAsync();
   }
   
   $scope.comprobarEmail = $scope.persona.email != null && $scope.persona.email.length > 1;
   if($scope.comprobarEmail)
   {
       $scope.persona.email = $scope.pasaraMayusPrimerLetra($scope.persona.email);
       $scope.$evalAsync();
   }
   
   $scope.comprobarPass = $scope.persona.pass != null && $scope.persona.pass.length > 1;
   if($scope.comprobarPass)
   {
       $scope.persona.pass = $scope.pasaraMayusPrimerLetra($scope.persona.pass);
       $scope.$evalAsync();
   }
   
   $scope.comprobarTelefono = $scope.persona.telefono != null && $scope.persona.telefono.length > 1;
   if($scope.comprobarTelefono)
   {
       $scope.persona.telefono = $scope.pasaraMayusPrimerLetra($scope.persona.telefono);
       $scope.$evalAsync();
   }
   
   $scope.comprobarDireccion = $scope.persona.direccion != null && $scope.persona.direccion.length > 1;
   if($scope.comprobarDireccion)
   {
       $scope.persona.direccion = $scope.pasaraMayusPrimerLetra($scope.persona.direccion);
       $scope.$evalAsync();
   }
   
   $scope.comprobarFoto = $scope.persona.foto != null && $scope.persona.foto.length > 1;
   if($scope.comprobarFoto)
   {
       $scope.persona.foto = $scope.pasaraMayusPrimerLetra($scope.persona.foto);
       $scope.$evalAsync();
   }
   
   $scope.comprobarEsCliente = $scope.persona.esCliente != null && $scope.persona.esCliente.length > 1;
   if($scope.comprobarEsCliente)
   {
       $scope.persona.esCliente = $scope.pasaraMayusPrimerLetra($scope.persona.esCliente);
       $scope.$evalAsync();
   }
   
   $scope.comprobarEsReferido = $scope.persona.esReferido != null && $scope.persona.esReferido.length > 1;
   if($scope.comprobarEsReferido)
   {
       $scope.persona.esReferido = $scope.pasaraMayusPrimerLetra($scope.persona.esReferido);
       $scope.$evalAsync();
   }
   
   $scope.comprobarEsOperador = $scope.persona.esOperador != null && $scope.persona.esOperador.length > 1;
   if($scope.comprobarEsOperador)
   {
       $scope.persona.esOperador = $scope.pasaraMayusPrimerLetra($scope.persona.esOperador);
       $scope.$evalAsync();
   }
   
   $scope.comprobarObservaciones = $scope.persona.observaciones != null && $scope.persona.observaciones.length > 1;
   if($scope.comprobarObservaciones)
   {
       $scope.persona.observaciones = $scope.pasaraMayusPrimerLetra($scope.persona.observaciones);
       $scope.$evalAsync();
   }
   
   //COMPROBACION FINAL
   if( $scope.comprobarNombre && $scope.comprobarApellido && $scope.comprobarEmail && $scope.comprobarPass && $scope.comprobarTelefono && $scope.comprobarDireccion && $scope.comprobarFoto && $scope.comprobarEsCliente && $scope.comprobarEsReferido && $scope.comprobarEsOperador && $scope.comprobarObservaciones )
   {
       console.log("TODO VALIDADO");
       $scope.todoOK = true;
       $scope.$evalAsync();
   }
		
}
