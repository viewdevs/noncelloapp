$scope.arrPedidos = [];
$scope.findPedidos = function()
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../findPedidos",
        data:
        {

        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.arrPedidos = resultado;
            $scope.cargando = false;
            $scope.$evalAsync();
        }
    });
}


$scope.dameMisPedidos = function()
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../dameMisPedidos",
        data:
        {

        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.arrPedidos = resultado;
            $scope.cargando = false;
            $scope.$evalAsync();
        }
    });
}
$scope.miPedido = null;
$scope.damePedidoEstoyHaciendo = function()
{
    $scope.cargandoCarrito = false;
    $.ajax(
    {
        url:"../../damePedidoEstoyHaciendo",
        data:
        {

        },
        beforeSend: function (xhr) 
        {
            $scope.cargandoCarrito = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.miPedido = resultado;
//            console.log("mi pedido res: " + JSON.stringify(resultado));
//            if(JSON.stringify(resultado).length > 0)
//            {
//                
//            }
            $scope.cargandoCarrito = false;
            $scope.$evalAsync();
//            console.log("mi pedido: " + JSON.stringify($scope.miPedido));
        }
    });
}


//AJAX SAVE
$scope.agregarDetalleAMiPedido = function(opcionProducto)
{
    
    $scope.$evalAsync();
    
    if($scope.personaLogeada != null)
    {
        if($scope.personaLogeada.id == -1)
        {
            $scope.abrirModalLogin();
        }
        else
        {
            if(!$scope.cargandoCarrito)
            {
                $scope.cargandoCarrito = true;
                $.ajax(
                {
                    url:"../../agregarDetalleAMiPedido",
                    data:
                    {
                        "idOpcionProducto": opcionProducto.id,
                        "cantidad": opcionProducto.cantidadLlevo
                    },
                    beforeSend: function (xhr) 
                    {
                        $scope.cargandoCarrito = true;
                    },
                    success: function (resultado, textStatus, jqXHR) 
                    {
                        console.log("res WS -> agregarDetalleAMiPedido : " + resultado);
                        $scope.cargandoCarrito = false;
                        $scope.miPedido = resultado;

                        $("#modal-producto").modal('hide');

                        $scope.snack("Producto agregado al Carrito!");
                        $scope.$evalAsync();
                    }
                });
            }
        }
    }
    else
    {
        $scope.abrirModalLogin();
    }
}
$scope.quitarDetalleAMiPedido = function(idDetalle)
{
    
    $scope.$evalAsync();
    
    if($scope.personaLogeada != null)
    {
        if($scope.personaLogeada.id == -1)
        {
            $scope.abrirModalLogin();
        }
        else
        {
            if(!$scope.cargandoCarrito)
            {
                $scope.cargandoCarrito = true;
                $.ajax(
                {
                    url:"../../quitarDetalleAMiPedido",
                    data:
                    {
                        "idDetalle": idDetalle,
                    },
                    beforeSend: function (xhr) 
                    {
                        $scope.cargandoCarrito = true;
                    },
                    success: function (resultado, textStatus, jqXHR) 
                    {
                        console.log("res WS -> quitarDetalleAMiPedido : " + resultado);
                        $scope.cargandoCarrito = false;
                        $scope.miPedido = resultado;

                        $("#modal-producto").modal('hide');

                        $scope.snack("Producto quitado!");
                        $scope.$evalAsync();
                    }
                });
            }
        }
    }
    else
    {
        $scope.abrirModalLogin();
    }
}
$scope.guardarPedido = function()
{
    guardo = false;
    $scope.cargando = false;
    
    $.ajax(
    {
        url:"../../guardarPedido",
        async: false,
        data:
        {
            "strPedido": JSON.stringify($scope.pedido)
        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            console.log("res WS -> guardarPedido : " + resultado);
            $scope.cargando = false;
            guardo = resultado;
            
            if(guardo)
            {
                $scope.snack("Guardado con exito!");
                $scope.findPedidos();
            }
            $scope.$evalAsync();
        }

    });
    
    return guardo;
}
//AJAX GET
$scope.pedido = {};
$scope.getPedido = function(idPedido)
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getPedido",
       async: false,
       data:
       {
            "idPedido":idPedido
       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.pedido = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}

//AJAX GET - EMPTY
$scope.pedidoEmpty = {};
$scope.getPedidoEmpty = function()
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getPedidoEmpty",
       async: false,
       data:
       {

       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.pedidoEmpty = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}
//VALIDAR
$scope.validarPedido = function()
{
   $scope.todoOK = false;
   $scope.comprobarFecha = $scope.pedido.fecha != null && $scope.pedido.fecha.length > 1;
   if($scope.comprobarFecha)
   {
       $scope.pedido.fecha = $scope.pasaraMayusPrimerLetra($scope.pedido.fecha);
       $scope.$evalAsync();
   }
   
   $scope.comprobarReferido = $scope.pedido.referido != null && $scope.pedido.referido.length > 1;
   if($scope.comprobarReferido)
   {
       $scope.pedido.referido = $scope.pasaraMayusPrimerLetra($scope.pedido.referido);
       $scope.$evalAsync();
   }
   
   $scope.comprobarCliente = $scope.pedido.cliente != null && $scope.pedido.cliente.length > 1;
   if($scope.comprobarCliente)
   {
       $scope.pedido.cliente = $scope.pasaraMayusPrimerLetra($scope.pedido.cliente);
       $scope.$evalAsync();
   }
   
   $scope.comprobarDireccionEntrega = $scope.pedido.direccionEntrega != null && $scope.pedido.direccionEntrega.length > 1;
   if($scope.comprobarDireccionEntrega)
   {
       $scope.pedido.direccionEntrega = $scope.pasaraMayusPrimerLetra($scope.pedido.direccionEntrega);
       $scope.$evalAsync();
   }
   
   $scope.comprobarPrecioCongelado = $scope.pedido.precioCongelado != null && $scope.pedido.precioCongelado.length > 1;
   if($scope.comprobarPrecioCongelado)
   {
       $scope.pedido.precioCongelado = $scope.pasaraMayusPrimerLetra($scope.pedido.precioCongelado);
       $scope.$evalAsync();
   }
   
   //COMPROBACION FINAL
   if( $scope.comprobarFecha && $scope.comprobarReferido && $scope.comprobarCliente && $scope.comprobarDireccionEntrega && $scope.comprobarPrecioCongelado )
   {
       console.log("TODO VALIDADO");
       $scope.todoOK = true;
       $scope.$evalAsync();
   }
		
}
