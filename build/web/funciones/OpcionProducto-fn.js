$scope.arrOpcionProductos = [];
$scope.findOpcionProductos = function()
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../findOpcionProductos",
        data:
        {

        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.arrOpcionProductos = resultado;
            $scope.cargando = false;
            $scope.$evalAsync();
        }
    });
}


//AJAX SAVE
$scope.guardarOpcionProducto = function()
{
    guardo = false;
    $scope.cargando = false;
    
    $.ajax(
    {
        url:"../../guardarOpcionProducto",
        async: false,
        data:
        {
            "strOpcionProducto": JSON.stringify($scope.opcionProducto)
        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            console.log("res WS -> guardarOpcionProducto : " + resultado);
            $scope.cargando = false;
            guardo = resultado;
            
            if(guardo)
            {
                $scope.snack("Guardado con exito!");
                $scope.findOpcionProductos();
            }
            $scope.$evalAsync();
        }

    });
    
    return guardo;
}
//AJAX GET
$scope.opcionProducto = {};
$scope.getOpcionProducto = function(idOpcionProducto)
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getOpcionProducto",
       async: false,
       data:
       {
            "idOpcionProducto":idOpcionProducto
       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.opcionProducto = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}

//AJAX GET - EMPTY
$scope.opcionProductoEmpty = {};
$scope.getOpcionProductoEmpty = function()
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getOpcionProductoEmpty",
       async: false,
       data:
       {

       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.opcionProductoEmpty = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}
//VALIDAR
$scope.validarOpcionProducto = function()
{
   $scope.todoOK = false;
   $scope.comprobarNombre = $scope.opcionProducto.nombre != null && $scope.opcionProducto.nombre.length > 1;
   if($scope.comprobarNombre)
   {
       $scope.opcionProducto.nombre = $scope.pasaraMayusPrimerLetra($scope.opcionProducto.nombre);
       $scope.$evalAsync();
   }
   
   $scope.comprobarSubEncabezado = $scope.opcionProducto.subEncabezado != null && $scope.opcionProducto.subEncabezado.length > 1;
   if($scope.comprobarSubEncabezado)
   {
       $scope.opcionProducto.subEncabezado = $scope.pasaraMayusPrimerLetra($scope.opcionProducto.subEncabezado);
       $scope.$evalAsync();
   }
   
   $scope.comprobarPrecio = $scope.opcionProducto.precio != null && $scope.opcionProducto.precio.length > 1;
   if($scope.comprobarPrecio)
   {
       $scope.opcionProducto.precio = $scope.pasaraMayusPrimerLetra($scope.opcionProducto.precio);
       $scope.$evalAsync();
   }
   
   $scope.comprobarDescripcion = $scope.opcionProducto.descripcion != null && $scope.opcionProducto.descripcion.length > 1;
   if($scope.comprobarDescripcion)
   {
       $scope.opcionProducto.descripcion = $scope.pasaraMayusPrimerLetra($scope.opcionProducto.descripcion);
       $scope.$evalAsync();
   }
   
   $scope.comprobarProductoPadre = $scope.opcionProducto.productoPadre != null && $scope.opcionProducto.productoPadre.length > 1;
   if($scope.comprobarProductoPadre)
   {
       $scope.opcionProducto.productoPadre = $scope.pasaraMayusPrimerLetra($scope.opcionProducto.productoPadre);
       $scope.$evalAsync();
   }
   
   $scope.comprobarVisible = $scope.opcionProducto.visible != null && $scope.opcionProducto.visible.length > 1;
   if($scope.comprobarVisible)
   {
       $scope.opcionProducto.visible = $scope.pasaraMayusPrimerLetra($scope.opcionProducto.visible);
       $scope.$evalAsync();
   }
   
   //COMPROBACION FINAL
   if( $scope.comprobarNombre && $scope.comprobarSubEncabezado && $scope.comprobarPrecio && $scope.comprobarDescripcion && $scope.comprobarProductoPadre && $scope.comprobarVisible )
   {
       console.log("TODO VALIDADO");
       $scope.todoOK = true;
       $scope.$evalAsync();
   }
		
}
