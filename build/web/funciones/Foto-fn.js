$scope.arrFotos = [];
$scope.findFotos = function()
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../findFotos",
        data:
        {

        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.arrFotos = resultado;
            $scope.cargando = false;
            $scope.$evalAsync();
        }
    });
}


//AJAX SAVE
$scope.guardarFoto = function()
{
    guardo = false;
    $scope.cargando = false;
    
    $.ajax(
    {
        url:"../../guardarFoto",
        async: false,
        data:
        {
            "strFoto": JSON.stringify($scope.foto)
        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            console.log("res WS -> guardarFoto : " + resultado);
            $scope.cargando = false;
            guardo = resultado;
            
            if(guardo)
            {
                $scope.snack("Guardado con exito!");
                $scope.findFotos();
            }
            $scope.$evalAsync();
        }

    });
    
    return guardo;
}
//AJAX GET
$scope.foto = {};
$scope.getFoto = function(idFoto)
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getFoto",
       async: false,
       data:
       {
            "idFoto":idFoto
       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.foto = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}

//AJAX GET - EMPTY
$scope.fotoEmpty = {};
$scope.getFotoEmpty = function()
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getFotoEmpty",
       async: false,
       data:
       {

       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.fotoEmpty = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}
//VALIDAR
$scope.validarFoto = function()
{
   $scope.todoOK = false;
   $scope.comprobarUrlProvisoria = $scope.foto.urlProvisoria != null && $scope.foto.urlProvisoria.length > 1;
   if($scope.comprobarUrlProvisoria)
   {
       $scope.foto.urlProvisoria = $scope.pasaraMayusPrimerLetra($scope.foto.urlProvisoria);
       $scope.$evalAsync();
   }
   
   //COMPROBACION FINAL
   if( $scope.comprobarUrlProvisoria )
   {
       console.log("TODO VALIDADO");
       $scope.todoOK = true;
       $scope.$evalAsync();
   }
		
}
