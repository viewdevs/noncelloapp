$scope.arrRelProductoHashs = [];
$scope.findRelProductoHashs = function()
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../findRelProductoHashs",
        data:
        {

        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.arrRelProductoHashs = resultado;
            $scope.cargando = false;
            $scope.$evalAsync();
        }
    });
}


//AJAX SAVE
$scope.guardarRelProductoHash = function()
{
    guardo = false;
    $scope.cargando = false;
    
    $.ajax(
    {
        url:"../../guardarRelProductoHash",
        async: false,
        data:
        {
            "strRelProductoHash": JSON.stringify($scope.relProductoHash)
        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            console.log("res WS -> guardarRelProductoHash : " + resultado);
            $scope.cargando = false;
            guardo = resultado;
            
            if(guardo)
            {
                $scope.snack("Guardado con exito!");
                $scope.findRelProductoHashs();
            }
            $scope.$evalAsync();
        }

    });
    
    return guardo;
}
//AJAX GET
$scope.relProductoHash = {};
$scope.getRelProductoHash = function(idRelProductoHash)
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getRelProductoHash",
       async: false,
       data:
       {
            "idRelProductoHash":idRelProductoHash
       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.relProductoHash = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}

//AJAX GET - EMPTY
$scope.relProductoHashEmpty = {};
$scope.getRelProductoHashEmpty = function()
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getRelProductoHashEmpty",
       async: false,
       data:
       {

       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.relProductoHashEmpty = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}
//VALIDAR
$scope.validarRelProductoHash = function()
{
   $scope.todoOK = false;
   $scope.comprobarHashtag = $scope.relProductoHash.hashtag != null && $scope.relProductoHash.hashtag.length > 1;
   if($scope.comprobarHashtag)
   {
       $scope.relProductoHash.hashtag = $scope.pasaraMayusPrimerLetra($scope.relProductoHash.hashtag);
       $scope.$evalAsync();
   }
   
   $scope.comprobarProducto = $scope.relProductoHash.producto != null && $scope.relProductoHash.producto.length > 1;
   if($scope.comprobarProducto)
   {
       $scope.relProductoHash.producto = $scope.pasaraMayusPrimerLetra($scope.relProductoHash.producto);
       $scope.$evalAsync();
   }
   
   $scope.comprobarActivo = $scope.relProductoHash.activo != null && $scope.relProductoHash.activo.length > 1;
   if($scope.comprobarActivo)
   {
       $scope.relProductoHash.activo = $scope.pasaraMayusPrimerLetra($scope.relProductoHash.activo);
       $scope.$evalAsync();
   }
   
   //COMPROBACION FINAL
   if( $scope.comprobarHashtag && $scope.comprobarProducto && $scope.comprobarActivo )
   {
       console.log("TODO VALIDADO");
       $scope.todoOK = true;
       $scope.$evalAsync();
   }
		
}
