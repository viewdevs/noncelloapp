-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-07-2019 a las 02:30:36
-- Versión del servidor: 10.1.22-MariaDB
-- Versión de PHP: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `noncello`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cambiosestadospedido`
--

CREATE TABLE `cambiosestadospedido` (
  `ID` int(11) NOT NULL,
  `fkPedido` int(11) DEFAULT NULL,
  `fkOperador` int(11) DEFAULT NULL,
  `fkEstadoPedidoDisponible` int(11) DEFAULT NULL,
  `CUANDO` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cambiosestadospedidos`
--

CREATE TABLE `cambiosestadospedidos` (
  `ID` int(11) NOT NULL,
  `CUANDO` datetime DEFAULT NULL,
  `fkEstadoPedidoDisponible` int(11) DEFAULT NULL,
  `fkOperador` int(11) DEFAULT NULL,
  `fkPedido` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuraciones`
--

CREATE TABLE `configuraciones` (
  `ID` int(11) NOT NULL,
  `ENABLED` tinyint(1) DEFAULT '0',
  `IP` varchar(255) DEFAULT NULL,
  `NOMBRE` varchar(255) DEFAULT NULL,
  `NOMBREPROYECTO` varchar(255) DEFAULT NULL,
  `subCarpetaImagenes` text NOT NULL,
  `PROTOCOLO` varchar(255) DEFAULT NULL,
  `PUERTO` varchar(255) DEFAULT NULL,
  `VARSESSIONHTTP` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `configuraciones`
--

INSERT INTO `configuraciones` (`ID`, `ENABLED`, `IP`, `NOMBRE`, `NOMBREPROYECTO`, `subCarpetaImagenes`, `PROTOCOLO`, `PUERTO`, `VARSESSIONHTTP`) VALUES
(1, 0, 'viewdevscompany.com', 'produccion', 'noncello', 'iconos', 'http://', ':8081', 'usuarioNoncello'),
(2, 0, '192.168.2.8', 'home', 'noncello', 'iconos', 'http://', '', 'usuarioNoncello'),
(3, 1, '192.168.5.90', 'noc', 'noncello', 'iconos', 'http://', '', 'usuarioNoncello');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detallespedidos`
--

CREATE TABLE `detallespedidos` (
  `ID` int(11) NOT NULL,
  `CANTIDAD` double DEFAULT NULL,
  `fkPedido` int(11) DEFAULT NULL,
  `fkOpcionProducto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detallespedidos`
--

INSERT INTO `detallespedidos` (`ID`, `CANTIDAD`, `fkPedido`, `fkOpcionProducto`) VALUES
(16, 3, 16, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direcciones`
--

CREATE TABLE `direcciones` (
  `ID` int(11) NOT NULL,
  `CALLE` varchar(255) DEFAULT NULL,
  `NUMERO` varchar(255) DEFAULT NULL,
  `HOTEL` varchar(255) DEFAULT NULL,
  `PISO` varchar(255) DEFAULT NULL,
  `HABITACION` varchar(255) DEFAULT NULL,
  `OBSERVACION` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `direcciones`
--

INSERT INTO `direcciones` (`ID`, `CALLE`, `NUMERO`, `HOTEL`, `PISO`, `HABITACION`, `OBSERVACION`) VALUES
(-1, 'NONE', 'NONE', 'NONE', 'NONE', 'NONE', 'NONE'),
(1, 'Frey', '468', 'NONE', '4', 'B', 'NONE'),
(2, 'Vilcapugio', '02', 'NONE', 'NONE', 'NONE', 'NONE'),
(3, 'test', 'test', 'test', 'test', 'test', 'test'),
(4, 'test', 'test', 'test', 'test', 'test', 'test'),
(5, 'NONE', 'NONE', 'NONE', 'NONE', 'NONE', 'NONE'),
(6, 'NONE', 'NONE', 'NONE', 'NONE', 'NONE', 'NONE'),
(7, 'NONE', 'NONE', 'NONE', 'NONE', 'NONE', 'NONE'),
(8, 'Frew', '230', 'NONE', 'NONE', 'NONE', 'NONE'),
(9, 'Moreno 187', 'SUCURSAL', 'SUCURSAL', 'SUCURSAL', 'SUCURSAL', 'RETIRO EN SUCURSAL'),
(10, 'For', '1234', 'VACIO', 'VACIO', 'VACIO', 'VACIO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadospedidosdisponibles`
--

CREATE TABLE `estadospedidosdisponibles` (
  `ID` int(11) NOT NULL,
  `NOMBR` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estadospedidosdisponibles`
--

INSERT INTO `estadospedidosdisponibles` (`ID`, `NOMBR`) VALUES
(1, 'COMPRANDO'),
(2, 'SOLICITADO'),
(3, 'CANCELADO CLIENTE'),
(4, 'CONFIRMADO PROVEEDOR'),
(5, 'COMPLETADO CLIENTE'),
(6, 'COMPLETADO PROVEEDOR'),
(7, 'CANCELADO PROVEEDOR');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fotos`
--

CREATE TABLE `fotos` (
  `ID` int(11) NOT NULL,
  `URLPROVISORIA` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fotos`
--

INSERT INTO `fotos` (`ID`, `URLPROVISORIA`) VALUES
(1, '1-1.jpg'),
(2, '2-1.jpg'),
(3, '2-2.jpg'),
(4, '3-1.jpg'),
(5, '3-2.jpg'),
(6, '4-2.jpg'),
(7, '4-1.jpg'),
(8, '5-2.jpg'),
(9, '5-1.jpg'),
(10, '6-2.jpg'),
(11, '6-1.jpg'),
(12, '7-2.jpg'),
(13, '7-1.jpg'),
(14, '11-1.jpg'),
(15, '10-1.jpg'),
(16, '9-1.jpg'),
(17, '8-1.jpg'),
(18, '14-1.jpg'),
(19, '15-1.jpg'),
(20, '16-1.jpg'),
(21, '12-1.jpg'),
(22, '18-1.jpg'),
(23, '13-1.jpg'),
(24, '17-1.jpg'),
(25, '20-1.jpg'),
(26, '19-1.jpg'),
(27, '21-1.jpg'),
(28, '22-1.jpg'),
(29, '23-1.jpg'),
(30, '24-1.jpg'),
(31, '25-1.jpg'),
(32, 'lu.jpg'),
(33, 'tomi.jpg'),
(34, 'nico.jpg'),
(35, 'default.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fotosopcproductos`
--

CREATE TABLE `fotosopcproductos` (
  `ID` int(11) NOT NULL,
  `VISIBLE` tinyint(1) DEFAULT '0',
  `fkOpcionProducto` int(11) DEFAULT NULL,
  `fkFoto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fotosopcproductos`
--

INSERT INTO `fotosopcproductos` (`ID`, `VISIBLE`, `fkOpcionProducto`, `fkFoto`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 3),
(3, 1, 2, 2),
(4, 1, 3, 4),
(5, 1, 3, 5),
(6, 1, 4, 7),
(7, 1, 4, 6),
(8, 1, 5, 9),
(9, 1, 5, 8),
(10, 1, 6, 11),
(11, 1, 6, 10),
(12, 1, 7, 12),
(13, 1, 7, 13),
(14, 1, 9, 15),
(15, 1, 8, 16),
(16, 1, 10, 17),
(17, 1, 11, 14),
(18, 1, 17, 23),
(19, 1, 16, 18),
(20, 1, 12, 24),
(21, 1, 14, 20),
(22, 1, 15, 22),
(23, 1, 13, 21),
(24, 1, 18, 19),
(25, 1, 20, 25),
(26, 1, 19, 26),
(27, 1, 21, 27),
(28, 1, 22, 28),
(29, 1, 23, 29),
(30, 1, 24, 30),
(31, 1, 25, 31);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hashs`
--

CREATE TABLE `hashs` (
  `ID` int(11) NOT NULL,
  `HASH` varchar(255) DEFAULT NULL,
  `SUBTEXTO` varchar(255) DEFAULT NULL,
  `FOTO` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `hashs`
--

INSERT INTO `hashs` (`ID`, `HASH`, `SUBTEXTO`, `FOTO`) VALUES
(1, 'Chocolates', 'El mejor chocolate de Bariloche', 'banner-rama.jpg'),
(2, 'Chocolates Rellenos', 'Los mejores chocolates rellenos de Bariloche', 'choco-blanco.jpg'),
(3, 'Frutos Secos', 'Chocolates con Frutos Secos', 'choco-nuez.jpg'),
(4, 'Ramas', 'Ramas Saborizadas', 'choco.jpg'),
(5, 'Dulces', 'Mermeladas', 'dulces.jpg'),
(6, 'Huevos', 'Huevos de Chocolate y Pascuas', 'huevo.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menus`
--

CREATE TABLE `menus` (
  `ID` int(11) NOT NULL,
  `NOMBRE` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `icono` text NOT NULL,
  `img` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menus`
--

INSERT INTO `menus` (`ID`, `NOMBRE`, `URL`, `icono`, `img`) VALUES
(1, 'Home', '../home/home.jsp', 'home.png', 'home.jpg'),
(2, 'Quienes Somos?', '../quienes/quienes.jsp', 'quienes.png', 'quienes_somos.jpg'),
(3, 'Productos', '../categorias/categorias.jsp', 'chocolate.png', 'producto.jpg'),
(4, 'Promos', '../promos/promos.jsp', 'promo.png', 'promo.jpg'),
(6, 'Mi Carrito', '../carrito/carrito.jsp', 'carrito.png', 'Banner4.jpg'),
(99, 'Contacto', '../contacto/contacto.jsp', 'contacto.png', 'Banner11.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `opcionesproductos`
--

CREATE TABLE `opcionesproductos` (
  `ID` int(11) NOT NULL,
  `fkProductoPadre` int(11) DEFAULT NULL,
  `NOMBRE` varchar(255) DEFAULT NULL,
  `SUBENCABEZADO` varchar(255) DEFAULT NULL,
  `PRECIO` double DEFAULT NULL,
  `DESCRIPCION` varchar(255) DEFAULT NULL,
  `VISIBLE` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `opcionesproductos`
--

INSERT INTO `opcionesproductos` (`ID`, `fkProductoPadre`, `NOMBRE`, `SUBENCABEZADO`, `PRECIO`, `DESCRIPCION`, `VISIBLE`) VALUES
(1, 1, 'Caja de chocolates 3 Piezas', '3 Tabletas a elección!', 100, '3 Tabletas del más Exquisito chocolate a elección!', 1),
(2, 2, 'Caja de chocolates Chica', 'Podes armar tu caja con el chocolate que vos elijas!', 250, 'Caja de chocolate chica. Armada en el momento con el más exquisito chocolate que vos elijas!', 1),
(3, 3, 'Caja de chocolates Mediana', 'Podes armar tu caja con el chocolate que vos elijas!', 350, 'Caja de chocolate mediana Armada en el momento con el más exquisito chocolate que vos elijas!', 1),
(4, 4, 'Caja de chocolates Grande', 'Podes armar tu caja con el chocolate que vos elijas!', 450, 'Caja de chocolate grande. Armada en el momento con el más exquisito chocolate que vos elijas!', 1),
(5, 5, 'Caja de chocolates Extra Grande', 'Podes armar tu caja con el chocolate que vos elijas!', 550, 'Caja de chocolate extra grande. Armada en el momento con el más exquisito chocolate que vos elijas!', 1),
(6, 6, 'Caja de chocolates Super Promo!', 'Podes armar tu caja con el chocolate que vos elijas!', 700, 'Caja de chocolate super promo!. Armada en el momento con el más exquisito chocolate que vos elijas. A un precío único!', 1),
(7, 7, 'Caja 6 Alfajores', '6 Unidades', 280, '6 Alfajores de chocolate rellenos del más rico dulce de leche!', 1),
(8, 8, 'Dulce Home Made Frambuesa Chico', '45 gramos', 60, 'El más rico dulce artesanal, elaborado en la patagonía!', 1),
(9, 8, 'Dulce Home Made Calafate Chico', '45 gramos', 60, 'El más rico dulce artesanal, elaborado en la patagonía!', 1),
(10, 8, 'Dulce Home Made Mosqueta Chico', '45 gramos', 60, 'El más rico dulce artesanal, elaborado en la patagonía!', 1),
(11, 8, 'Dulce Home Made Sauco Chico', '45 gramos', 60, 'El más rico dulce artesanal, elaborado en la patagonía!', 1),
(12, 9, 'Dulce Home Made Calafate Grande', '454 gramos', 160, 'El más rico dulce artesanal, elaborado en la patagonía!', 1),
(13, 9, 'Dulce Home Made Mosqueta Grande', '454 gramos', 160, 'El más rico dulce artesanal, elaborado en la patagonía!', 1),
(14, 9, 'Dulce Home Made Sauco Grande', '454 gramos', 160, 'El más rico dulce artesanal, elaborado en la patagonía!', 1),
(15, 9, 'Dulce Home Made Giunda Grande', '454 gramos', 160, 'El más rico dulce artesanal, elaborado en la patagonía!', 1),
(16, 9, 'Dulce Home Made Frutilla Grande', '454 gramos', 160, 'El más rico dulce artesanal, elaborado en la patagonía!', 1),
(17, 9, 'Dulce Home Made Frambuesa Grande', '454 gramos', 160, 'El más rico dulce artesanal, elaborado en la patagonía!', 1),
(18, 9, 'Dulce Home Made Ruibarbo Grande', '454 gramos', 160, 'El más rico dulce artesanal, elaborado en la patagonía!', 1),
(19, 10, 'Dulce Home Made Frambuesa Mediano', '250 gramos', 90, 'El más rico dulce artesanal, elaborado en la patagonía!', 1),
(20, 10, 'Dulce Home Made Mosqueta Mediano', '250 gramos', 90, 'El más rico dulce artesanal, elaborado en la patagonía!', 1),
(21, 11, 'Chocolate por peso', '1000 gramos', 1300, 'Chocolate por peso, único por su gran calidad e inigualable sabor!', 1),
(22, 12, 'PROMO 1', 'Arma las cajas de tu promo como más te guste!', 980, '2 x caja 01 – 1 x caja 06 – 1 x caja 08 – 1 x caja 012', 1),
(23, 13, 'PROMO 2', 'Arma las cajas de tu promo como más te guste!', 1300, '3 x caja 01 – 2 x caja 06 – 2 x caja 08', 1),
(24, 14, 'PROMO 3', 'Arma las cajas de tu promo como más te guste!', 1800, '4 x caja 01 – 3 x caja 06 – 3 x caja 08', 1),
(25, 15, 'PROMO 4', 'Arma las cajas de tu promo como más te guste!', 2500, '4 x caja 01 – 2 x caja 06 – 2 x caja 012 – 1 x caja  016 – 1 x caja alfajores', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `ID` int(11) NOT NULL,
  `FECHA` datetime DEFAULT NULL,
  `fkCliente` int(11) DEFAULT NULL,
  `fkReferido` int(11) DEFAULT NULL,
  `fkDireccionEntrega` int(11) DEFAULT NULL,
  `PRECIOCONGELADO` double DEFAULT NULL,
  `retiroSucursal` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pedidos`
--

INSERT INTO `pedidos` (`ID`, `FECHA`, `fkCliente`, `fkReferido`, `fkDireccionEntrega`, `PRECIOCONGELADO`, `retiroSucursal`) VALUES
(16, '2019-07-15 21:27:43', 1, 2, 10, 1785, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `ID` int(11) NOT NULL,
  `NOMBRE` varchar(255) DEFAULT NULL,
  `APELLIDO` varchar(255) DEFAULT NULL,
  `EMAIL` text,
  `TELEFONO` varchar(255) DEFAULT NULL,
  `fkFoto` int(11) DEFAULT NULL,
  `fkDireccion` int(11) DEFAULT NULL,
  `PASS` varchar(255) DEFAULT NULL,
  `ESCLIENTE` tinyint(1) DEFAULT '0',
  `ESOPERADOR` tinyint(1) DEFAULT '0',
  `ESREFERIDO` tinyint(1) DEFAULT '0',
  `OBSERVACIONES` varchar(255) DEFAULT NULL,
  `codigoReferencial` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`ID`, `NOMBRE`, `APELLIDO`, `EMAIL`, `TELEFONO`, `fkFoto`, `fkDireccion`, `PASS`, `ESCLIENTE`, `ESOPERADOR`, `ESREFERIDO`, `OBSERVACIONES`, `codigoReferencial`) VALUES
(1, 'Pepi', 'ggg', 'nico.grossi4@gmail.com', '789789789999', 34, 1, '123456', 1, 0, 0, '', ''),
(2, 'Luana', 'Mataloni', 'viewdevscompany@gmail.com', '2920 545454', 32, 1, '123456', 0, 0, 1, 'NONE', '2233'),
(3, 'Tomas', 'Grossi', 'tomygrossi1998@gmail.com', '54 9 294 480-0796', 33, 2, '123456', 0, 0, 1, 'NONE', ''),
(10, '', '', '', '', 35, 9, '', 1, 0, 0, NULL, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `ID` int(11) NOT NULL,
  `NOMBRE` varchar(255) DEFAULT NULL,
  `ESPROMO` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`ID`, `NOMBRE`, `ESPROMO`) VALUES
(1, 'Caja de chocolates 3 Piezas', 0),
(2, 'Caja de chocolates Chica', 0),
(3, 'Caja de chocolates Mediana', 0),
(4, 'Caja de chocolates Grande', 0),
(5, 'Caja de chocolates Extra Grande', 0),
(6, 'Caja de chocolates Super Promo!', 0),
(7, 'Caja 6 Alfajores', 0),
(8, 'Dulce Home Made  Chico', 0),
(9, 'Dulce Home Made Grande', 0),
(10, 'Dulce Home Made Mediano', 0),
(11, 'Chocolate por peso', 0),
(12, 'PROMO 1', 1),
(13, 'PROMO 2', 1),
(14, 'PROMO 3', 1),
(15, 'PROMO 4', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `relproductohash`
--

CREATE TABLE `relproductohash` (
  `ID` int(11) NOT NULL,
  `ACTIVO` tinyint(1) DEFAULT '0',
  `fkHashtag` int(11) DEFAULT NULL,
  `fkProducto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `relproductohash`
--

INSERT INTO `relproductohash` (`ID`, `ACTIVO`, `fkHashtag`, `fkProducto`) VALUES
(1, 1, 1, 1),
(2, 1, 1, 2),
(3, 1, 1, 3),
(4, 1, 1, 4),
(5, 1, 1, 5),
(6, 1, 1, 6),
(7, 1, 5, 8),
(8, 1, 5, 9),
(9, 1, 5, 10),
(10, 1, 1, 11);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cambiosestadospedido`
--
ALTER TABLE `cambiosestadospedido`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_cambiosEstadosPedido_fkPedido` (`fkPedido`),
  ADD KEY `FK_cambiosEstadosPedido_fkEstadoPedidoDisponible` (`fkEstadoPedidoDisponible`),
  ADD KEY `FK_cambiosEstadosPedido_fkOperador` (`fkOperador`);

--
-- Indices de la tabla `cambiosestadospedidos`
--
ALTER TABLE `cambiosestadospedidos`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_cambiosestadospedidos_fkPedido` (`fkPedido`),
  ADD KEY `FK_cambiosestadospedidos_fkEstadoPedidoDisponible` (`fkEstadoPedidoDisponible`),
  ADD KEY `FK_cambiosestadospedidos_fkOperador` (`fkOperador`);

--
-- Indices de la tabla `configuraciones`
--
ALTER TABLE `configuraciones`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `detallespedidos`
--
ALTER TABLE `detallespedidos`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_detallesPedidos_fkOpcionProducto` (`fkOpcionProducto`),
  ADD KEY `FK_detallesPedidos_fkPedido` (`fkPedido`);

--
-- Indices de la tabla `direcciones`
--
ALTER TABLE `direcciones`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `estadospedidosdisponibles`
--
ALTER TABLE `estadospedidosdisponibles`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fotos`
--
ALTER TABLE `fotos`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fotosopcproductos`
--
ALTER TABLE `fotosopcproductos`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_fotosOpcProductos_fkOpcionProducto` (`fkOpcionProducto`),
  ADD KEY `FK_fotosOpcProductos_fkFoto` (`fkFoto`);

--
-- Indices de la tabla `hashs`
--
ALTER TABLE `hashs`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `opcionesproductos`
--
ALTER TABLE `opcionesproductos`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_opcionesProductos_fkProductoPadre` (`fkProductoPadre`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_pedidos_fkDireccionEntrega` (`fkDireccionEntrega`),
  ADD KEY `FK_pedidos_fkReferido` (`fkReferido`),
  ADD KEY `FK_pedidos_fkCliente` (`fkCliente`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_personas_fkFoto` (`fkFoto`),
  ADD KEY `FK_personas_fkDireccion` (`fkDireccion`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `relproductohash`
--
ALTER TABLE `relproductohash`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_relProductoHash_fkHashtag` (`fkHashtag`),
  ADD KEY `FK_relProductoHash_fkProducto` (`fkProducto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cambiosestadospedido`
--
ALTER TABLE `cambiosestadospedido`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cambiosestadospedidos`
--
ALTER TABLE `cambiosestadospedidos`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `configuraciones`
--
ALTER TABLE `configuraciones`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `detallespedidos`
--
ALTER TABLE `detallespedidos`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `direcciones`
--
ALTER TABLE `direcciones`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `estadospedidosdisponibles`
--
ALTER TABLE `estadospedidosdisponibles`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `fotos`
--
ALTER TABLE `fotos`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT de la tabla `fotosopcproductos`
--
ALTER TABLE `fotosopcproductos`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT de la tabla `hashs`
--
ALTER TABLE `hashs`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `menus`
--
ALTER TABLE `menus`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT de la tabla `opcionesproductos`
--
ALTER TABLE `opcionesproductos`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `relproductohash`
--
ALTER TABLE `relproductohash`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cambiosestadospedido`
--
ALTER TABLE `cambiosestadospedido`
  ADD CONSTRAINT `FK_cambiosEstadosPedido_fkEstadoPedidoDisponible` FOREIGN KEY (`fkEstadoPedidoDisponible`) REFERENCES `estadospedidosdisponibles` (`ID`),
  ADD CONSTRAINT `FK_cambiosEstadosPedido_fkOperador` FOREIGN KEY (`fkOperador`) REFERENCES `personas` (`ID`),
  ADD CONSTRAINT `FK_cambiosEstadosPedido_fkPedido` FOREIGN KEY (`fkPedido`) REFERENCES `pedidos` (`ID`);

--
-- Filtros para la tabla `cambiosestadospedidos`
--
ALTER TABLE `cambiosestadospedidos`
  ADD CONSTRAINT `FK_cambiosestadospedidos_fkEstadoPedidoDisponible` FOREIGN KEY (`fkEstadoPedidoDisponible`) REFERENCES `estadospedidosdisponibles` (`ID`),
  ADD CONSTRAINT `FK_cambiosestadospedidos_fkOperador` FOREIGN KEY (`fkOperador`) REFERENCES `personas` (`ID`),
  ADD CONSTRAINT `FK_cambiosestadospedidos_fkPedido` FOREIGN KEY (`fkPedido`) REFERENCES `pedidos` (`ID`);

--
-- Filtros para la tabla `detallespedidos`
--
ALTER TABLE `detallespedidos`
  ADD CONSTRAINT `FK_detallesPedidos_fkOpcionProducto` FOREIGN KEY (`fkOpcionProducto`) REFERENCES `opcionesproductos` (`ID`),
  ADD CONSTRAINT `FK_detallesPedidos_fkPedido` FOREIGN KEY (`fkPedido`) REFERENCES `pedidos` (`ID`);

--
-- Filtros para la tabla `fotosopcproductos`
--
ALTER TABLE `fotosopcproductos`
  ADD CONSTRAINT `FK_fotosOpcProductos_fkFoto` FOREIGN KEY (`fkFoto`) REFERENCES `fotos` (`ID`),
  ADD CONSTRAINT `FK_fotosOpcProductos_fkOpcionProducto` FOREIGN KEY (`fkOpcionProducto`) REFERENCES `opcionesproductos` (`ID`);

--
-- Filtros para la tabla `opcionesproductos`
--
ALTER TABLE `opcionesproductos`
  ADD CONSTRAINT `FK_opcionesProductos_fkProductoPadre` FOREIGN KEY (`fkProductoPadre`) REFERENCES `productos` (`ID`);

--
-- Filtros para la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `FK_pedidos_fkCliente` FOREIGN KEY (`fkCliente`) REFERENCES `personas` (`ID`),
  ADD CONSTRAINT `FK_pedidos_fkDireccionEntrega` FOREIGN KEY (`fkDireccionEntrega`) REFERENCES `direcciones` (`ID`),
  ADD CONSTRAINT `FK_pedidos_fkReferido` FOREIGN KEY (`fkReferido`) REFERENCES `personas` (`ID`);

--
-- Filtros para la tabla `personas`
--
ALTER TABLE `personas`
  ADD CONSTRAINT `FK_personas_fkDireccion` FOREIGN KEY (`fkDireccion`) REFERENCES `direcciones` (`ID`),
  ADD CONSTRAINT `FK_personas_fkFoto` FOREIGN KEY (`fkFoto`) REFERENCES `fotos` (`ID`);

--
-- Filtros para la tabla `relproductohash`
--
ALTER TABLE `relproductohash`
  ADD CONSTRAINT `FK_relProductoHash_fkHashtag` FOREIGN KEY (`fkHashtag`) REFERENCES `hashs` (`ID`),
  ADD CONSTRAINT `FK_relProductoHash_fkProducto` FOREIGN KEY (`fkProducto`) REFERENCES `productos` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
