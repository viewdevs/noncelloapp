package ws;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import controller.MasterController;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import mailsUtils.MailManager;
import mailsUtils.ParametroMail;
import modelo.*;
import modelo.Pedido;
import org.springframework.web.bind.annotation.*;

@RestController
public class wsPedido
{
    @RequestMapping(value = "getPedido")
    public static Pedido getPedido
    (
        HttpServletRequest request
    )
    {
        Pedido pedido = new Pedido();
        
        return pedido;
    }
    @RequestMapping(value = "agregarOpcionProductoMiCarrito")
    public static Carrito agregarOpcionProductoMiCarrito
    (
        @RequestParam(value = "idOpcionProducto") int idOpcionProducto,
        @RequestParam(value = "cantidad") int cantidad,
        @RequestParam(value = "seLaAumento") boolean seLaAumento,
        HttpServletRequest request
    )
    {
        // 1 - TRAIGO MI CARRITO SEGUN EL CODIGO DE SESION:
        Carrito miCarrito = wsLogin.dameCarritoSegunCodigoDeSesion(request);
        
        // 2 - TRAIGO LA OPCION PRODUCTO DE DB:
        OpcionProducto opcionProductoDB = wsOpcionProducto.getOpcionProducto(idOpcionProducto);
        
        
        if(opcionProductoDB != null)
        {
            if(miCarrito != null)
            {
                Pedido miPedido = miCarrito.getPedido();

                if(miPedido != null)
                {
                    // 3 - AGREGO LA CANTIDAD SI YA EXISTE AL PEDIDO O AGREGO EL PRODUCTO SI NO EXISTE:
                    miPedido.agregarOpcionProductoListadoDetallesPedido(opcionProductoDB, cantidad, seLaAumento);
                }
            }

        }
        
        
        return miCarrito;
    }
    
    
     @RequestMapping(value = "concretarLaVenta" , method = RequestMethod.POST)
    public static boolean concretarLaVenta
    (
        @RequestParam(value = "strPedidoFinal") String strPedidoFinal,
        @RequestParam(value = "retiroSucursal") boolean retiroSucursal,
        HttpServletRequest request
    )
    {
        boolean ok = false;
        
        // 1 - ES MOMENTO DE CONCRETAR LA VENTA:
        
        // 2 - TRAIGO UN PEDIDO FINAL:
        Pedido pedidoFinal;
        
        // 3 - TRAIGO EL CARRITO CON EL PEDIDO NO FINAL, QUE TENIA COMPRANDO EN SESION:
        Carrito carrito = wsLogin.dameCarritoSegunCodigoDeSesion(request);
        
       
        
        // 4 - CONVIERTO EL STR PEDIDO FINAL RECIBIDO EN UN OBJ PEDIDO FINAL:
        if(strPedidoFinal != null)
        {
            pedidoFinal = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create().fromJson(strPedidoFinal, Pedido.class);
            
            if(pedidoFinal != null)
            {
                // 5 - SETEO EL ID DE PEDIDO EN -1 Y LA FECHA ACTUAL:
                pedidoFinal.setId(-1);
                pedidoFinal.setFecha(new Date());
            
                // 6 - TRAIGO EL CLIENTE, LO BUSCO SI YA EXISTE EN DB Y LO SETEO:
                Persona clienteRecibido = pedidoFinal.getCliente();
//                cliente.setId(-1);
                
                if(clienteRecibido != null)
                {
                    String emailCliente = clienteRecibido.getEmail();
                    if(emailCliente != null)
                    {
                        Persona clienteDB = wsPersona.getPersonaByEmail(emailCliente);
                        
                        if(clienteDB != null)
                        {
                            clienteDB.setNombre(clienteRecibido.getNombre());
                            clienteDB.setApellido(clienteRecibido.getApellido());
                            clienteDB.setCodigoReferencial("");
                            clienteDB.setEsCliente(true);
                            clienteDB.setEsOperador(false);
                            clienteDB.setEsReferido(false);
                            clienteDB.setObservaciones("");
                            clienteDB.setTelefono(clienteRecibido.getTelefono());
                            pedidoFinal.setCliente(clienteDB);
                        }
                        else
                        {
                            clienteRecibido.setId(-1);
                            clienteRecibido.setPass(carrito.getCodigoSesion());
                            pedidoFinal.setCliente(clienteRecibido);
                            
                            // ENVIO EMAIL CON CLAVE DEL CLIENTE:
                        }
                    }
                }
            
                
                // 7 - BUSCO EL REFERIDO EN DB Y LO SETEO:
                Persona referidoDB = wsPersona.getReferidoByCodigo(pedidoFinal.getCodigoReferido());
                if(referidoDB != null)
                {
                    pedidoFinal.setReferido(referidoDB);
                }
                else
                {
                    pedidoFinal.setReferido(MasterController.getReferidoDefault());
                }
                            
                
           
                // 8 - RETIRO EN SUCURSAL, SETEO LA DIRECCION DE NONCELLO | ENVIO A DOMICILIO SETEO UNA NUEVA DIRECCION SI NO EXISTE:
                Direccion direccionEntrega = null;
                if(pedidoFinal.isRetiroSucursal())
                {
                    direccionEntrega = MasterController.getDireccionEntregaDefault();
                }
                else
                {
                    direccionEntrega = pedidoFinal.getDireccionEntrega();
                    
                    if(direccionEntrega.getHotel() == null)
                    {
                        direccionEntrega.setHotel("VACIO");
                    }
                    if(direccionEntrega.getObservacion() == null)
                    {
                        direccionEntrega.setObservacion("VACIO");
                    }
                    if(direccionEntrega.getPiso()== null)
                    {
                        direccionEntrega.setPiso("VACIO");
                    }
                    if(direccionEntrega.getHabitacion()== null)
                    {
                        direccionEntrega.setHabitacion("VACIO");
                    }
                    direccionEntrega.setId(-99);
                }
            
                pedidoFinal.setDireccionEntrega(direccionEntrega);
                pedidoFinal.setPrecioCongelado(pedidoFinal.getCalcularTotal());
                
                
                // 9 -  SETEANDO LOS DETALLES AL PEDIDO:
                for(DetallePedido detalleLoop : pedidoFinal.getDetallesList())
                {
                    detalleLoop.setPedido(pedidoFinal);
                }
                
                // 10 - SI ES RETIRO EN SUCURSAL, LE PONGO LA DIRE DE NONCELLO:
                if(pedidoFinal.isRetiroSucursal())
                {
                    pedidoFinal.setDireccionEntrega(MasterController.getDireccionEntregaDefault());
                }
                
                System.out.println("GUARDARIA: " + pedidoFinal.toString());
                
                ok = dao.DAOEclipse.update(pedidoFinal);
            }
        }
        
//        if(carrito != null)
//        {
//            // 1 - GUARDAR EL PEDIDO:
//            Pedido pedido = carrito.getPedido();
//            pedido.setId(-1);
//            pedido.setFecha(new Date());
//            
//            Persona cliente = pedido.getCliente();
//            cliente.setId(-1);
//            
//            pedido.setCliente(cliente);
//            pedido.setReferido(MasterController.getReferidoDefault());
//           
//            Direccion direccionEntrega = pedido.getDireccionEntrega();
//            direccionEntrega.setId(-1);
//            
//            pedido.setDireccionEntrega(direccionEntrega);
//            pedido.setPrecioCongelado(pedido.getCalcularTotal());
//            System.out.println("GUARDARIA: " + pedido.toString());
//            ok = dao.DAOEclipse.update(pedido);
//            ok = dao.DAOEclipse.update(pedido);
            
            
            /// ENVIAR EMAIL:
            
//            Persona cliente = pedido.getCliente();
//            
//            String asunto =  "Nuevo pedido desde la WEB " + cliente.getNombre() +" - " + cliente.getApellido();
//
//            // 1 - LEER EL ARCHIVO HTML PARA PODER PONER LOS VALORES QUE YO QUIERO:
//            //QUITADO:String urlArchivoHtmlTemplate = MasterController.dameUrlArchivoHtmlSegunSO();
//
//            // 2 - LISTADO DE VALORES A REEMPLAZAR EN EL HTML A ENVIAR:
//
//            // ARMO EL ARRAY DE DESTINATARIOS:
//            List<String> destinatarios = new ArrayList<>();
//            //QUITADO:destinatarios.add(MasterController.getMailEncargadoConsultas());
//            destinatarios.add("nico.grossi4@gmail.com");
//
//            MailManager mailManager = new MailManager();
//
//             // 3 - CONVERTIME EL ARCHIVO HTML CON ETIQUETAS ANGULAR CON LOS VALORES QUE YO NECESITO:
//            List<ParametroMail> parametrosMailList = new ArrayList<>();
//
//            List<ParametroMail> parametrosList = new ArrayList<>();
//            parametrosMailList.add(new ParametroMail("titulo", asunto));
//            parametrosMailList.add(new ParametroMail("subtitulo","xxx"));
//            parametrosMailList.add(new ParametroMail("horario", MasterController.formatearFechaAAlgoBonito(pedido.getFecha(),true)));


            // 4 - BUSCO LA FOTO DEL PRODUCTO:
//            String urlImagen = "";
//            String motivo = consultaRecibido.getMotivo();
//            if(motivo != null)
//            {
//                if(motivo.length() > 0)
//                {
//                    ServicioDeLaSeccion servicioDB = (ServicioDeLaSeccion) dao.DAOEclipse.getByJPQL("SELECT s FROM ServicioDeLaSeccion s WHERE s.nombreServicio LIKE '%" + motivo + "%'");
//                    if(servicioDB != null)
//                    {
//                        urlImagen = servicioDB.getFullImgServicio();
//                        System.out.println("URL IMAGEN:" + urlImagen);
//                    }
//                }
//            }
//            parametrosMailList.add(new ParametroMail("imagen", urlImagen));

            // 5 - GENERO UN HTML POPULADO CON ESTA INFORMACION:
            //QUITADO:List<String> htmlSinValoresList = HtmlParserForMails.testHTMLAutocompletar(urlArchivoHtmlTemplate, parametrosMailList);
            //QUITADO:String htmlFinal = HtmlParserForMails.dameHtmlAsStringFromList(htmlSinValoresList);

//            MailManager mail = new MailManager();
//            mail.inicializarParametros();
//            mail.sendMail(asunto, "<h3>Hola</h3>", destinatarios);
//            
//            // 6 - FINALMENTE ENVIO EL MAIL:
//            System.out.println("ENVIO EL MAIL!!");
//        }
        
        return ok;
    }
}
