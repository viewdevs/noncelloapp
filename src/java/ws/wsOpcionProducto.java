package ws;
import com.google.gson.Gson;
import java.util.*;
import modelo.*;
import modelo.FotoOpcProducto;
import modelo.OpcionProducto;
import org.springframework.web.bind.annotation.*;

@RestController
public class wsOpcionProducto
{
    @RequestMapping(value = "findOpcionProductos")
    public static List<OpcionProducto> findOpcionProductos()
    {
        List<OpcionProducto> opcionProductosList = new ArrayList<OpcionProducto>();
        
        String jpql = "SELECT o FROM OpcionProducto o";
        opcionProductosList = dao.DAOEclipse.findAllByJPQL(jpql);
        
        Collections.sort(opcionProductosList);
        
        return opcionProductosList;
    }
    
    @RequestMapping(value = "guardarOpcionProducto")
    public static boolean guardarOpcionProducto(@RequestParam(value = "strOpcionProducto" , defaultValue = "") String strOpcionProducto)
    {
        boolean guarde = false;
        boolean modoEdit = false;
        
        OpcionProducto opcionProductoDB = null;
        if(strOpcionProducto != null)
        {
            try
            {
                OpcionProducto opcionProductoRecibido = new Gson().fromJson(strOpcionProducto, OpcionProducto.class);
                
                if(opcionProductoRecibido != null)
                {
                    // MODO EDIT:
                    if(opcionProductoRecibido.getId() != -1)
                    {
                        opcionProductoDB = (OpcionProducto) dao.DAOEclipse.get(OpcionProducto.class, opcionProductoRecibido.getId());
                        
                        if(opcionProductoDB != null)
                        {
                            // 0 - ACTUALIZO VALORES DEL OBJ.DB CON LOS DEL OBJ.RECIBIDOS:
                            opcionProductoDB.setNombre(opcionProductoRecibido.getNombre());
                            opcionProductoDB.setSubEncabezado(opcionProductoRecibido.getSubEncabezado());
                            opcionProductoDB.setPrecio(opcionProductoRecibido.getPrecio());
                            opcionProductoDB.setDescripcion(opcionProductoRecibido.getDescripcion());
                            opcionProductoDB.setProductoPadre(opcionProductoRecibido.getProductoPadre());
                            
                            // ---- FOTOSOPCPRODUCTOSLISTS ----
                            // 1 - BORRO TODAS/OS LAS/LOS FOTOSOPCPRODUCTOSLISTS DE ANTES:
                            boolean borreTodos1 = true;
                            for(FotoOpcProducto fotosOpcProductosListLoop: opcionProductoDB.getFotosOpcProductosList())
                            {
                                boolean borreEsta = dao.DAOEclipse.remove(fotosOpcProductosListLoop);
                                if(!borreEsta)
                                {
                                    borreTodos1 = false;
                                }
                            }
                            
                            // 2 - ASOCIO LAS/LOS NUEVAS/OS FOTOSOPCPRODUCTOSLISTS:
                            opcionProductoDB.setFotosOpcProductosList(opcionProductoRecibido.getFotosOpcProductosList());
                                                 
                            opcionProductoDB.setVisible(opcionProductoRecibido.getVisible());
                            if( borreTodos1 )
                            {
                                guarde = dao.DAOEclipse.update(opcionProductoDB);
                            }
                        }
                    }
                    else
                    {
                        // 3 - MODO ADD:
                        
                        // ---- ASOCIO FOTOOPCPRODUCTOS ----
                        for(FotoOpcProducto fotoOpcProductoLoop: opcionProductoRecibido.getFotosOpcProductosList())
                        {
                            fotoOpcProductoLoop.setOpcionProducto(opcionProductoRecibido);
                        }
                        
                        guarde = dao.DAOEclipse.update(opcionProductoRecibido);
                        
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            
        }
        
        return guarde;
    }
    
    @RequestMapping(value = "getOpcionProducto")
    public static OpcionProducto getOpcionProducto(@RequestParam(value = "idOpcionProducto" , defaultValue = "-1") int idOpcionProducto)
    {
        OpcionProducto opcionProductoDB = null;
        
        if(idOpcionProducto != -1)
        {
            opcionProductoDB = (OpcionProducto) dao.DAOEclipse.get(OpcionProducto.class, idOpcionProducto);
        }
        
        return opcionProductoDB;
    }
    @RequestMapping(value = "getOpcionProductoEmpty")
    public OpcionProducto getOpcionProductoEmpty()
    {
       OpcionProducto opcionProductoEmpty = new OpcionProducto();
       opcionProductoEmpty.setId(-1);
       return opcionProductoEmpty;
    }
}
