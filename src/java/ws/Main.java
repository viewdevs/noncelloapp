package ws;


import auxiliar.ModeloOpc;
import auxiliar.ModeloProducto;
import java.util.ArrayList;
import java.util.List;
import modelo.Foto;
import modelo.FotoOpcProducto;
import modelo.Hashtag;
import modelo.OpcionProducto;
import modelo.Persona;
import modelo.Producto;
import modelo.RelProductoHash;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import util.ExcelReader;


@RestController
public class Main
{
    @RequestMapping(value = "importar")
    public static List<Producto> main(String args[])
    {
        System.out.println("iniciando");
        List<Producto> arrProductos = new ArrayList<Producto>();
        
        List<ModeloProducto> lista = null;
        List<ModeloOpc> lista2 = null;
        String ruta = "D:\\desktop\\productos-noncello.xls";
        String ruta2 = "D:\\desktop\\opc-productos-noncello.xls";
        try
        {
            int hoja = 0;
            //System.out.println("LEER:\n" + ExcelReader.leerXLS(ruta,hoja,1) );
            System.out.println("---");
            System.out.println("HEADERS:" + ExcelReader.dameArrayDeHeaders2(ruta, hoja , 1) );
            
            Class clase = Class.forName("auxiliar.ModeloProducto");
            lista = ExcelReader.parearXLS2(ruta,hoja,1,clase, false);
            System.out.println("PARSER:" +  lista.size());
            
            // -- 
            
            Class clase2 = Class.forName("auxiliar.ModeloOpc");
            lista2 = ExcelReader.parearXLS2(ruta2,hoja,1,clase2, false);
            System.out.println("PARSER:" +  lista2.size());
            
            int contador = 1;
            for(ModeloProducto modeloProductoLoop : lista)
            {
                Producto producto = new Producto(modeloProductoLoop.getNombre(), true);
                producto.setId(Integer.parseInt(modeloProductoLoop.getCodigoProducto()));
                System.out.println("PRODUCTO: " + producto.toString());
                    
                for(ModeloOpc modeloOpcionLoop : lista2)
                {
                    // ONLY PROMOS
                    if(modeloProductoLoop.getCodigoProducto().equalsIgnoreCase(modeloOpcionLoop.getFkProducto()))
                    {
                        String nombreOP = modeloOpcionLoop.getNombreOp();
                        String subencabezadoOP = modeloOpcionLoop.getSubencabezado();
                        String strPrecioOP = modeloOpcionLoop.getPrecio();
                        double precioOP = Double.parseDouble(strPrecioOP);
                        String descripcionOP = modeloOpcionLoop.getDescripcion();
                        OpcionProducto opcion = new OpcionProducto(nombreOP, subencabezadoOP, precioOP, descripcionOP, producto, true);
                        opcion.setId(Integer.parseInt(modeloOpcionLoop.getCodigo()));

                        
                        for(int i = 0 ; i < Integer.parseInt(modeloOpcionLoop.getCantFotos()); i ++)
                        {
                            Foto foto = new Foto(modeloOpcionLoop.getCodigo()+"-" + (i+1) +".jpg");
                            FotoOpcProducto fotoOpcProducto = new FotoOpcProducto(foto,opcion,true);
                            opcion.addFotoOpcProducto(fotoOpcProducto);
                        }
                        
                        System.out.println("    op:"   + opcion.toString());
                        producto.addOpcionProducto(opcion);
                    }
                }
                for(String strCategoriaLoop : modeloProductoLoop.dameCategoriasDinamicos())
                {
                    Hashtag hashtagDB = wsHashtag.getHashtagByName(strCategoriaLoop);
                    
                    if(hashtagDB != null)
                    {
                        RelProductoHash rel = new RelProductoHash(hashtagDB, producto, true);
                        producto.addRelProductoHash(rel);
                    }
                }
                
                arrProductos.add(producto);
                dao.DAOEclipse.update(producto);
                System.out.println("------");
                
                contador++;
                
            }
        } 
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return arrProductos;
    }
}
