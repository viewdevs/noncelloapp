package ws;
import com.google.gson.Gson;
import java.util.*;
import modelo.*;
import modelo.FotoOpcProducto;
import org.springframework.web.bind.annotation.*;

@RestController
public class wsFotoOpcProducto
{
    @RequestMapping(value = "findFotoOpcProductos")
    public static List<FotoOpcProducto> findFotoOpcProductos()
    {
        List<FotoOpcProducto> fotoOpcProductosList = new ArrayList<FotoOpcProducto>();
        
        String jpql = "SELECT f FROM FotoOpcProducto f";
        fotoOpcProductosList = dao.DAOEclipse.findAllByJPQL(jpql);
        
        Collections.sort(fotoOpcProductosList);
        
        return fotoOpcProductosList;
    }
    
    @RequestMapping(value = "guardarFotoOpcProducto")
    public static boolean guardarFotoOpcProducto(@RequestParam(value = "strFotoOpcProducto" , defaultValue = "") String strFotoOpcProducto)
    {
        boolean guarde = false;
        boolean modoEdit = false;
        
        FotoOpcProducto fotoOpcProductoDB = null;
        if(strFotoOpcProducto != null)
        {
            try
            {
                FotoOpcProducto fotoOpcProductoRecibido = new Gson().fromJson(strFotoOpcProducto, FotoOpcProducto.class);
                
                if(fotoOpcProductoRecibido != null)
                {
                    // MODO EDIT:
                    if(fotoOpcProductoRecibido.getId() != -1)
                    {
                        fotoOpcProductoDB = (FotoOpcProducto) dao.DAOEclipse.get(FotoOpcProducto.class, fotoOpcProductoRecibido.getId());
                        
                        if(fotoOpcProductoDB != null)
                        {
                            // 0 - ACTUALIZO VALORES DEL OBJ.DB CON LOS DEL OBJ.RECIBIDOS:
                            fotoOpcProductoDB.setFoto(fotoOpcProductoRecibido.getFoto());
                            fotoOpcProductoDB.setOpcionProducto(fotoOpcProductoRecibido.getOpcionProducto());
                            fotoOpcProductoDB.setVisible(fotoOpcProductoRecibido.getVisible());
                            guarde = dao.DAOEclipse.update(fotoOpcProductoDB);
                        }
                    }
                    else
                    {
                        // 3 - MODO ADD:
                        
                        guarde = dao.DAOEclipse.update(fotoOpcProductoRecibido);
                        
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            
        }
        
        return guarde;
    }
    
    @RequestMapping(value = "getFotoOpcProducto")
    public static FotoOpcProducto getFotoOpcProducto(@RequestParam(value = "idFotoOpcProducto" , defaultValue = "-1") int idFotoOpcProducto)
    {
        FotoOpcProducto fotoOpcProductoDB = null;
        
        if(idFotoOpcProducto != -1)
        {
            fotoOpcProductoDB = (FotoOpcProducto) dao.DAOEclipse.get(FotoOpcProducto.class, idFotoOpcProducto);
        }
        
        return fotoOpcProductoDB;
    }
    @RequestMapping(value = "getFotoOpcProductoEmpty")
    public FotoOpcProducto getFotoOpcProductoEmpty()
    {
       FotoOpcProducto fotoOpcProductoEmpty = new FotoOpcProducto();
       fotoOpcProductoEmpty.setId(-1);
       return fotoOpcProductoEmpty;
    }
}
