package ws;
import com.google.gson.Gson;
import java.util.*;
import modelo.*;
import modelo.CambioEstadoPedido;
import org.springframework.web.bind.annotation.*;

@RestController
public class wsCambioEstadoPedido
{
    @RequestMapping(value = "findCambioEstadoPedidos")
    public static List<CambioEstadoPedido> findCambioEstadoPedidos()
    {
        List<CambioEstadoPedido> cambioEstadoPedidosList = new ArrayList<CambioEstadoPedido>();
        
        String jpql = "SELECT c FROM CambioEstadoPedido c";
        cambioEstadoPedidosList = dao.DAOEclipse.findAllByJPQL(jpql);
        
        Collections.sort(cambioEstadoPedidosList);
        
        return cambioEstadoPedidosList;
    }
    
    @RequestMapping(value = "guardarCambioEstadoPedido")
    public static boolean guardarCambioEstadoPedido(@RequestParam(value = "strCambioEstadoPedido" , defaultValue = "") String strCambioEstadoPedido)
    {
        boolean guarde = false;
        boolean modoEdit = false;
        
        CambioEstadoPedido cambioEstadoPedidoDB = null;
        if(strCambioEstadoPedido != null)
        {
            try
            {
                CambioEstadoPedido cambioEstadoPedidoRecibido = new Gson().fromJson(strCambioEstadoPedido, CambioEstadoPedido.class);
                
                if(cambioEstadoPedidoRecibido != null)
                {
                    // MODO EDIT:
                    if(cambioEstadoPedidoRecibido.getId() != -1)
                    {
                        cambioEstadoPedidoDB = (CambioEstadoPedido) dao.DAOEclipse.get(CambioEstadoPedido.class, cambioEstadoPedidoRecibido.getId());
                        
                        if(cambioEstadoPedidoDB != null)
                        {
                            // 0 - ACTUALIZO VALORES DEL OBJ.DB CON LOS DEL OBJ.RECIBIDOS:
                            cambioEstadoPedidoDB.setPedido(cambioEstadoPedidoRecibido.getPedido());
                            cambioEstadoPedidoDB.setEstadoPedidoDisponible(cambioEstadoPedidoRecibido.getEstadoPedidoDisponible());
                            cambioEstadoPedidoDB.setCuando(cambioEstadoPedidoRecibido.getCuando());
                            cambioEstadoPedidoDB.setOperador(cambioEstadoPedidoRecibido.getOperador());
                            guarde = dao.DAOEclipse.update(cambioEstadoPedidoDB);
                        }
                    }
                    else
                    {
                        // 3 - MODO ADD:
                        
                        guarde = dao.DAOEclipse.update(cambioEstadoPedidoRecibido);
                        
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            
        }
        
        return guarde;
    }
    
    @RequestMapping(value = "getCambioEstadoPedido")
    public static CambioEstadoPedido getCambioEstadoPedido(@RequestParam(value = "idCambioEstadoPedido" , defaultValue = "-1") int idCambioEstadoPedido)
    {
        CambioEstadoPedido cambioEstadoPedidoDB = null;
        
        if(idCambioEstadoPedido != -1)
        {
            cambioEstadoPedidoDB = (CambioEstadoPedido) dao.DAOEclipse.get(CambioEstadoPedido.class, idCambioEstadoPedido);
        }
        
        return cambioEstadoPedidoDB;
    }
    @RequestMapping(value = "getCambioEstadoPedidoEmpty")
    public CambioEstadoPedido getCambioEstadoPedidoEmpty()
    {
       CambioEstadoPedido cambioEstadoPedidoEmpty = new CambioEstadoPedido();
       cambioEstadoPedidoEmpty.setId(-1);
       return cambioEstadoPedidoEmpty;
    }
}
