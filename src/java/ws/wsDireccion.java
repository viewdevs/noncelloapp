package ws;
import com.google.gson.Gson;
import java.util.*;
import modelo.*;
import modelo.Direccion;
import org.springframework.web.bind.annotation.*;

@RestController
public class wsDireccion
{
    @RequestMapping(value = "findDireccions")
    public static List<Direccion> findDireccions()
    {
        List<Direccion> direccionsList = new ArrayList<Direccion>();
        
        String jpql = "SELECT d FROM Direccion d";
        direccionsList = dao.DAOEclipse.findAllByJPQL(jpql);
        
        Collections.sort(direccionsList);
        
        return direccionsList;
    }
    
    @RequestMapping(value = "guardarDireccion")
    public static boolean guardarDireccion(@RequestParam(value = "strDireccion" , defaultValue = "") String strDireccion)
    {
        boolean guarde = false;
        boolean modoEdit = false;
        
        Direccion direccionDB = null;
        if(strDireccion != null)
        {
            try
            {
                Direccion direccionRecibido = new Gson().fromJson(strDireccion, Direccion.class);
                
                if(direccionRecibido != null)
                {
                    // MODO EDIT:
                    if(direccionRecibido.getId() != -1)
                    {
                        direccionDB = (Direccion) dao.DAOEclipse.get(Direccion.class, direccionRecibido.getId());
                        
                        if(direccionDB != null)
                        {
                            // 0 - ACTUALIZO VALORES DEL OBJ.DB CON LOS DEL OBJ.RECIBIDOS:
                            direccionDB.setHotel(direccionRecibido.getHotel());
                            direccionDB.setCalle(direccionRecibido.getCalle());
                            direccionDB.setNumero(direccionRecibido.getNumero());
                            direccionDB.setPiso(direccionRecibido.getPiso());
                            direccionDB.setHabitacion(direccionRecibido.getHabitacion());
                            direccionDB.setObservacion(direccionRecibido.getObservacion());
                            guarde = dao.DAOEclipse.update(direccionDB);
                        }
                    }
                    else
                    {
                        // 3 - MODO ADD:
                        
                        guarde = dao.DAOEclipse.update(direccionRecibido);
                        
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            
        }
        
        return guarde;
    }
    
    @RequestMapping(value = "getDireccion")
    public static Direccion getDireccion(@RequestParam(value = "idDireccion" , defaultValue = "-1") int idDireccion)
    {
        Direccion direccionDB = null;
        
        if(idDireccion != -1)
        {
            direccionDB = (Direccion) dao.DAOEclipse.get(Direccion.class, idDireccion);
        }
        
        return direccionDB;
    }
    @RequestMapping(value = "getDireccionEmpty")
    public Direccion getDireccionEmpty()
    {
       Direccion direccionEmpty = new Direccion();
       direccionEmpty.setId(-1);
       return direccionEmpty;
    }
}
