package ws;
import com.google.gson.Gson;
import java.util.*;
import modelo.*;
import modelo.Hashtag;
import org.springframework.web.bind.annotation.*;

@RestController
public class wsHashtag
{
    @RequestMapping(value = "findHashtags")
    public static List<Hashtag> findHashtags()
    {
        List<Hashtag> hashtagsList = new ArrayList<Hashtag>();
        
        String jpql = "SELECT h FROM Hashtag h";
        hashtagsList = dao.DAOEclipse.findAllByJPQL(jpql);
        
        Collections.sort(hashtagsList);
        
        return hashtagsList;
    }
    @RequestMapping(value = "findHashtagsActivos")
    public static List<Hashtag> findHashtagsActivos()
    {
        List<RelProductoHash> listaAux = null;
        List<Hashtag> hashtagsList = new ArrayList<Hashtag>();
        
        String jpql = "SELECT r FROM RelProductoHash r";
        listaAux = dao.DAOEclipse.findAllByJPQL(jpql);
        
        for(RelProductoHash relLoop : listaAux)
        {
            if(relLoop.getActivo())
            {
                boolean loTengo = false;
                for(Hashtag hashLoop : hashtagsList)
                {
                    if(hashLoop.getId() == relLoop.getHashtag().getId())
                    {
                        loTengo = true;
                        break;
                    }
                }
                
                if(!loTengo)
                {
                   hashtagsList.add(relLoop.getHashtag());
                }
            }
        }
        
        Collections.sort(hashtagsList);
        
        return hashtagsList;
    }
    
    @RequestMapping(value = "guardarHashtag")
    public static boolean guardarHashtag(@RequestParam(value = "strHashtag" , defaultValue = "") String strHashtag)
    {
        boolean guarde = false;
        boolean modoEdit = false;
        
        Hashtag hashtagDB = null;
        if(strHashtag != null)
        {
            try
            {
                Hashtag hashtagRecibido = new Gson().fromJson(strHashtag, Hashtag.class);
                
                if(hashtagRecibido != null)
                {
                    // MODO EDIT:
                    if(hashtagRecibido.getId() != -1)
                    {
                        hashtagDB = (Hashtag) dao.DAOEclipse.get(Hashtag.class, hashtagRecibido.getId());
                        
                        if(hashtagDB != null)
                        {
                            // 0 - ACTUALIZO VALORES DEL OBJ.DB CON LOS DEL OBJ.RECIBIDOS:
                            hashtagDB.setHash(hashtagRecibido.getHash());
                            hashtagDB.setSubTexto(hashtagRecibido.getSubTexto());
                            hashtagDB.setFoto(hashtagRecibido.getFoto());
                            guarde = dao.DAOEclipse.update(hashtagDB);
                        }
                    }
                    else
                    {
                        // 3 - MODO ADD:
                        
                        guarde = dao.DAOEclipse.update(hashtagRecibido);
                        
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            
        }
        
        return guarde;
    }
    
    @RequestMapping(value = "getHashtagByName")
    public static Hashtag getHashtagByName(@RequestParam(value = "name" , defaultValue = "-1") String nombre)
    {
        Hashtag hashtagDB = null;
        
        if(nombre != null)
        {
            String jpql = "SELECT h FROM Hashtag h WHERE h.hash = \"" + nombre + "\"";
            
            hashtagDB = (Hashtag) dao.DAOEclipse.getByJPQL(jpql);
        }
        
        return hashtagDB;
    }
    @RequestMapping(value = "getHashtag")
    public static Hashtag getHashtag(@RequestParam(value = "idHashtag" , defaultValue = "-1") int idHashtag)
    {
        Hashtag hashtagDB = null;
        
        if(idHashtag != -1)
        {
            hashtagDB = (Hashtag) dao.DAOEclipse.get(Hashtag.class, idHashtag);
        }
        
        return hashtagDB;
    }
    @RequestMapping(value = "getHashtagEmpty")
    public Hashtag getHashtagEmpty()
    {
       Hashtag hashtagEmpty = new Hashtag();
       hashtagEmpty.setId(-1);
       return hashtagEmpty;
    }
}
