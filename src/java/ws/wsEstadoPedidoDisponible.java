package ws;
import com.google.gson.Gson;
import java.util.*;
import modelo.*;
import modelo.EstadoPedidoDisponible;
import org.springframework.web.bind.annotation.*;

@RestController
public class wsEstadoPedidoDisponible
{
    @RequestMapping(value = "findEstadoPedidoDisponibles")
    public static List<EstadoPedidoDisponible> findEstadoPedidoDisponibles()
    {
        List<EstadoPedidoDisponible> estadoPedidoDisponiblesList = new ArrayList<EstadoPedidoDisponible>();
        
        String jpql = "SELECT e FROM EstadoPedidoDisponible e";
        estadoPedidoDisponiblesList = dao.DAOEclipse.findAllByJPQL(jpql);
        
        Collections.sort(estadoPedidoDisponiblesList);
        
        return estadoPedidoDisponiblesList;
    }
    public static EstadoPedidoDisponible dameEstadoPedidoByName(String nombre)
    {
        EstadoPedidoDisponible estadoPedidoDisponibleDB = new EstadoPedidoDisponible();
        
        String jpql = "SELECT e FROM EstadoPedidoDisponible e WHERE e.nombr = \"" + nombre +"\"";
        estadoPedidoDisponibleDB = (EstadoPedidoDisponible) dao.DAOEclipse.getByJPQL(jpql);
        
        return estadoPedidoDisponibleDB;
    }
    
//    @RequestMapping(value = "guardarEstadoPedidoDisponible")
//    public static boolean guardarEstadoPedidoDisponible(@RequestParam(value = "strEstadoPedidoDisponible" , defaultValue = "") String strEstadoPedidoDisponible)
//    {
//        boolean guarde = false;
//        boolean modoEdit = false;
//        
//        EstadoPedidoDisponible estadoPedidoDisponibleDB = null;
//        if(strEstadoPedidoDisponible != null)
//        {
//            try
//            {
//                EstadoPedidoDisponible estadoPedidoDisponibleRecibido = new Gson().fromJson(strEstadoPedidoDisponible, EstadoPedidoDisponible.class);
//                
//                if(estadoPedidoDisponibleRecibido != null)
//                {
//                    // MODO EDIT:
//                    if(estadoPedidoDisponibleRecibido.getId() != -1)
//                    {
//                        estadoPedidoDisponibleDB = (EstadoPedidoDisponible) dao.DAOEclipse.get(EstadoPedidoDisponible.class, estadoPedidoDisponibleRecibido.getId());
//                        
//                        if(estadoPedidoDisponibleDB != null)
//                        {
//                            // 0 - ACTUALIZO VALORES DEL OBJ.DB CON LOS DEL OBJ.RECIBIDOS:
//                            estadoPedidoDisponibleDB.setNombr(estadoPedidoDisponibleRecibido.getNombr());
//                            guarde = dao.DAOEclipse.update(estadoPedidoDisponibleDB);
//                        }
//                    }
//                    else
//                    {
//                        // 3 - MODO ADD:
//                        
//                        guarde = dao.DAOEclipse.update(estadoPedidoDisponibleRecibido);
//                        
//                    }
//                }
//            }
//            catch(Exception e)
//            {
//                e.printStackTrace();
//            }
//            
//        }
//        
//        return guarde;
//    }
    
    @RequestMapping(value = "getEstadoPedidoDisponible")
    public static EstadoPedidoDisponible getEstadoPedidoDisponible(@RequestParam(value = "idEstadoPedidoDisponible" , defaultValue = "-1") int idEstadoPedidoDisponible)
    {
        EstadoPedidoDisponible estadoPedidoDisponibleDB = null;
        
        if(idEstadoPedidoDisponible != -1)
        {
            estadoPedidoDisponibleDB = (EstadoPedidoDisponible) dao.DAOEclipse.get(EstadoPedidoDisponible.class, idEstadoPedidoDisponible);
        }
        
        return estadoPedidoDisponibleDB;
    }
    @RequestMapping(value = "getEstadoPedidoDisponibleEmpty")
    public EstadoPedidoDisponible getEstadoPedidoDisponibleEmpty()
    {
       EstadoPedidoDisponible estadoPedidoDisponibleEmpty = new EstadoPedidoDisponible();
       estadoPedidoDisponibleEmpty.setId(-1);
       return estadoPedidoDisponibleEmpty;
    }
}
