package ws;
import com.google.gson.Gson;
import java.util.*;
import modelo.*;
import modelo.RelProductoHash;
import org.springframework.web.bind.annotation.*;

@RestController
public class wsRelProductoHash
{
    @RequestMapping(value = "findRelProductoHashs")
    public static List<RelProductoHash> findRelProductoHashs()
    {
        List<RelProductoHash> relProductoHashsList = new ArrayList<RelProductoHash>();
        
        String jpql = "SELECT r FROM RelProductoHash r";
        relProductoHashsList = dao.DAOEclipse.findAllByJPQL(jpql);
        
        Collections.sort(relProductoHashsList);
        
        return relProductoHashsList;
    }
    
    @RequestMapping(value = "guardarRelProductoHash")
    public static boolean guardarRelProductoHash(@RequestParam(value = "strRelProductoHash" , defaultValue = "") String strRelProductoHash)
    {
        boolean guarde = false;
        boolean modoEdit = false;
        
        RelProductoHash relProductoHashDB = null;
        if(strRelProductoHash != null)
        {
            try
            {
                RelProductoHash relProductoHashRecibido = new Gson().fromJson(strRelProductoHash, RelProductoHash.class);
                
                if(relProductoHashRecibido != null)
                {
                    // MODO EDIT:
                    if(relProductoHashRecibido.getId() != -1)
                    {
                        relProductoHashDB = (RelProductoHash) dao.DAOEclipse.get(RelProductoHash.class, relProductoHashRecibido.getId());
                        
                        if(relProductoHashDB != null)
                        {
                            // 0 - ACTUALIZO VALORES DEL OBJ.DB CON LOS DEL OBJ.RECIBIDOS:
                            relProductoHashDB.setHashtag(relProductoHashRecibido.getHashtag());
                            relProductoHashDB.setProducto(relProductoHashRecibido.getProducto());
                            relProductoHashDB.setActivo(relProductoHashRecibido.getActivo());
                            guarde = dao.DAOEclipse.update(relProductoHashDB);
                        }
                    }
                    else
                    {
                        // 3 - MODO ADD:
                        
                        guarde = dao.DAOEclipse.update(relProductoHashRecibido);
                        
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            
        }
        
        return guarde;
    }
    
    @RequestMapping(value = "getRelProductoHash")
    public static RelProductoHash getRelProductoHash(@RequestParam(value = "idRelProductoHash" , defaultValue = "-1") int idRelProductoHash)
    {
        RelProductoHash relProductoHashDB = null;
        
        if(idRelProductoHash != -1)
        {
            relProductoHashDB = (RelProductoHash) dao.DAOEclipse.get(RelProductoHash.class, idRelProductoHash);
        }
        
        return relProductoHashDB;
    }
    @RequestMapping(value = "getRelProductoHashEmpty")
    public RelProductoHash getRelProductoHashEmpty()
    {
       RelProductoHash relProductoHashEmpty = new RelProductoHash();
       relProductoHashEmpty.setId(-1);
       return relProductoHashEmpty;
    }
}
