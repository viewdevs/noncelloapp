package ws;
import com.google.gson.Gson;
import java.util.*;
import modelo.*;
import modelo.Menu;
import modelo.Menu;
import org.springframework.web.bind.annotation.*;

@RestController
public class wsMenu
{
    @RequestMapping(value = "findMenus")
    public static List<Menu> findMenus()
    {
        List<Menu> menusList = new ArrayList<Menu>();
        
        String jpql = "SELECT m FROM Menu m";
        menusList = dao.DAOEclipse.findAllByJPQL(jpql);
        
        Collections.sort(menusList);
        
        return menusList;
    }
    
    @RequestMapping(value = "guardarMenu")
    public static boolean guardarMenu(@RequestParam(value = "strMenu" , defaultValue = "") String strMenu)
    {
        boolean guarde = false;
        boolean modoEdit = false;
        
        Menu menuDB = null;
        if(strMenu != null)
        {
            try
            {
                Menu menuRecibido = new Gson().fromJson(strMenu, Menu.class);
                
                if(menuRecibido != null)
                {
                    // MODO EDIT:
                    if(menuRecibido.getId() != -1)
                    {
                        menuDB = (Menu) dao.DAOEclipse.get(Menu.class, menuRecibido.getId());
                        
                        if(menuDB != null)
                        {
                            // 0 - ACTUALIZO VALORES DEL OBJ.DB CON LOS DEL OBJ.RECIBIDOS:
                            menuDB.setNombre(menuRecibido.getNombre());
                            menuDB.setUrl(menuRecibido.getUrl());
                            guarde = dao.DAOEclipse.update(menuDB);
                        }
                    }
                    else
                    {
                        // 3 - MODO ADD:
                        
                        guarde = dao.DAOEclipse.update(menuRecibido);
                        
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            
        }
        
        return guarde;
    }
    
    @RequestMapping(value = "getMenu")
    public static Menu getMenu(@RequestParam(value = "idMenu" , defaultValue = "-1") int idMenu)
    {
        Menu menuDB = null;
        
        if(idMenu != -1)
        {
            menuDB = (Menu) dao.DAOEclipse.get(Menu.class, idMenu);
        }
        
        return menuDB;
    }
    @RequestMapping(value = "getMenuEmpty")
    public Menu getMenuEmpty()
    {
       Menu menuEmpty = new Menu();
       menuEmpty.setId(-1);
       return menuEmpty;
    }
}
