package ws;
import com.google.gson.Gson;
import java.util.*;
import modelo.*;
import org.springframework.web.bind.annotation.*;

@RestController
public class wsProducto
{
    @RequestMapping(value = "findProductos")
    public static List<Producto> findProductos()
    {
        List<Producto> productosList = new ArrayList<Producto>();
        
        String jpql = "SELECT p FROM Producto p";
        productosList = dao.DAOEclipse.findAllByJPQL(jpql);
        
        Collections.sort(productosList);
        
        return productosList;
    }
    
    @RequestMapping(value = "findPromos")
    public static List<Producto> findPromos()
    {
        List<Producto> productosList = new ArrayList<Producto>();
        
        String jpql = "SELECT p FROM Producto p WHERE p.esPromo = 1";
        productosList = dao.DAOEclipse.findAllByJPQL(jpql);
        
        Collections.sort(productosList);
        
        return productosList;
    }
    
    @RequestMapping(value = "findProductosByHash")
    public static List<Producto> findProductosByHash(@RequestParam(value = "fkHash") int fkHash)
    {
        List<Producto> productosList = new ArrayList<Producto>();
        List<Producto> productosListAux = new ArrayList<Producto>();
        
        String jpql = "SELECT p FROM Producto p";
        productosListAux = dao.DAOEclipse.findAllByJPQL(jpql);
        
        
        for(Producto productoLoop : productosListAux)
        {
            for(RelProductoHash relHashLoop : productoLoop.getRelshashtagsList())
            {
                if(relHashLoop.getHashtag().getId() == fkHash)
                {
                    productosList.add(productoLoop);
                }
            }
        }
        
        
        Collections.sort(productosList);
        
        return productosList;
    }
    
    @RequestMapping(value = "guardarProducto")
    public static boolean guardarProducto(@RequestParam(value = "strProducto" , defaultValue = "") String strProducto)
    {
        boolean guarde = false;
        boolean modoEdit = false;
        
        Producto productoDB = null;
        if(strProducto != null)
        {
            try
            {
                Producto productoRecibido = new Gson().fromJson(strProducto, Producto.class);
                
                if(productoRecibido != null)
                {
                    // MODO EDIT:
                    if(productoRecibido.getId() != -1)
                    {
                        productoDB = (Producto) dao.DAOEclipse.get(Producto.class, productoRecibido.getId());
                        
                        if(productoDB != null)
                        {
                            // 0 - ACTUALIZO VALORES DEL OBJ.DB CON LOS DEL OBJ.RECIBIDOS:
                            productoDB.setNombre(productoRecibido.getNombre());
                            
                            // ---- OPCIONESLISTS ----
                            // 1 - BORRO TODAS/OS LAS/LOS OPCIONESLISTS DE ANTES:
                            boolean borreTodos1 = true;
                            for(OpcionProducto opcionesListLoop: productoDB.getOpcionesList())
                            {
                                boolean borreEsta = dao.DAOEclipse.remove(opcionesListLoop);
                                if(!borreEsta)
                                {
                                    borreTodos1 = false;
                                }
                            }
                            
                            // 2 - ASOCIO LAS/LOS NUEVAS/OS OPCIONESLISTS:
                            productoDB.setOpcionesList(productoRecibido.getOpcionesList());
                                                 
                            
                            // ---- RELSHASHTAGSLISTS ----
                            // 2 - BORRO TODAS/OS LAS/LOS RELSHASHTAGSLISTS DE ANTES:
                            boolean borreTodos2 = true;
                            for(RelProductoHash relshashtagsListLoop: productoDB.getRelshashtagsList())
                            {
                                boolean borreEsta = dao.DAOEclipse.remove(relshashtagsListLoop);
                                if(!borreEsta)
                                {
                                    borreTodos2 = false;
                                }
                            }
                            
                            // 3 - ASOCIO LAS/LOS NUEVAS/OS RELSHASHTAGSLISTS:
                            productoDB.setRelshashtagsList(productoRecibido.getRelshashtagsList());
                                                 
                            productoDB.setEsPromo(productoRecibido.getEsPromo());
//                            productoDB.setVisible(productoRecibido.getVisible());
                            if( borreTodos1 &&  borreTodos2 )
                            {
                                guarde = dao.DAOEclipse.update(productoDB);
                            }
                        }
                    }
                    else
                    {
                        // 3 - MODO ADD:
                        
                        // ---- ASOCIO OPCIONPRODUCTOS ----
                        for(OpcionProducto opcionProductoLoop: productoRecibido.getOpcionesList())
                        {
                            opcionProductoLoop.setProductoPadre(productoRecibido);
                        }
                        
                        // ---- ASOCIO RELPRODUCTOHASHS ----
                        for(RelProductoHash relProductoHashLoop: productoRecibido.getRelshashtagsList())
                        {
                            relProductoHashLoop.setProducto(productoRecibido);
                        }
                        
                        guarde = dao.DAOEclipse.update(productoRecibido);
                        
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            
        }
        
        return guarde;
    }
    
    @RequestMapping(value = "getProducto")
    public static Producto getProducto(@RequestParam(value = "idProducto" , defaultValue = "-1") int idProducto)
    {
        Producto productoDB = null;
        
        if(idProducto != -1)
        {
            productoDB = (Producto) dao.DAOEclipse.get(Producto.class, idProducto);
        }
        
        return productoDB;
    }
    @RequestMapping(value = "getProductoEmpty")
    public Producto getProductoEmpty()
    {
       Producto productoEmpty = new Producto();
       productoEmpty.setId(-1);
       return productoEmpty;
    }
}
