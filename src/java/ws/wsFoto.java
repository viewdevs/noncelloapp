package ws;
import com.google.gson.Gson;
import java.util.*;
import modelo.*;
import modelo.Foto;
import org.springframework.web.bind.annotation.*;

@RestController
public class wsFoto
{
    @RequestMapping(value = "findFotos")
    public static List<Foto> findFotos()
    {
        List<Foto> fotosList = new ArrayList<Foto>();
        
        String jpql = "SELECT f FROM Foto f";
        fotosList = dao.DAOEclipse.findAllByJPQL(jpql);
        
        Collections.sort(fotosList);
        
        return fotosList;
    }
    
    @RequestMapping(value = "guardarFoto")
    public static boolean guardarFoto(@RequestParam(value = "strFoto" , defaultValue = "") String strFoto)
    {
        boolean guarde = false;
        boolean modoEdit = false;
        
        Foto fotoDB = null;
        if(strFoto != null)
        {
            try
            {
                Foto fotoRecibido = new Gson().fromJson(strFoto, Foto.class);
                
                if(fotoRecibido != null)
                {
                    // MODO EDIT:
                    if(fotoRecibido.getId() != -1)
                    {
                        fotoDB = (Foto) dao.DAOEclipse.get(Foto.class, fotoRecibido.getId());
                        
                        if(fotoDB != null)
                        {
                            // 0 - ACTUALIZO VALORES DEL OBJ.DB CON LOS DEL OBJ.RECIBIDOS:
                            fotoDB.setUrlProvisoria(fotoRecibido.getUrlProvisoria());
                            guarde = dao.DAOEclipse.update(fotoDB);
                        }
                    }
                    else
                    {
                        // 3 - MODO ADD:
                        
                        guarde = dao.DAOEclipse.update(fotoRecibido);
                        
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            
        }
        
        return guarde;
    }
    
    @RequestMapping(value = "getFoto")
    public static Foto getFoto(@RequestParam(value = "idFoto" , defaultValue = "-1") int idFoto)
    {
        Foto fotoDB = null;
        
        if(idFoto != -1)
        {
            fotoDB = (Foto) dao.DAOEclipse.get(Foto.class, idFoto);
        }
        
        return fotoDB;
    }
    @RequestMapping(value = "getFotoDefault")
    public static Foto getFotoDefault()
    {
        Foto fotoDefault = null;
        
        String jpql = "SELECT f FROM Foto f WHERE f.id = 666";
        fotoDefault = (Foto) dao.DAOEclipse.getByJPQL(jpql);
        
        return fotoDefault;
    }
    @RequestMapping(value = "getFotoEmpty")
    public Foto getFotoEmpty()
    {
       Foto fotoEmpty = new Foto();
       fotoEmpty.setId(-1);
       return fotoEmpty;
    }
}
