package ws;

import com.google.gson.Gson;
import controller.MasterController;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import modelo.Carrito;
import modelo.Configuracion;
import modelo.Direccion;
import modelo.Foto;
import modelo.Persona;
import org.springframework.web.bind.annotation.*;

@RestController
public class wsLogin
{
    public static List<Carrito> carritosList;
    
    
    @RequestMapping(value = "verCodigoDeSession")
    public static String verCodigoDeSession(HttpServletRequest request)
    {
//        Object atributoCrudo = request.getSession().getAttribute(codigoSalida);
        
        String codigoDeSession = null;
        
        if(request.getSession().getAttribute("codigoCarrito") != null)
        {
            codigoDeSession = request.getSession().getAttribute("codigoCarrito").toString();
        }
        return codigoDeSession;
    }
    @RequestMapping(value = "generarCodigoSesion")
    public static String generarCodigoSesion(HttpServletRequest request)
    {
        String codigoSalida = verCodigoDeSession(request);
        
        // 1 - ATRIBUTO DE SESION EN CRUDO:
        if(codigoSalida != null)
        {
            // 2 - ATRIBUTO EN STRING:
            System.out.println("codigoCarrito DE SESSION: " + codigoSalida);
        }
        else
        {
            // 3 - SI NO TENGO CODIGO, LE GENERO UNO AGREGO UN CARRITO A LA LISTA Y DEVUELVO LA VARIABLE DE SESSION:
            String codigoSesionGenerado = generarCodigoCuatroDigitos();
            carritosList.add(new Carrito(codigoSesionGenerado));
            codigoSalida = codigoSesionGenerado;
        }
        
        request.getSession().setAttribute("codigoCarrito", codigoSalida);
        
        
        return codigoSalida;
    }
    @RequestMapping(value = "cerrarCodigoDeSession")
    public static String cerrarCodigoDeSession(HttpServletRequest request)
    {
        request.getSession().setAttribute("codigoCarrito", null);
        
        return verCodigoDeSession(request);
    }
    @RequestMapping(value = "dameCarritoSegunCodigoDeSesion")
    public static Carrito dameCarritoSegunCodigoDeSesion(HttpServletRequest request)
    {
        Carrito carritoSalida = null;
        
        // 1 - TRAIGO MI CODIGO DE SESION DE REQUEST:
        String codigoDeSesion = verCodigoDeSession(request);
        
        // 2 - SI NO TENGO CODIGO DE SEDDION GENERO UNO PARA TENER UN CARRITO:
        if(codigoDeSesion == null)
        {
            codigoDeSesion = generarCodigoSesion(request);
        }
        if(carritosList == null)
        {
            carritosList = new ArrayList<>();
        }
        
        
        // 3 - BUSCO MI CARRITO:
        if(codigoDeSesion != null && carritosList != null)
        {
            for(Carrito carritoLoop : carritosList)
            {
                if(carritoLoop.getCodigoSesion().equalsIgnoreCase(codigoDeSesion))
                {
                    carritoSalida = carritoLoop;
                    break;
                }
            }
            if(carritoSalida == null)
            {
                request.getSession().setAttribute("codigoCarrito", null);
                generarCodigoSesion(request);
                carritoSalida = dameCarritoSegunCodigoDeSesion(request);
            }
        }
        
//        if(carritoSalida == null)
//        {
//            codigoDeSesion = generarCodigoSesion(request);
//        }
        
        return carritoSalida;
    }
//    @RequestMapping(value = "generarCodigoSinRepetir")
//    public static String generarCodigoSinRepetir()
//    {
//        String codigoGenerado = "";
//        boolean yaLoTengo = false;
//        for(Carrito carritoLoop : carritosList)
//        {
//            if(carritoLoop.getCodigoSesion().equalsIgnoreCase(codigoSesionGenerado))
//            {
//                yaLoTengo = true;
//                break;
//            }
//        }
//
//        // 7 - SI YA TENGO EL CODIGO, GENERO UNO NUEVO
//        if(yaLoTengo)
//        {
//            codigoSesionGenerado = generarCodigoCuatroDigitos();
//        }
//        else
//        {
//            // 8 - SI TENGO EL CODIGO BIEN GENERADO, AGREGO UN CARRITO A LA LISTA Y DEVUELVO LA VARIABLE DE SESSION:
//            carritosList.add(new Carrito(codigoSesionGenerado));
//            codigoSalida = codigoSesionGenerado;
//        }
//    }
    
    @RequestMapping(value = "verListadoCarritos")
    public static List<Carrito> verListadoCarritos(HttpServletRequest request)
    {
        if(carritosList == null)
        {
            carritosList = new ArrayList<Carrito>();
        }
        return carritosList;
    }
//    @RequestMapping(value = "generarCodigoCuatroDigitos")
    private static String generarCodigoCuatroDigitos()
    {
        String codigoGenerado = "";
        boolean yaLoTengo = true;
        
        if(carritosList == null)
        {
            carritosList = new ArrayList<>();
        }
        
        while(yaLoTengo)
        {
            
            // 1 - GENERO CODIGO:
            codigoGenerado = "";
            for(int i = 0  ; i < 4 ;i++)
            {
                int numeroAleatorio = (int) (Math.random() * 9 ) + 1;
                codigoGenerado += numeroAleatorio;
            }
            
            // 2 - COMPRUEBO SI YA EXISTE EN EL LISTADO:
            if(carritosList != null)
            {
                yaLoTengo = false;
                for(Carrito carritoLoop : carritosList)
                {
                    if(carritoLoop.getCodigoSesion().equalsIgnoreCase(codigoGenerado))
                    {
                        yaLoTengo = true;
                        break;
                    }
                }
            }
        }
        
        
        return codigoGenerado;
    }
    
    @RequestMapping(value = "logearPersona")
    public static Persona logearPersona
    (
        @RequestParam(value = "email") String email,
        @RequestParam(value = "pass") String pass,
        HttpServletRequest request
    )
    {
        Persona personaDB = null;
        
        if(email != null && pass != null)
        {
            if(MasterController.dameConfigMaster() != null)
            {
                personaDB = ws.wsPersona.getPersonaByEmailAndPass(email, pass);
                
                if(personaDB != null)
                {
//                    String strPersonaJSON = personaDB.toJson();
                    request.getSession().setAttribute(MasterController.dameConfigMaster().getVarSessionHTTP(), personaDB.getEmail());
                    System.out.println("SESIONANDO USUARIO: " + personaDB.toJson());
                }
            }
        }
       
        
        return personaDB;
    }
    @RequestMapping(value = "damePersonaLogeada")
    public static Persona damePersonaLogeada(HttpServletRequest request)
    {
        Persona personaDB = wsPersona.getPersonaEmpty();
        
        Configuracion config = MasterController.dameConfigMaster();
        if( config != null )
        {
            String emailUsuarioSesionado = (String) request.getSession().getAttribute(config.getVarSessionHTTP());
            if(emailUsuarioSesionado != null)
            {
                if(emailUsuarioSesionado.trim().length() != 0)
                {
                    personaDB = wsPersona.getPersonaByEmail(emailUsuarioSesionado);
                }
            }
        }
        
        return personaDB;
    }
    
    @RequestMapping(value = "exit")
    public static boolean exit(HttpServletRequest request)
    {
        boolean deslogeado = false;
        
        Configuracion config = MasterController.dameConfigMaster();
        if(config != null)
        {
            request.getSession().setAttribute(config.getVarSessionHTTP(), "");
        }        
        deslogeado = true;
        
        return deslogeado;
    }
    /*
    public static boolean existeCliente(String email)
    {
        boolean yaExiste = false;
        
        Cliente clienteDB = (Cliente) dao.DAOEclipse.getByJPQL("SELECT c FROM Cliente c WHERE c.email = '" + email + "'");
        
        if(clienteDB != null)
        {
            yaExiste = true;
        }
        
        return yaExiste;
    }*/
    @RequestMapping(value = "altaCliente",method = RequestMethod.POST)
    public static boolean altaCliente
    (
        @RequestParam(value = "nombre" ,required = true) String nombre,
        @RequestParam(value = "apellido" ,required = true) String apellido,
        @RequestParam(value = "email",required = true) String email,
        @RequestParam(value = "pass",required = true) String pass,
        @RequestParam(value = "telefono",required = true) String telefono,
        @RequestParam(value = "hotel" , defaultValue = "NONE") String hotel,
        @RequestParam(value = "calle" , defaultValue = "NONE") String calle,
        @RequestParam(value = "numero", defaultValue = "NONE") String numero,
        @RequestParam(value = "piso", defaultValue = "NONE") String piso,
        @RequestParam(value = "habitacion", defaultValue = "NONE") String habitacion,
        @RequestParam(value = "observacion", defaultValue = "NONE") String observaciones,
        HttpServletRequest request
    )
    {
        boolean ok = false;
        
        //Direccion direccionEmpty = ws.wsDireccion.getDireccion(-1);
        Direccion direccionPosta = new Direccion(hotel, calle, numero, piso, habitacion, observaciones);
        Foto fotoDefault = wsFoto.getFotoDefault();
        //fotoEmpty.setUrlProvisoria("default.png");
        Persona persona = new Persona(nombre, apellido, email, pass, telefono, direccionPosta, fotoDefault, true, false, false, "CLIENTE","1234");
        String codigoGenerado = generarCodigoAleatorio();
        //persona.setCodigo(codigoGenerado);

        ok = dao.DAOEclipse.update(persona);
        
        logearPersona(email, pass, request);
        
        return ok;
    }
    public static String generarCodigoAleatorio()
    {
        String salida = "";
        
        for(int i = 0 ;  i < 4 ; i ++)
        {
            int numeroRandom = (int) ((Math.random() * 10));
            salida += numeroRandom;
        }
        
        return salida;
    }
}
