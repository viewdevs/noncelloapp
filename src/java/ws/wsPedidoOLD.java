package ws;
import com.google.gson.Gson;
import controller.MasterController;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import modelo.*;
import modelo.Pedido;
import org.springframework.web.bind.annotation.*;

@RestController
public class wsPedidoOLD
{
//    @RequestMapping(value = "agregarDetalleAMiPedido")
    public static Pedido agregarDetalleAMiPedido
    (
        @RequestParam(value = "idOpcionProducto", defaultValue = "-1") int idOpcionProducto,
        @RequestParam(value = "cantidad", defaultValue = "1") int cantidad,
        HttpServletRequest request
    )
    {
        Pedido pedido = damePedidoEstoyHaciendo(request);
        
        if(idOpcionProducto != -1)
        {
            OpcionProducto opcionProductoDB = ws.wsOpcionProducto.getOpcionProducto(idOpcionProducto);
            if(opcionProductoDB != null)
            {
                pedido.addDetallePedido(new DetallePedido(pedido, opcionProductoDB, cantidad));
                
                double monto = pedido.getPrecioCongelado() + (opcionProductoDB.getPrecio() * cantidad);
                pedido.setPrecioCongelado(monto);
                
                dao.DAOEclipse.update(pedido);
            }
        }
        
        
        
        return pedido;
    }
//    @RequestMapping(value = "quitarDetalleAMiPedido")
    public static Pedido quitarDetalleAMiPedido
    (
        @RequestParam(value = "idDetalle", defaultValue = "-1") int idDetalle,
        HttpServletRequest request
    )
    {
        Pedido pedido = damePedidoEstoyHaciendo(request);
        
        if(idDetalle != -1)
        {
            for(DetallePedido detalleLoop : pedido.getDetallesList())
            {
                if(detalleLoop.getId() == idDetalle)
                {
                    pedido.rmDetallePedido(detalleLoop);
                    
                    double monto = pedido.getPrecioCongelado() - (detalleLoop.getOpcionProducto().getPrecio() * detalleLoop.getCantidad());
                    if(monto < 0 )
                    {
                        monto = 0;
                    }
                        
                    pedido.setPrecioCongelado(monto);
                    
                    dao.DAOEclipse.update(pedido);
                    dao.DAOEclipse.remove(detalleLoop);
                }
            }
            
        }
        
        
        
        return pedido;
    }
//    @RequestMapping(value = "damePedidoEstoyHaciendo")
    public static Pedido damePedidoEstoyHaciendo(HttpServletRequest request)
    {
        Pedido pedidoActual = getPedidoEmpty();
        List<Pedido> pedidosList = new ArrayList<Pedido>();
        SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd/MM/yyyy"); 
        
        Persona personaLogeada = wsLogin.damePersonaLogeada(request);
        
        if(personaLogeada != null)
        {
            String jpql = "SELECT p FROM Pedido p WHERE p.cliente.id = " + personaLogeada.getId();
            pedidosList = dao.DAOEclipse.findAllByJPQL(jpql);
            
            if(pedidosList != null)
            {
                if(pedidosList.size() > 0)
                {
                    Date sieteDiasMenos = MasterController.dameFechaDeXDiasMenos(7);
                    System.out.println("7 DIAS ATRAS: " + formateadorFecha.format(sieteDiasMenos));
                    
                    for(Pedido pedidoLoop : pedidosList)
                    {
                        System.out.println("    FECHA PEDIDO: " + formateadorFecha.format(pedidoLoop.getFecha()));
                        
                        if(pedidoLoop.getFecha().getTime() > sieteDiasMenos.getTime())
                        {
                            CambioEstadoPedido ultimoCambioEstado = pedidoLoop.getUltimoCambioEstado();
                            if(ultimoCambioEstado != null)
                            {
                                if(ultimoCambioEstado.getEstadoPedidoDisponible().getNombr().equalsIgnoreCase("COMPRANDO") )
                                {
                                    pedidoActual = pedidoLoop;
                                }
                            }
                        }
                    }
                }
            }
            
            if(pedidoActual == null)
            {
                Date ahora = new Date();
                Direccion direccion = personaLogeada.getDireccion();
                if(direccion != null)
                {
                    pedidoActual = new Pedido(ahora, personaLogeada, personaLogeada, direccion, 0);
                    EstadoPedidoDisponible estadoPedidoDisponibleDBComprando = wsEstadoPedidoDisponible.dameEstadoPedidoByName("COMPRANDO");
                    pedidoActual.addCambioEstadoPedido(new CambioEstadoPedido(pedidoActual, estadoPedidoDisponibleDBComprando, ahora, personaLogeada));
                }
            }
        }
        
        
        
        return pedidoActual;
    }
//    @RequestMapping(value = "dameMisPedidos")
    public static List<Pedido> dameMisPedidos(HttpServletRequest request)
    {
        List<Pedido> pedidosList = new ArrayList<Pedido>();
        
        Persona personaLogeada = wsLogin.damePersonaLogeada(request);
        
        if(personaLogeada != null)
        {
            String jpql = "SELECT p FROM Pedido p WHERE p.cliente.id = " + personaLogeada.getId();
            pedidosList = dao.DAOEclipse.findAllByJPQL(jpql);

            Collections.sort(pedidosList);
        }
        
        return pedidosList;
    }
//    @RequestMapping(value = "findPedidos")
    public static List<Pedido> findPedidos()
    {
        List<Pedido> pedidosList = new ArrayList<Pedido>();
        
        String jpql = "SELECT p FROM Pedido p";
        pedidosList = dao.DAOEclipse.findAllByJPQL(jpql);
        
        Collections.sort(pedidosList);
        
        return pedidosList;
    }
    
    
//    @RequestMapping(value = "generarPedido")
//    public static boolean guardarPedido(@RequestParam(value = "strPedido" , defaultValue = "") String strPedido)
            
//    @RequestMapping(value = "guardarPedido")
    public static boolean guardarPedido(@RequestParam(value = "strPedido" , defaultValue = "") String strPedido)
    {
        boolean guarde = false;
        boolean modoEdit = false;
        
        Pedido pedidoDB = null;
        if(strPedido != null)
        {
            try
            {
                Pedido pedidoRecibido = new Gson().fromJson(strPedido, Pedido.class);
                
                if(pedidoRecibido != null)
                {
                    // MODO EDIT:
                    if(pedidoRecibido.getId() != -1)
                    {
                        pedidoDB = (Pedido) dao.DAOEclipse.get(Pedido.class, pedidoRecibido.getId());
                        
                        if(pedidoDB != null)
                        {
                            // 0 - ACTUALIZO VALORES DEL OBJ.DB CON LOS DEL OBJ.RECIBIDOS:
                            pedidoDB.setFecha(pedidoRecibido.getFecha());
                            pedidoDB.setReferido(pedidoRecibido.getReferido());
                            pedidoDB.setCliente(pedidoRecibido.getCliente());
                            
                            // ---- DETALLESLISTS ----
                            // 1 - BORRO TODAS/OS LAS/LOS DETALLESLISTS DE ANTES:
                            boolean borreTodos1 = true;
                            for(DetallePedido detallesListLoop: pedidoDB.getDetallesList())
                            {
                                boolean borreEsta = dao.DAOEclipse.remove(detallesListLoop);
                                if(!borreEsta)
                                {
                                    borreTodos1 = false;
                                }
                            }
                            
                            // 2 - ASOCIO LAS/LOS NUEVAS/OS DETALLESLISTS:
                            pedidoDB.setDetallesList(pedidoRecibido.getDetallesList());
                                                 
                            
                            // ---- CAMBIOSESTADOLISTS ----
                            // 2 - BORRO TODAS/OS LAS/LOS CAMBIOSESTADOLISTS DE ANTES:
                            boolean borreTodos2 = true;
                            for(CambioEstadoPedido cambiosEstadoListLoop: pedidoDB.getCambiosEstadoList())
                            {
                                boolean borreEsta = dao.DAOEclipse.remove(cambiosEstadoListLoop);
                                if(!borreEsta)
                                {
                                    borreTodos2 = false;
                                }
                            }
                            
                            // 3 - ASOCIO LAS/LOS NUEVAS/OS CAMBIOSESTADOLISTS:
                            pedidoDB.setCambiosEstadoList(pedidoRecibido.getCambiosEstadoList());
                                                 
                            pedidoDB.setDireccionEntrega(pedidoRecibido.getDireccionEntrega());
                            pedidoDB.setPrecioCongelado(pedidoRecibido.getPrecioCongelado());
                            if( borreTodos1 &&  borreTodos2 )
                            {
                                guarde = dao.DAOEclipse.update(pedidoDB);
                            }
                        }
                    }
                    else
                    {
                        // 3 - MODO ADD:
                        
                        // ---- ASOCIO DETALLEPEDIDOS ----
                        for(DetallePedido detallePedidoLoop: pedidoRecibido.getDetallesList())
                        {
                            detallePedidoLoop.setPedido(pedidoRecibido);
                        }
                        
                        // ---- ASOCIO CAMBIOESTADOPEDIDOS ----
                        for(CambioEstadoPedido cambioEstadoPedidoLoop: pedidoRecibido.getCambiosEstadoList())
                        {
                            cambioEstadoPedidoLoop.setPedido(pedidoRecibido);
                        }
                        
                        guarde = dao.DAOEclipse.update(pedidoRecibido);
                        
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            
        }
        
        return guarde;
    }
    
//    @RequestMapping(value = "getPedido")
    public static Pedido getPedido(@RequestParam(value = "idPedido" , defaultValue = "-1") int idPedido)
    {
        Pedido pedidoDB = null;
        
        if(idPedido != -1)
        {
            pedidoDB = (Pedido) dao.DAOEclipse.get(Pedido.class, idPedido);
        }
        
        return pedidoDB;
    }
//    @RequestMapping(value = "getPedidoEmpty")
    public static Pedido getPedidoEmpty()
    {
       Pedido pedidoEmpty = new Pedido();
       pedidoEmpty.setId(-1);
       return pedidoEmpty;
    }
}
