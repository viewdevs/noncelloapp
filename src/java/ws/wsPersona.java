package ws;
import com.google.gson.Gson;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import modelo.*;
import modelo.Persona;
import org.springframework.web.bind.annotation.*;

@RestController
public class wsPersona
{
    @RequestMapping(value = "findPersonas")
    public static List<Persona> findPersonas(HttpServletRequest request)
    {
        List<Persona> personasList = new ArrayList<Persona>();
        
        String jpql = "SELECT p FROM Persona p";
        
        Persona personaLogeada = wsLogin.damePersonaLogeada(request);
        if(personaLogeada != null)
        {
            if(personaLogeada.getEsOperador())
            {
                personasList = dao.DAOEclipse.findAllByJPQL(jpql);
                Collections.sort(personasList);
            }
        }
        
        
        return personasList;
    }
    @RequestMapping(value = "findReferidos")
    public static List<Persona> findReferidos()
    {
        List<Persona> personasList = new ArrayList<Persona>();
        
        String jpql = "SELECT p FROM Persona p WHERE p.esReferido = true";
        personasList = dao.DAOEclipse.findAllByJPQL(jpql);
        
        Collections.sort(personasList);
        
        return personasList;
    }
    
    @RequestMapping(value = "guardarPersona")
    public static boolean guardarPersona(@RequestParam(value = "strPersona" , defaultValue = "") String strPersona)
    {
        boolean guarde = false;
        boolean modoEdit = false;
        
        Persona personaDB = null;
        if(strPersona != null)
        {
            try
            {
                Persona personaRecibido = new Gson().fromJson(strPersona, Persona.class);
                
                if(personaRecibido != null)
                {
                    // SI NO EXISTE ESTA PERSONA:
                    if(getPersonaByEmail(personaRecibido.getEmail()) == null)
                    {
                        // MODO EDIT:
                        if(personaRecibido.getId() != -1)
                        {
                            personaDB = (Persona) dao.DAOEclipse.get(Persona.class, personaRecibido.getId());

                            if(personaDB != null)
                            {
                                // 0 - ACTUALIZO VALORES DEL OBJ.DB CON LOS DEL OBJ.RECIBIDOS:
                                personaDB.setNombre(personaRecibido.getNombre());
                                personaDB.setApellido(personaRecibido.getApellido());
                                personaDB.setEmail(personaRecibido.getEmail());
                                personaDB.setPass(personaRecibido.getPass());
                                personaDB.setTelefono(personaRecibido.getTelefono());
                                personaDB.setDireccion(personaRecibido.getDireccion());
                                personaDB.setFoto(personaRecibido.getFoto());
                                personaDB.setEsCliente(personaRecibido.getEsCliente());
                                personaDB.setEsReferido(personaRecibido.getEsReferido());
                                personaDB.setEsOperador(personaRecibido.getEsOperador());
                                personaDB.setObservaciones(personaRecibido.getObservaciones());
                                guarde = dao.DAOEclipse.update(personaDB);
                            }
                        }
                        else
                        {
                            // 3 - MODO ADD:
                            guarde = dao.DAOEclipse.update(personaRecibido);

                        }
                    }
                    
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            
        }
        
        return guarde;
    }
    
    @RequestMapping(value = "getPersona")
    public static Persona getPersona(@RequestParam(value = "idPersona" , defaultValue = "-1") int idPersona)
    {
        Persona personaDB = null;
        
        if(idPersona != -1)
        {
            personaDB = (Persona) dao.DAOEclipse.get(Persona.class, idPersona);
        }
        
        return personaDB;
    }
    @RequestMapping(value = "getPersonaByEmailAndPass")
    public static Persona getPersonaByEmailAndPass
    (
        @RequestParam(value = "email") String email,
        @RequestParam(value = "pass") String pass
    )
    {
        Persona personaDB = null;
        
        String jpql = "SELECT p FROM Persona p WHERE p.email = \"" + email +"\" AND p.pass = \"" + pass + "\"";
        
        personaDB = (Persona) dao.DAOEclipse.getByJPQL(jpql);
        
        return personaDB;
    }
    @RequestMapping(value = "getPersonaByEmail")
    public static Persona getPersonaByEmail
    (
        @RequestParam(value = "email") String email
    )
    {
        Persona personaDB = null;
        
        String jpql = "SELECT p FROM Persona p WHERE p.email = \"" + email +"\"";
        
        personaDB = (Persona) dao.DAOEclipse.getByJPQL(jpql);
        
        return personaDB;
    }
    @RequestMapping(value = "getReferidoByCodigo")
    public static Persona getReferidoByCodigo
    (
        @RequestParam(value = "codigoReferencial") String codigoReferencial
    )
    {
        Persona personaDB = null;
        
        String jpql = "SELECT p FROM Persona p WHERE p.codigoReferencial = \"" + codigoReferencial +"\" AND p.esReferido = true";
        
        personaDB = (Persona) dao.DAOEclipse.getByJPQL(jpql);
        
        return personaDB;
    }
    @RequestMapping(value = "getPersonaEmpty")
    public static Persona getPersonaEmpty()
    {
       Persona personaEmpty = new Persona();
       personaEmpty.setId(-1);
       personaEmpty.setFoto(new Foto("default.jpg"));
       return personaEmpty;
    }
}
