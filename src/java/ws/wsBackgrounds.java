package ws;
import com.google.gson.Gson;
import java.util.*;
import modelo.*;
import modelo.Menu;
import modelo.Menu;
import org.springframework.web.bind.annotation.*;

@RestController
public class wsBackgrounds
{
    
    @RequestMapping(value = "dameBg")
    public static String dameBg(@RequestParam(value = "bg" , defaultValue = "") String bg)
    {
        String fullBG = wsConfiguraciones.dameConfigActual().getUrlVisualizacionBackground() +"/"+ bg;
        return fullBG;
    }
}
