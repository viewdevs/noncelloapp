
import auxiliar.ModeloOpc;
import auxiliar.ModeloProducto;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import modelo.Foto;
import modelo.FotoOpcProducto;
import modelo.OpcionProducto;
import modelo.Persona;
import modelo.Producto;
import org.springframework.web.bind.annotation.RestController;
import util.ExcelReader;

public class Main
{
    public static void main(String args[])
    {
        System.out.println("iniciando");

        // 1 - TRAIGO LA FECHA DE HOY:
        Date hoy = new Date();
        
        // 2 - CONVIERTO LA FECHA DE HOY AL FORMATO DE CALENDAR:
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String strHoy = dateFormat.format(hoy);

        // 3 - CALCULO CON CALENDAR 7 DIAS MENOS:
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -7);
        
        // 4 - VUELVO A CREAR UN DATE CON EL DATE DE CALENDAR:
        Date sieteDiasMenos = cal.getTime();    
        String strSieteDiasMenos = dateFormat.format(sieteDiasMenos);
        
//        DateTime lastWeek = new DateTime().minusDays(7);
        
        System.out.println("7 dias menos: " + sieteDiasMenos);
//        
//        List<ModeloProducto> lista = null;
//        List<ModeloOpc> lista2 = null;
//        String ruta = "D:\\desktop\\productos-noncello.xls";
//        String ruta2 = "D:\\desktop\\opc-productos-noncello.xls";
//        try
//        {
//            int hoja = 0;
//            //System.out.println("LEER:\n" + ExcelReader.leerXLS(ruta,hoja,1) );
//            System.out.println("---");
//            System.out.println("HEADERS:" + ExcelReader.dameArrayDeHeaders2(ruta, hoja , 1) );
//            
//            Class clase = Class.forName("auxiliar.ModeloProducto");
//            lista = ExcelReader.parearXLS2(ruta,hoja,1,clase, false);
//            System.out.println("PARSER:" +  lista.size());
//            
//            // -- 
//            
//            Class clase2 = Class.forName("auxiliar.ModeloOpc");
//            lista2 = ExcelReader.parearXLS2(ruta2,hoja,1,clase2, false);
//            System.out.println("PARSER:" +  lista2.size());
//            
//            int contador = 1;
//            for(ModeloProducto modeloProductoLoop : lista)
//            {
//                Producto producto = new Producto(modeloProductoLoop.getNombre(), true);
//                producto.setId(Integer.parseInt(modeloProductoLoop.getCodigoProducto()));
//                System.out.println("PRODUCTO: " + producto.toString());
//                    
//                for(ModeloOpc modeloOpcionLoop : lista2)
//                {
//                    // ONLY PROMOS
//                    
//                    if(modeloProductoLoop.getCodigoProducto().equalsIgnoreCase(modeloOpcionLoop.getFkProducto()))
//                    {
//                        String nombreOP = modeloOpcionLoop.getNombreOp();
//                        String subencabezadoOP = modeloOpcionLoop.getSubencabezado();
//                        String strPrecioOP = modeloOpcionLoop.getPrecio();
//                        double precioOP = Double.parseDouble(strPrecioOP);
//                        String descripcionOP = modeloOpcionLoop.getDescripcion();
//                        OpcionProducto opcion = new OpcionProducto(nombreOP, subencabezadoOP, precioOP, descripcionOP, producto, true);
//                        opcion.setId(Integer.parseInt(modeloOpcionLoop.getCodigo()));
//                        System.out.println("    op:"   + opcion.toString());
//
//                        
//                        for(int i = 0 ; i < Integer.parseInt(modeloOpcionLoop.getCantFotos()); i ++)
//                        {
//                            Foto foto = new Foto(modeloProductoLoop.getCodigoProducto()+"-" + i +".jpg");
//                            FotoOpcProducto fotoOpcProducto = new FotoOpcProducto(foto,opcion,true);
//                            opcion.addFotoOpcProducto(fotoOpcProducto);
//                            producto.addOpcionProducto(opcion);
//                            
//                        }
////                            dao.DAOEclipse.update(producto);
//                    }
//                    
//                }
//                System.out.println("------");
//                contador++;
//                
//            }
//        } 
//        catch (Exception ex)
//        {
//            ex.printStackTrace();
//        }
    }
}
