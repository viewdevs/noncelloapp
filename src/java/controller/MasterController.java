package controller;

import com.google.gson.Gson;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import modelo.Configuracion;
import modelo.Direccion;
import modelo.Foto;
import modelo.Persona;
import org.springframework.web.bind.annotation.RequestParam;
import ws.wsConfiguraciones;


public class MasterController
{
    public static long timestampUltimaActualizacionConfiguracion = -1; 
    private static Configuracion masterConfig;
    
    
    public static Configuracion dameConfigMaster()
    {
        Date ahora = new Date();
        long timestampActual = ahora.getTime();
        
        if(timestampUltimaActualizacionConfiguracion == -1 || timestampActual > (timestampUltimaActualizacionConfiguracion + ( 3600 * 1 * 1000)) )
        {
            masterConfig = wsConfiguraciones.dameConfigActual();
            timestampUltimaActualizacionConfiguracion = timestampActual;
        }
        
        return masterConfig;
    }
    
    
   
     public static String formatearFechaAAlgoBonito(Date fecha, boolean ponerHoras)
    {
        Date hoy = new Date();

        String fechaFormateada = "";

        int dia = fecha.getDate();
        int mes = fecha.getMonth() + 1 ;
        int year = fecha.getYear() + 1900;
        int hora = fecha.getHours();
        int minutos = fecha.getMinutes();

        String strDia;
        String strDiaSemana = "";
        String strMes;
        String strYear;
        String strH;
        String strM;


        //DIA:
        if(dia < 10)
        {
            strDia = "0" + dia;
        }
        else
        {
            strDia = "" + dia;
        }

        //MES:
        if(mes < 10)
        {
            strMes = "0" + mes;
        }
        else
        {
            strMes = "" + mes;
        }

        //YEAR:
        strYear = "" + year;

        //HORA:
        if(hora < 10)
        {
            strH = "0" + hora;
        }
        else
        {
            strH = "" + hora;
        }

        //MINUTOS:
        if(minutos < 10)
        {
            strM = "0" + minutos;
        }
        else
        {
            strM = "" + minutos;
        }
       
        strDiaSemana = resuelveDiaDeLaSemana(fecha.getDay());

        
        if(dia == hoy.getDate() && (mes == hoy.getMonth() + 1))
        {
            if(ponerHoras)
            {
                fechaFormateada = strH + ":" + strM + " hs";
            }
            else
            {
                fechaFormateada = "Hoy";
            }
        }
        else if((mes == hoy.getMonth() + 1))
        {
            if(ponerHoras)
            {
                fechaFormateada =  strDiaSemana + " " +strDia + " de " + resuelveStrMes(mes) + "a las (" + strH + ":" + strM + " hs)";
            }
            else
            {
                fechaFormateada =  strDiaSemana + " " +strDia + " de " + resuelveStrMes(mes);
            }
        }
        else
        {
            if(ponerHoras)
            {
                fechaFormateada =  strDiaSemana + " " +strDia + " de " + resuelveStrMes(mes) + "/" + strYear + " (" + strH + ":" + strM +" hs)";
            }
            else
            {
                fechaFormateada =  strDiaSemana + " " + strDia + " de" + resuelveStrMes(mes) + "/" + strYear;
            }
        }


        return  fechaFormateada;
    }
    public static String resuelveDiaDeLaSemana(int dia)
    {
        String strDiaSemana = "";
        switch (dia)
        {
            case 0: strDiaSemana = "Domingo"; break;
            case 1: strDiaSemana = "Lunes"; break;
            case 2: strDiaSemana = "Martes"; break;
            case 3: strDiaSemana = "Miercoles"; break;
            case 4: strDiaSemana = "Jueves"; break;
            case 5: strDiaSemana = "Viernes"; break;
            case 6: strDiaSemana = "Sabado"; break;

        }
        return strDiaSemana;
    }
    public static String resuelveStrMes(int mes)
    {
        String strMes = "";
        switch (mes)
        {
            case 1: strMes = "Enero"; break;
            case 2: strMes = "Febrero"; break;
            case 3: strMes = "Marzo"; break;
            case 4: strMes = "Abril"; break;
            case 5: strMes = "Mayo"; break;
            case 6: strMes = "Junio"; break;
            case 7: strMes = "Julio"; break;
            case 8: strMes = "Agosto"; break;
            case 9: strMes = "Septiembre"; break;
            case 10: strMes = "Octubre"; break;
            case 11: strMes = "Noviembre"; break;
            case 12: strMes = "Diciembre"; break;

        }
        return strMes;
    }
    
    public static Date dameFechaDeXDiasMenos(int cantidadDiasMenos)
    {
        // 1 - TRAIGO LA FECHA DE HOY:
        Date hoy = new Date();
        
        // 2 - CONVIERTO LA FECHA DE HOY AL FORMATO DE CALENDAR:
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String strHoy = dateFormat.format(hoy);

        // 3 - CALCULO CON CALENDAR 7 DIAS MENOS:
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -cantidadDiasMenos);
        
        // 4 - VUELVO A CREAR UN DATE CON EL DATE DE CALENDAR:
        Date sieteDiasMenos = cal.getTime();    
        String strSieteDiasMenos = dateFormat.format(sieteDiasMenos);
        
//        DateTime lastWeek = new DateTime().minusDays(7);
        
//            System.out.println("7 dias menos: " + sieteDiasMenos);
            
        return sieteDiasMenos;
    }
    
    
    
    public static Persona getReferidoDefault()
    {
        Persona referidoDefault = new Persona();
        referidoDefault = ws.wsPersona.getPersona(1);
        
        return referidoDefault;
    }
    public static Persona getClienteDefault()
    {
        Persona clienteDefault = new Persona();
        clienteDefault = ws.wsPersona.getPersona(10);
        
        return clienteDefault;
    }
    public static Direccion getDireccionEntregaDefault()
    {
        Direccion direccionDefault = ws.wsDireccion.getDireccion(9);
        
        return direccionDefault;
    }
    
    
    
    
    //LETRAS:
    public static String primeraMayus(String entrada)
    {
        String salida = "";
        String strPrimerChar = "";
        String restoDeLaCadena = "";
        
        if(entrada.length() > 0)
        {
            char primerChar = entrada.charAt(0);
            strPrimerChar = "" + primerChar;
            strPrimerChar = strPrimerChar.toUpperCase();
            restoDeLaCadena = entrada.substring(1,entrada.length());
        }
        
        salida = strPrimerChar + restoDeLaCadena;
        
        return salida;
    }
    
    
    
    
    
    // COSAS - LOGIN:
    /*private final static String varSessionHTTP = "usuario";
    public static final boolean modoRelease = false;*/
    
//    public static Operador getUsuarioEmpty()
//    {
//        Operador operadorEmpty = new Operador( "", "","", "","", "", false);
//        
//        operadorEmpty.setId( -1 );
//        
//        return operadorEmpty;
//    }
//    public static boolean sesionarUsuario(HttpServletRequest request, Operador usuario)
//    {
//        boolean sesionado = false;
//        
//        String strUsuarioJson = usuario.toJson();
//        if(strUsuarioJson != null)
//        {
//            if(dameConfigMaster() != null)
//            {
//                request.getSession().setAttribute(masterConfig.getVarSessionHTTP(), strUsuarioJson);
//                sesionado = true;
//            }
//        }
//        
//        return sesionado;
//    }
//    public static Operador dameUsuarioLogeadoFromDB(HttpServletRequest request)
//    {
//        // 1 - INICIALIZO UN USUARIO VACIO: 
//        Operador usuarioLogeado = getUsuarioEmpty();
//        
//        // 2 - BUSCO EN LA VARIABLE DE SESION EL ATRIBUTO RESTAURANTE Y LO CONVIERTO A JSON:
//        if(dameConfigMaster() != null)
//        {
//            String strRestaurant = (String) request.getSession().getAttribute(masterConfig.getVarSessionHTTP());
//            if(strRestaurant != null)
//            {
//                usuarioLogeado = new Gson().fromJson(strRestaurant, Operador.class);
//            }
//
//            // 3 - SI EL RESTAURANT ES DISTINTO DE -1 , LO BUSCO BIEN EN DB:
//            if(usuarioLogeado != null)
//            {
//                if(usuarioLogeado.getId() != -1)
//                {
//                    Operador usuarioDB = (Operador) dao.DAOEclipse.get(Operador.class, usuarioLogeado.getId());
//                    usuarioLogeado = usuarioDB;
//                }
//            }
//        }
//        
//        return usuarioLogeado;
//    }
//    public static boolean exit(HttpServletRequest request)
//    {
//        boolean deslogeado = false;
//        
//        if(dameConfigMaster() != null)
//        {
//            request.getSession().setAttribute(masterConfig.getVarSessionHTTP(), MasterController.getUsuarioEmpty().toJson());
//
//            deslogeado = true;
//        }
//        
//        return deslogeado;
//    }
}
