package util;

public class UtilidadesConStrings 
{
    public static String dameStringEntre(String cadena, String inicio, String fin)
    {
        boolean paseInicio = false;
        boolean llegueAlFin = false;
        String respuesta = "";
        String strAcumulador = "";
        
        for(int i = 0 ; i < cadena.length() ; i++)
        {
            char caracterActual = cadena.charAt(i);
            
            strAcumulador += "" + caracterActual;
            
            if(!paseInicio && strAcumulador.endsWith(inicio))
            {
                paseInicio = true;
            }
            if(paseInicio && strAcumulador.endsWith(fin))
            {
                llegueAlFin = true;
            }
            
            if(paseInicio && !llegueAlFin)
            {
                respuesta += caracterActual;
            }
        }
        return respuesta;
    }
    
}