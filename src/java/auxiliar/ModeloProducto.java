package auxiliar;

import java.util.ArrayList;
import java.util.List;

public class ModeloProducto
{
    public String codigoProducto;
    public String nombre;
    public String esPromo;
    public String visible;
    public String categorias;

    public ModeloProducto()
    {
    }

    public String getCodigoProducto()
    {
        return codigoProducto;
    }

    public void setCodigoProducto(String codigoProducto)
    {
        this.codigoProducto = codigoProducto;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getEsPromo()
    {
        return esPromo;
    }

    public void setEsPromo(String esPromo)
    {
        this.esPromo = esPromo;
    }

    public String getVisible()
    {
        return visible;
    }

    public void setVisible(String visible)
    {
        this.visible = visible;
    }

    public String getCategorias()
    {
        return categorias;
    }

    public void setCategorias(String categorias)
    {
        this.categorias = categorias;
    }
    
    
    @Override
    public String toString()
    {
        return "ModeloProducto{" + "codigoProducto=" + codigoProducto + ", nombre=" + nombre + ", esPromo=" + esPromo + ", visible=" + visible + '}';
    }
    public List<String> dameCategoriasDinamicos()
    {
        List<String> arrCategorias = new ArrayList<String>();
        
        String strAcumulador = "";
        if(categorias != null)
        {
            for(int i = 0 ; i < categorias.length() ;i ++)
            {
                char actual = categorias.charAt(i);

                int largo = categorias.length();
                if(actual == ';' || (i == largo-1) )
                {
                    if((i == largo-1))
                    {
                        strAcumulador += actual;
                    }
                    arrCategorias.add(strAcumulador);
                    strAcumulador = "";
                }
                else
                {
                    strAcumulador += actual;
                }

            }
        }
        
        return arrCategorias;
    }
    
    
}
