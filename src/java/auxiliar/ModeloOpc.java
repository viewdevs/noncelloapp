package auxiliar;

import com.google.gson.Gson;
import controller.MasterController;
import java.util.ArrayList;
import java.util.List;

public class ModeloOpc
{
    public String codigo;
    public String fkProducto;
    public String nombreOp;
    public String subencabezado;
    public String precio;
    public String descripcion;
    public String visible;
    public String cantFotos;
    
    

    public ModeloOpc()
    {
        
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }


    public String getFkProducto()
    {
        return fkProducto;
    }

    public void setFkProducto(String fkProducto)
    {
        this.fkProducto = fkProducto;
    }

    public String getNombreOp()
    {
        return nombreOp;
    }

    public void setNombreOp(String nombreOp)
    {
        this.nombreOp = nombreOp;
    }

    public String getSubencabezado()
    {
        return subencabezado;
    }

    public void setSubencabezado(String subencabezado)
    {
        this.subencabezado = subencabezado;
    }

    public String getPrecio()
    {
        return precio;
    }

    public void setPrecio(String precio)
    {
        this.precio = precio;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getVisible()
    {
        return visible;
    }

    public void setVisible(String visible)
    {
        this.visible = visible;
    }

    public String getCantFotos()
    {
        return cantFotos;
    }

    public void setCantFotos(String cantFotos)
    {
        this.cantFotos = cantFotos;
    }

    @Override
    public String toString()
    {
        return "ModeloOpc{" + "codigo=" + codigo + ", fkProducto=" + fkProducto + ", nombreOp=" + nombreOp + ", subencabezado=" + subencabezado + ", precio=" + precio + ", descripcion=" + descripcion + ", visible=" + visible + ", cantFotos=" + cantFotos + '}';
    }
    
    
    
    

    
    
    
}
