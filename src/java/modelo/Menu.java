package modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import controller.MasterController;
import java.util.*;
import javax.persistence.*;

@Entity @Table(name = "menus")
public class Menu implements Comparable<Menu>
{
    //ATRIBUTOS:
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nombre;
    private String url;
    private String icono;
    private String img;
    
    
    //CONTRUCTOR VACIO:
    public Menu() 
    {
    }
    
    //CONTRUCTOR PARAMETROS SIN LISTAS:

    public Menu(String nombre, String url, String icono)
    {
        this.nombre = nombre;
        this.url = url;
        this.icono = icono;
    }
    


    //<editor-fold desc="GETTERS Y SETTERS:">
    public int getId() 
    {
        return id;
    }
    public String getNombre() 
    {
        return nombre;
    }
    public String getUrl() 
    {
        return url;
    }

    public String getIcono()
    {
        return icono;
    }
    

    //SET
    public void setId( int id ) 
    {
        this.id = id;
    }
    public void setNombre( String nombre ) 
    {
        this.nombre = nombre;
    }
    public void setUrl( String url ) 
    {
        this.url = url;
    }

    public void setIcono(String icono)
    {
        this.icono = icono;
    }
    
    //</editor-fold>
    
    //@Override
    public String toString()
    {
        String str = "{";
        str += "id:" + id + ", ";
        str += "nombre:" + nombre + ", ";
        str += "url:" + url + ", ";
        str += "icono:" + icono + ", ";
        
        str += "}";
        
        return str;
    }

 
        
    
    //DYN:

    
    public int compareTo(Menu otro)
    {
        return 1;
    }
    public boolean getSelected()
    {
        return false;
    }
    public String getFullIcono()
    {
        String fullIcono = "";
        
        if(icono != null)
        {
           fullIcono =  MasterController.dameConfigMaster().getUrlVisualizacionBackground() + "/" + icono;
        }
        
        return fullIcono;
    }
    public String getFullImg()
    {
        String fullIcono = "";
        
        if(icono != null)
        {
           fullIcono =  MasterController.dameConfigMaster().getUrlVisualizacionBackground() + "/" + img;
        }
        
        return fullIcono;
    }
}
