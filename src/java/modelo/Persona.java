package modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.*;
import javax.persistence.*;

@Entity @Table(name = "personas")
public class Persona implements Comparable<Persona>
{
    //ATRIBUTOS:
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nombre;
    private String apellido;
    @Column(name = "email", unique=true)
    private String email;
    @JsonIgnore
    private String pass;
    private String telefono;
    @OneToOne() @JoinColumn(name = "fkDireccion")
    private Direccion direccion;
    @OneToOne() @JoinColumn(name = "fkFoto")
    private Foto foto;
    private boolean esCliente;
    private boolean esReferido;
    private boolean esOperador;
    private String observaciones;
    private String codigoReferencial;
    
    
    
    //CONTRUCTOR VACIO:
    public Persona() 
    {
    }
    
    //CONTRUCTOR PARAMETROS SIN LISTAS:

    public Persona(String nombre, String apellido, String email, String pass, String telefono, Direccion direccion, Foto foto, boolean esCliente, boolean esReferido, boolean esOperador, String observaciones, String codigoReferencial)
    {
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.pass = pass;
        this.telefono = telefono;
        this.direccion = direccion;
        this.foto = foto;
        this.esCliente = esCliente;
        this.esReferido = esReferido;
        this.esOperador = esOperador;
        this.observaciones = observaciones;
        this.codigoReferencial = codigoReferencial;
    }
    


    //<editor-fold desc="GETTERS Y SETTERS:">
    public int getId() 
    {
        return id;
    }
    public String getNombre() 
    {
        return nombre;
    }
    public String getApellido() 
    {
        return apellido;
    }
    public String getEmail() 
    {
        return email;
    }
    public String getPass() 
    {
        return pass;
    }
    public String getTelefono() 
    {
        return telefono;
    }
    public Direccion getDireccion() 
    {
        return direccion;
    }
    public Foto getFoto() 
    {
        return foto;
    }
    public boolean getEsCliente() 
    {
        return esCliente;
    }
    public boolean getEsReferido() 
    {
        return esReferido;
    }
    public boolean getEsOperador() 
    {
        return esOperador;
    }
    public String getObservaciones() 
    {
        return observaciones;
    }

    public String getCodigoReferencial()
    {
        return codigoReferencial;
    }

    public void setCodigoReferencial(String codigoReferencial)
    {
        this.codigoReferencial = codigoReferencial;
    }
    

    //SET
    public void setId( int id ) 
    {
        this.id = id;
    }
    public void setNombre( String nombre ) 
    {
        this.nombre = nombre;
    }
    public void setApellido( String apellido ) 
    {
        this.apellido = apellido;
    }
    public void setEmail( String email ) 
    {
        this.email = email;
    }
    public void setPass( String pass ) 
    {
        this.pass = pass;
    }
    public void setTelefono( String telefono ) 
    {
        this.telefono = telefono;
    }
    public void setDireccion( Direccion direccion ) 
    {
        this.direccion = direccion;
    }
    public void setFoto( Foto foto ) 
    {
        this.foto = foto;
    }
    public void setEsCliente( boolean esCliente ) 
    {
        this.esCliente = esCliente;
    }
    public void setEsReferido( boolean esReferido ) 
    {
        this.esReferido = esReferido;
    }
    public void setEsOperador( boolean esOperador ) 
    {
        this.esOperador = esOperador;
    }
    public void setObservaciones( String observaciones ) 
    {
        this.observaciones = observaciones;
    }
    
    //</editor-fold>
    
    //@Override
    public String toString()
    {
        String str = "{";
        str += "id:" + id + ", ";
        str += "nombre:" + nombre + ", ";
        str += "apellido:" + apellido + ", ";
        str += "email:" + email + ", ";
        str += "pass:" + pass + ", ";
        str += "telefono:" + telefono + ", ";
        str += "direccion:" + direccion + ", ";
        str += "foto:" + foto + ", ";
        str += "esCliente:" + esCliente + ", ";
        str += "esReferido:" + esReferido + ", ";
        str += "esOperador:" + esOperador + ", ";
        str += "observaciones:" + observaciones + ", ";
        str += "codigoReferencial:" + codigoReferencial + ", ";
        
        str += "}";
        
        return str;
    }

 
        
    
    //DYN:

    
    public int compareTo(Persona otro)
    {
        return 1;
    }
    public String toJson()
    {
        String str = "{";
        str += "id:" + id + ", ";
        str += "nombre:" + nombre + ", ";
        str += "apellido:" + apellido + ", ";
        str += "email:" + email + ", ";
        str += "pass:" + pass + ", ";
        str += "telefono:" + telefono + ", ";
        str += "direccion:" + direccion + ", ";
        str += "foto:" + foto + ", ";
        str += "esCliente:" + esCliente + ", ";
        str += "esReferido:" + esReferido + ", ";
        str += "esOperador:" + esOperador + ", ";
        str += "observaciones:" + observaciones + "";
        str += "codigoReferencial:" + codigoReferencial + "";
        
        str += "}";
        
        return str;
    }
    
}
