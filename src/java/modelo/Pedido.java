package modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import controller.MasterController;
import java.util.*;
import javax.persistence.*;

@Entity @Table(name = "pedidos")
public class Pedido implements Comparable<Pedido>
{
    //ATRIBUTOS:
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @OneToOne() @JoinColumn(name = "fkReferido")
    private Persona referido;
    @OneToOne(cascade = CascadeType.ALL) @JoinColumn(name = "fkCliente" ) 
    private Persona cliente;
    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "pedido")
    private List<DetallePedido> detallesList;
    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "pedido")
    private List<CambioEstadoPedido> cambiosEstadoList;
    @OneToOne() @JoinColumn(name = "fkDireccionEntrega")
    private Direccion direccionEntrega;
    private double precioCongelado;
    @Transient
    private String codigoReferido;
    private boolean retiroSucursal;
    
    //CONTRUCTOR VACIO:
    public Pedido() 
    {
        fecha = new Date();
        referido = MasterController.getReferidoDefault();
        cliente = MasterController.getClienteDefault();
        direccionEntrega = MasterController.getDireccionEntregaDefault();
        precioCongelado = 0;
        detallesList = new ArrayList<>();
        cambiosEstadoList = new ArrayList<>();
        retiroSucursal = true;
    }
    
    //CONTRUCTOR PARAMETROS SIN LISTAS:
    public Pedido(Date fecha,Persona referido,Persona cliente,Direccion direccionEntrega,double precioCongelado)
    {
        this.fecha = fecha;
        this.referido = referido;
        this.cliente = cliente;
        this.detallesList = new ArrayList<>();
        this.cambiosEstadoList = new ArrayList<>();
        this.direccionEntrega = direccionEntrega;
        this.precioCongelado = precioCongelado;
        retiroSucursal = true;
    }
    
    //CONTRUCTOR PARAMETROS CON LISTAS:
    public Pedido(Date fecha,Persona referido,Persona cliente,List<DetallePedido> detallesList,List<CambioEstadoPedido> cambiosEstadoList,Direccion direccionEntrega,double precioCongelado)
    {
        this.fecha = fecha;
        this.referido = referido;
        this.cliente = cliente;
        this.detallesList = new ArrayList<>();
        this.cambiosEstadoList = new ArrayList<>();
        this.direccionEntrega = direccionEntrega;
        this.precioCongelado = precioCongelado;
        retiroSucursal = true;
    }


    //<editor-fold desc="GETTERS Y SETTERS:">
    public int getId() 
    {
        return id;
    }
    public Date getFecha() 
    {
        return fecha;
    }
    public Persona getReferido() 
    {
        return referido;
    }
    public Persona getCliente() 
    {
        return cliente;
    }
    public List<DetallePedido> getDetallesList() 
    {
        return detallesList;
    }
    public List<CambioEstadoPedido> getCambiosEstadoList() 
    {
        return cambiosEstadoList;
    }
    public Direccion getDireccionEntrega() 
    {
        return direccionEntrega;
    }
    public double getPrecioCongelado() 
    {
        return precioCongelado;
    }

    public boolean isRetiroSucursal()
    {
        return retiroSucursal;
    }
    

    //SET
    public void setId( int id ) 
    {
        this.id = id;
    }

    public void setRetiroSucursal(boolean retiroSucursal)
    {
        this.retiroSucursal = retiroSucursal;
    }
    
    
    public void setFecha( Date fecha ) 
    {
        this.fecha = fecha;
    }
    public void setReferido( Persona referido ) 
    {
        this.referido = referido;
    }
    public void setCliente( Persona cliente ) 
    {
        this.cliente = cliente;
    }
    public void setDetallesList( List<DetallePedido> detallesList ) 
    {
        this.detallesList = detallesList;
        for(DetallePedido detallePedidoLoop : detallesList)
        {
            detallePedidoLoop.setPedido(this);
        }
    }
    public boolean addDetallePedido(DetallePedido detallePedido)
    {
        boolean agregue = false;

        List<DetallePedido> listaAux = new ArrayList<>();
        
        // COMPRUEBO YA TENERLO:
//        boolean yaLoTengo = false;
//        for(DetallePedido detalleLoop : detallesList)
//        {
//            if(detalleLoop.getOpcionProducto().getId() == detallePedido.getOpcionProducto().getId())
//            {
//                yaLoTengo = true;
//                int nuevaCantidad = detalleLoop.getOpcionProducto().getCantidadLlevo() + 1;
//                DetallePedido detalleModificado = new DetallePedido(detalleLoop.getPedido(), detalleLoop.getOpcionProducto(), nuevaCantidad);
////                detalleLoop.getOpcionProducto().setCantidadLlevo(cuanto);
//                listaAux.add(detalleModificado);
//            }
//            else
//            {
//                detalleLoop.setPedido(this);
//                listaAux.add(detalleLoop);
//            }
//        }
//        
//        if(!yaLoTengo)
//        {
            detallePedido.setPedido(this);
            detallesList.add(detallePedido);
//        }
//        
//        this.detallesList = listaAux;

        return agregue;
    }
    public boolean rmDetallePedido(DetallePedido detallePedido)
    {
        boolean agregue = false;

        List<DetallePedido> detallesAux = new ArrayList<DetallePedido>();
        for(DetallePedido detalleLoop : this.detallesList)
        {
            if(detallePedido.getId() != detalleLoop.getId())
            {
                detallesAux.add(detalleLoop);
            }
        }
        this.detallesList = detallesAux;

        return agregue;
    }
    public void setCambiosEstadoList( List<CambioEstadoPedido> cambiosEstadoList ) 
    {
        this.cambiosEstadoList = cambiosEstadoList;
        for(CambioEstadoPedido cambioEstadoPedidoLoop : cambiosEstadoList)
        {
            cambioEstadoPedidoLoop.setPedido(this);
        }
    }
   public boolean addCambioEstadoPedido(CambioEstadoPedido cambioEstadoPedido)
   {
       boolean agregue = false;
       
       cambioEstadoPedido.setPedido(this);
       this.cambiosEstadoList.add(cambioEstadoPedido);
    
       return agregue;
   }
    public void setDireccionEntrega( Direccion direccionEntrega ) 
    {
        this.direccionEntrega = direccionEntrega;
    }
    public void setPrecioCongelado( double precioCongelado ) 
    {
        this.precioCongelado = precioCongelado;
    }
    //</editor-fold>
    
    //@Override
    public String toString()
    {
        String str = "\n{";
        str += "    id:" + id + ", \n";
        str += "    fecha:" + fecha + ", \n";
        str += "    referido:" + referido + ", \n";
        str += "    cliente:" + cliente + ", \n";
        
        if( detallesList != null) 
        {
            str += "    detallesList:" + detallesList.size() + ", \n";
        }
        
        if( cambiosEstadoList != null) 
        {
            str += "    cambiosEstadoList:" + cambiosEstadoList.size() + ", \n";
        }
        str += "    direccionEntrega:" + direccionEntrega + ", \n";
        str += "    precioCongelado:" + precioCongelado + ", \n";
        str += "    retiroSucursal:" + retiroSucursal + ", \n";
        
        str += "}";
        
        return str;
    }

 
        
    
    //DYN:
    public String getFechaBonita()
    {
        String fechaBonita = "";
        
        if(getFecha() != null)
        {
            fechaBonita = MasterController.formatearFechaAAlgoBonito(fecha, true);
        }
        
        return fechaBonita;
    }
    
    public int compareTo(Pedido otro)
    {
        return 1;
    }
    
    public CambioEstadoPedido getUltimoCambioEstado()
    {
        CambioEstadoPedido ultimoCambioEstado = null;
        long timeUltimoCambioEstado = 0 ;
        if(cambiosEstadoList != null)
        {
            for(CambioEstadoPedido cambioEstadoLoop :  cambiosEstadoList)
            {
                if(cambioEstadoLoop.getCuando().getTime() > timeUltimoCambioEstado)
                {
                    timeUltimoCambioEstado = cambioEstadoLoop.getCuando().getTime();
                    ultimoCambioEstado = cambioEstadoLoop;
                }
            }
        }
         
        return ultimoCambioEstado;
    }
    
    public void agregarOpcionProductoListadoDetallesPedido(OpcionProducto opcioProductoRecibida , double nuevaCantidad , boolean seLaAumento)
    {
        if(detallesList != null)
        {
            // 1 - RECORRO TODO EL LISTADO DE DETALLE PEDIDO A VER SI ENCUENTRO LA MISMA OPCIONPRODUCTO:
            boolean loTenia = false;
            for(DetallePedido detalleLoop : detallesList)
            {
                OpcionProducto opcionProductoTengo = detalleLoop.getOpcionProducto();
                if(opcionProductoTengo.getId() == opcioProductoRecibida.getId())
                {
                    loTenia = true;
                    double cantidadTenia = detalleLoop.getCantidad();
                    double cantidadTotal = cantidadTenia + nuevaCantidad;
                    
                    // 2 - SI TENGO QUE RESETEAR LA CANTIDAD O SE LA AUMENTO
                    if(seLaAumento)
                    {
                        detalleLoop.setCantidad(cantidadTotal);
                    }
                    else
                    {
                        detalleLoop.setCantidad(nuevaCantidad);
                    }
                }
            }
            
            if(!loTenia)
            {
                DetallePedido detallePedido = new DetallePedido(this, opcioProductoRecibida, nuevaCantidad);
                this.addDetallePedido(detallePedido);
            }
            
        }
    }
    
    public double getCalcularTotal()
    {
        double total = 0;
        if(detallesList != null)
        {
            for(DetallePedido detalleLoop : detallesList)
            {
                double precio = detalleLoop.getOpcionProducto().getPrecio();
                double cantidad = detalleLoop.getCantidad();
                
                total += precio * cantidad;
            }
        }
        
        // DESCUENTO CON EL CODIGO DE REFERIDO:
        if(codigoReferido != null)
        {
            if(codigoReferido.length() == 4)
            {
                if(referido != null)
                {
                    if(referido.getId() != MasterController.getReferidoDefault().getId())
                    {
                        total = total * 0.85;
                    }
                }
            }
        }
        
        return total;
    }

    public String getCodigoReferido()
    {
        return codigoReferido;
    }

    public void setCodigoReferido(String codigoReferido)
    {
        this.codigoReferido = codigoReferido;
    }
    
}
