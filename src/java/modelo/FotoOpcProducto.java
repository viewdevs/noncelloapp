package modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.*;
import javax.persistence.*;

@Entity @Table(name = "fotosopcproductos")
public class FotoOpcProducto implements Comparable<FotoOpcProducto>
{
    //ATRIBUTOS:
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @OneToOne() @JoinColumn(name = "fkFoto")
    private Foto foto;
    @ManyToOne() @JoinColumn(name = "fkOpcionProducto") @JsonIgnore
    private OpcionProducto opcionProducto;
    private boolean visible;
    
    
    //CONTRUCTOR VACIO:
    public FotoOpcProducto() 
    {
    }
    
    //CONTRUCTOR PARAMETROS SIN LISTAS:
    public FotoOpcProducto(Foto foto,OpcionProducto opcionProducto,boolean visible)
    {
        this.foto = foto;
        this.opcionProducto = opcionProducto;
        this.visible = visible;
    }


    //<editor-fold desc="GETTERS Y SETTERS:">
    public int getId() 
    {
        return id;
    }
    public Foto getFoto() 
    {
        return foto;
    }
    public OpcionProducto getOpcionProducto() 
    {
        return opcionProducto;
    }
    public boolean getVisible() 
    {
        return visible;
    }

    //SET
    public void setId( int id ) 
    {
        this.id = id;
    }
    public void setFoto( Foto foto ) 
    {
        this.foto = foto;
    }
    public void setOpcionProducto( OpcionProducto opcionProducto ) 
    {
        this.opcionProducto = opcionProducto;
    }
    public void setVisible( boolean visible ) 
    {
        this.visible = visible;
    }
    //</editor-fold>
    
    //@Override
    public String toString()
    {
        String str = "{";
        str += "id:" + id + ", ";
        str += "foto:" + foto + ", ";
        str += "opcionProducto:" + opcionProducto + ", ";
        str += "visible:" + visible + ", ";
        
        str += "}";
        
        return str;
    }

 
        
    
    //DYN:

    
    public int compareTo(FotoOpcProducto otro)
    {
        return 1;
    }
    
}
