package modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.*;
import javax.persistence.*;

@Entity @Table(name = "cambiosestadospedidos")
public class CambioEstadoPedido implements Comparable<CambioEstadoPedido>
{
    //ATRIBUTOS:
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @OneToOne() @JoinColumn(name = "fkPedido") @JsonIgnore
    private Pedido pedido;
    @OneToOne() @JoinColumn(name = "fkEstadoPedidoDisponible")
    private EstadoPedidoDisponible estadoPedidoDisponible;
    @Temporal(TemporalType.TIMESTAMP)
    private Date cuando;
    @OneToOne() @JoinColumn(name = "fkOperador")
    private Persona operador;
    
    
    //CONTRUCTOR VACIO:
    public CambioEstadoPedido() 
    {
    }
    
    //CONTRUCTOR PARAMETROS SIN LISTAS:
    public CambioEstadoPedido(Pedido pedido,EstadoPedidoDisponible estadoPedidoDisponible,Date cuando,Persona operador)
    {
        this.pedido = pedido;
        this.estadoPedidoDisponible = estadoPedidoDisponible;
        this.cuando = cuando;
        this.operador = operador;
    }


    //<editor-fold desc="GETTERS Y SETTERS:">
    public int getId() 
    {
        return id;
    }
    public Pedido getPedido() 
    {
        return pedido;
    }
    public EstadoPedidoDisponible getEstadoPedidoDisponible() 
    {
        return estadoPedidoDisponible;
    }
    public Date getCuando() 
    {
        return cuando;
    }
    public Persona getOperador() 
    {
        return operador;
    }

    //SET
    public void setId( int id ) 
    {
        this.id = id;
    }
    public void setPedido( Pedido pedido ) 
    {
        this.pedido = pedido;
    }
    public void setEstadoPedidoDisponible( EstadoPedidoDisponible estadoPedidoDisponible ) 
    {
        this.estadoPedidoDisponible = estadoPedidoDisponible;
    }
    public void setCuando( Date cuando ) 
    {
        this.cuando = cuando;
    }
    public void setOperador( Persona operador ) 
    {
        this.operador = operador;
    }
    //</editor-fold>
    
    //@Override
    public String toString()
    {
        String str = "{";
        str += "id:" + id + ", ";
        str += "pedido:" + pedido + ", ";
        str += "estadoPedidoDisponible:" + estadoPedidoDisponible + ", ";
        str += "cuando:" + cuando + ", ";
        str += "operador:" + operador + ", ";
        
        str += "}";
        
        return str;
    }

 
        
    
    //DYN:

    
    public int compareTo(CambioEstadoPedido otro)
    {
        return 1;
    }
    
}
