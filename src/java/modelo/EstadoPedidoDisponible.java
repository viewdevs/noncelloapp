package modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.*;
import javax.persistence.*;

@Entity @Table(name = "estadospedidosdisponibles")
public class EstadoPedidoDisponible implements Comparable<EstadoPedidoDisponible>
{
    //ATRIBUTOS:
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nombr;
    
    
    //CONTRUCTOR VACIO:
    public EstadoPedidoDisponible() 
    {
    }
    
    //CONTRUCTOR PARAMETROS SIN LISTAS:
    public EstadoPedidoDisponible(String nombr)
    {
        this.nombr = nombr;
    }


    //<editor-fold desc="GETTERS Y SETTERS:">
    public int getId() 
    {
        return id;
    }
    public String getNombr() 
    {
        return nombr;
    }

    //SET
    public void setId( int id ) 
    {
        this.id = id;
    }
    public void setNombr( String nombr ) 
    {
        this.nombr = nombr;
    }
    //</editor-fold>
    
    //@Override
    public String toString()
    {
        String str = "{";
        str += "id:" + id + ", ";
        str += "nombr:" + nombr + ", ";
        
        str += "}";
        
        return str;
    }

 
        
    
    //DYN:

    
    public int compareTo(EstadoPedidoDisponible otro)
    {
        return 1;
    }
    
}
