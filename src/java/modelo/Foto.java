package modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import controller.MasterController;
import java.util.*;
import javax.persistence.*;

@Entity @Table(name = "fotos")
public class Foto implements Comparable<Foto>
{
    //ATRIBUTOS:
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String urlProvisoria;
    
    
    //CONTRUCTOR VACIO:
    public Foto() 
    {
    }
    
    //CONTRUCTOR PARAMETROS SIN LISTAS:
    public Foto(String urlProvisoria)
    {
        this.urlProvisoria = urlProvisoria;
    }


    //<editor-fold desc="GETTERS Y SETTERS:">
    public int getId() 
    {
        return id;
    }
    public String getUrlProvisoria() 
    {
        return urlProvisoria;
    }

    //SET
    public void setId( int id ) 
    {
        this.id = id;
    }
    public void setUrlProvisoria( String urlProvisoria ) 
    {
        this.urlProvisoria = urlProvisoria;
    }
    //</editor-fold>
    
    //@Override
    public String toString()
    {
        String str = "{";
        str += "id:" + id + ", ";
        str += "urlProvisoria:" + urlProvisoria + ", ";
        
        str += "}";
        
        return str;
    }

 
        
    
    //DYN:

    
    public int compareTo(Foto otro)
    {
        return 1;
    }
    
    
    public String getUrlFull()
    {
        String urlFull = null;
        
        if(urlProvisoria != null)
        {
            urlFull = MasterController.dameConfigMaster().getUrlVisualizacionBackground() + "/" + urlProvisoria;
        }
        
        return urlFull;
    }
}
