package modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.*;
import javax.persistence.*;

@Entity @Table(name = "detallespedidos")
public class DetallePedido implements Comparable<DetallePedido>
{
    //ATRIBUTOS:
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne() @JoinColumn(name = "fkPedido") @JsonIgnore
    private Pedido pedido;
    @OneToOne() @JoinColumn(name = "fkOpcionProducto")
    private OpcionProducto opcionProducto;
    private double cantidad;
    
    
    //CONTRUCTOR VACIO:
    public DetallePedido() 
    {
    }
    
    //CONTRUCTOR PARAMETROS SIN LISTAS:
    public DetallePedido(Pedido pedido,OpcionProducto opcionProducto,double cantidad)
    {
        this.pedido = pedido;
        this.opcionProducto = opcionProducto;
        this.cantidad = cantidad;
    }


    //<editor-fold desc="GETTERS Y SETTERS:">
    public int getId() 
    {
        return id;
    }
    public Pedido getPedido() 
    {
        return pedido;
    }
    public OpcionProducto getOpcionProducto() 
    {
        return opcionProducto;
    }
    public double getCantidad() 
    {
        return cantidad;
    }

    //SET
    public void setId( int id ) 
    {
        this.id = id;
    }
    public void setPedido( Pedido pedido ) 
    {
        this.pedido = pedido;
    }
    public void setOpcionProducto( OpcionProducto opcionProducto ) 
    {
        this.opcionProducto = opcionProducto;
    }
    public void setCantidad( double cantidad ) 
    {
        this.cantidad = cantidad;
    }
    //</editor-fold>
    
    //@Override
    public String toString()
    {
        String str = "{";
        str += "id:" + id + ", ";
        str += "pedido:" + pedido + ", ";
        str += "opcionProducto:" + opcionProducto + ", ";
        str += "cantidad:" + cantidad + ", ";
        
        str += "}";
        
        return str;
    }

 
        
    
    //DYN:

    
    public int compareTo(DetallePedido otro)
    {
        return 1;
    }
    
}
