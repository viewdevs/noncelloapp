package modelo;

public class URLNico {
    
    private String urlProvisoria;
    private String urlFinal;

    public URLNico() {
    }

    public URLNico(String urlProvisoria, String urlFinal) {
        this.urlProvisoria = urlProvisoria;
        this.urlFinal = urlFinal;
    }

    public String getUrlProvisoria() {
        return urlProvisoria;
    }

    public void setUrlProvisoria(String urlProvisoria) {
        this.urlProvisoria = urlProvisoria;
    }

    public String getUrlFinal() {
        return urlFinal;
    }

    public void setUrlFinal(String urlFinal) {
        this.urlFinal = urlFinal;
    }

    @Override
    public String toString()
    {
        return "URLNico{" + "urlProvisoria=" + urlProvisoria + ", urlFinal=" + urlFinal + '}';
    }
    
    
    
    
}
