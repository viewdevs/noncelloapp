package modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.*;
import javax.persistence.*;

@Entity @Table(name = "direcciones")
public class Direccion implements Comparable<Direccion>
{
    //ATRIBUTOS:
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String hotel;
    private String calle;
    private String numero;
    private String piso;
    private String habitacion;
    private String observacion;
    
    
    //CONTRUCTOR VACIO:
    public Direccion() 
    {
    }
    
    //CONTRUCTOR PARAMETROS SIN LISTAS:
    public Direccion(String hotel,String calle,String numero,String piso,String habitacion,String observacion)
    {
        this.hotel = hotel;
        this.calle = calle;
        this.numero = numero;
        this.piso = piso;
        this.habitacion = habitacion;
        this.observacion = observacion;
    }


    //<editor-fold desc="GETTERS Y SETTERS:">
    public int getId() 
    {
        return id;
    }
    public String getHotel() 
    {
        return hotel;
    }
    public String getCalle() 
    {
        return calle;
    }
    public String getNumero() 
    {
        return numero;
    }
    public String getPiso() 
    {
        return piso;
    }
    public String getHabitacion() 
    {
        return habitacion;
    }
    public String getObservacion() 
    {
        return observacion;
    }

    //SET
    public void setId( int id ) 
    {
        this.id = id;
    }
    public void setHotel( String hotel ) 
    {
        this.hotel = hotel;
    }
    public void setCalle( String calle ) 
    {
        this.calle = calle;
    }
    public void setNumero( String numero ) 
    {
        this.numero = numero;
    }
    public void setPiso( String piso ) 
    {
        this.piso = piso;
    }
    public void setHabitacion( String habitacion ) 
    {
        this.habitacion = habitacion;
    }
    public void setObservacion( String observacion ) 
    {
        this.observacion = observacion;
    }
    //</editor-fold>
    
    //@Override
    public String toString()
    {
        String str = "{";
        str += "id:" + id + ", ";
        str += "hotel:" + hotel + ", ";
        str += "calle:" + calle + ", ";
        str += "numero:" + numero + ", ";
        str += "piso:" + piso + ", ";
        str += "habitacion:" + habitacion + ", ";
        str += "observacion:" + observacion + ", ";
        
        str += "}";
        
        return str;
    }

 
        
    
    //DYN:

    
    public int compareTo(Direccion otro)
    {
        return 1;
    }
    
}
