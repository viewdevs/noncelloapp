package modelo;

public class Carrito
{
    private String codigoSesion;
    private Pedido pedido;

    public Carrito()
    {
        pedido = new Pedido();
    }

    public Carrito(String codigoSesion)
    {
        pedido = new Pedido();
        this.codigoSesion = codigoSesion;
    }

    public Carrito(String codigoSesion, Pedido pedido)
    {
        this.codigoSesion = codigoSesion;
        this.pedido = pedido;
    }
    


    public String getCodigoSesion()
    {
        return codigoSesion;
    }

    public void setCodigoSesion(String codigoSesion)
    {
        this.codigoSesion = codigoSesion;
    }

    public Pedido getPedido()
    {
        return pedido;
    }

    public void setPedido(Pedido pedido)
    {
        this.pedido = pedido;
    }


    @Override
    public String toString()
    {
        return "Carrito{" + "codigoSesion=" + codigoSesion + ", pedido=" + pedido + '}';
    }
}
