package modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.*;
import javax.persistence.*;

@Entity @Table(name = "productos")
public class Producto implements Comparable<Producto>
{
    //ATRIBUTOS:
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nombre;
    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "productoPadre")
    private List<OpcionProducto> opcionesList;
    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "producto")
    private List<RelProductoHash> relshashtagsList;
    private boolean esPromo;
//    private boolean visible;
    
    
    //CONTRUCTOR VACIO:
    public Producto() 
    {
        opcionesList = new ArrayList<>();
        relshashtagsList = new ArrayList<>();
    }
    
    //CONTRUCTOR PARAMETROS SIN LISTAS:
    public Producto(String nombre,boolean esPromo)
    {
        this.nombre = nombre;
        this.opcionesList = new ArrayList<>();
        this.relshashtagsList = new ArrayList<>();
        this.esPromo = esPromo;
    }
    
    //CONTRUCTOR PARAMETROS CON LISTAS:
    public Producto(String nombre,List<OpcionProducto> opcionesList,List<RelProductoHash> relshashtagsList,boolean esPromo)
    {
        this.nombre = nombre;
        this.opcionesList = new ArrayList<>();
        this.relshashtagsList = new ArrayList<>();
        this.esPromo = esPromo;
    }


    //<editor-fold desc="GETTERS Y SETTERS:">
    public int getId() 
    {
        return id;
    }
    public String getNombre() 
    {
        return nombre;
    }
    public List<OpcionProducto> getOpcionesList() 
    {
        return opcionesList;
    }
    public List<RelProductoHash> getRelshashtagsList() 
    {
        return relshashtagsList;
    }
    public boolean getEsPromo() 
    {
        return esPromo;
    }

    //SET
    public void setId( int id ) 
    {
        this.id = id;
    }
    public void setNombre( String nombre ) 
    {
        this.nombre = nombre;
    }
    public void setOpcionesList( List<OpcionProducto> opcionesList ) 
    {
        this.opcionesList = opcionesList;
        for(OpcionProducto opcionProductoLoop : opcionesList)
        {
            opcionProductoLoop.setProductoPadre(this);
        }
    }
   public boolean addOpcionProducto(OpcionProducto opcionProducto)
   {
       boolean agregue = false;
       
       opcionProducto.setProductoPadre(this);
       this.opcionesList.add(opcionProducto);
    
       return agregue;
   }
    public void setRelshashtagsList( List<RelProductoHash> relshashtagsList ) 
    {
        this.relshashtagsList = relshashtagsList;
        for(RelProductoHash relProductoHashLoop : relshashtagsList)
        {
            relProductoHashLoop.setProducto(this);
        }
    }
   public boolean addRelProductoHash(RelProductoHash relProductoHash)
   {
       boolean agregue = false;
       
       relProductoHash.setProducto(this);
       this.relshashtagsList.add(relProductoHash);
    
       return agregue;
   }
    public void setEsPromo( boolean esPromo ) 
    {
        this.esPromo = esPromo;
    }
    //</editor-fold>
    
    //@Override
    public String toString()
    {
        String str = "{";
        str += "id:" + id + ", ";
        str += "nombre:" + nombre + ", ";
        
        if( opcionesList != null) 
        {
            str += "opcionesList:" + opcionesList.size() + ", ";
        }
        
        if( relshashtagsList != null) 
        {
            str += "relshashtagsList:" + relshashtagsList.size() + ", ";
        }
        str += "esPromo:" + esPromo + ", ";
        
        str += "}";
        
        return str;
    }

 
        
    
    //DYN:

    
    public int compareTo(Producto otro)
    {
        return 1;
    }
    
    public boolean getVisible()
    {
        boolean visible = false;
        
        if(opcionesList != null)
        {
            for(OpcionProducto opcionLoop : opcionesList)
            {
                if(opcionLoop.getVisible())
                {
                    visible = true;
                    break;
                }
            }
        }
        
        return visible;
    }
    public FotoOpcProducto getFotoPrincipal()
    {
        FotoOpcProducto fotoPrincipal = null;
        
        if(opcionesList != null)
        {
            for(OpcionProducto opcionLoop : opcionesList)
            {
                if(opcionLoop != null)
                {
                    if(opcionLoop.getFotosOpcProductosList() != null)
                    {
                        for(FotoOpcProducto fotoLoop : opcionLoop.getFotosOpcProductosList())
                        {
                            if(fotoLoop.getVisible())
                            {
                                fotoPrincipal = fotoLoop;
                                break;
                            }
                        }
                    }
                }
            }
        }
        
        return fotoPrincipal;
    }
    public double getMinPrecio()
    {
        double precioMinimo = 999999;
        
        if(opcionesList != null)
        {
            for(OpcionProducto opcionLoop : opcionesList)
            {
                if(opcionLoop != null)
                {
                    if(opcionLoop.getPrecio() < precioMinimo)
                    {
                        precioMinimo = opcionLoop.getPrecio();
                    }
                }
            }
        }
        
        return precioMinimo;
    }
    
}
