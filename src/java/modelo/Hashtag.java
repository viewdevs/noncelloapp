package modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import controller.MasterController;
import java.util.*;
import javax.persistence.*;

@Entity @Table(name = "hashs")
public class Hashtag implements Comparable<Hashtag>
{
    //ATRIBUTOS:
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String hash;
    private String subTexto;
    private String foto;
    
    
    //CONTRUCTOR VACIO:
    public Hashtag() 
    {
    }
    
    //CONTRUCTOR PARAMETROS SIN LISTAS:
    public Hashtag(String hash,String subTexto,String foto)
    {
        this.hash = hash;
        this.subTexto = subTexto;
        this.foto = foto;
    }


    //<editor-fold desc="GETTERS Y SETTERS:">
    public int getId() 
    {
        return id;
    }
    public String getHash() 
    {
        return hash;
    }
    public String getSubTexto() 
    {
        return subTexto;
    }
    public String getFoto() 
    {
        return foto;
    }

    //SET
    public void setId( int id ) 
    {
        this.id = id;
    }
    public void setHash( String hash ) 
    {
        this.hash = hash;
    }
    public void setSubTexto( String subTexto ) 
    {
        this.subTexto = subTexto;
    }
    public void setFoto( String foto ) 
    {
        this.foto = foto;
    }
    //</editor-fold>
    
    
    //@Override
    public String toString()
    {
        String str = "{";
        str += "id:" + id + ", ";
        str += "hash:" + hash + ", ";
        str += "subTexto:" + subTexto + ", ";
        str += "foto:" + foto + ", ";
        
        str += "}";
        
        return str;
    }

 
        
    
    //DYN:

    
    public int compareTo(Hashtag otro)
    {
        return 1;
    }
    public String getFullFoto()
    {
        return MasterController.dameConfigMaster().getUrlVisualizacionBackground() + "/"+ foto;
    }
    
}
