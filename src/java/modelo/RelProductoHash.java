package modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.*;
import javax.persistence.*;

@Entity @Table(name = "relproductohash")
public class RelProductoHash implements Comparable<RelProductoHash>
{
    //ATRIBUTOS:
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @OneToOne() @JoinColumn(name = "fkHashtag")
    private Hashtag hashtag;
    @OneToOne() @JoinColumn(name = "fkProducto") @JsonIgnore
    private Producto producto;
    private boolean activo;
    
    
    //CONTRUCTOR VACIO:
    public RelProductoHash() 
    {
    }
    
    //CONTRUCTOR PARAMETROS SIN LISTAS:
    public RelProductoHash(Hashtag hashtag,Producto producto,boolean activo)
    {
        this.hashtag = hashtag;
        this.producto = producto;
        this.activo = activo;
    }


    //<editor-fold desc="GETTERS Y SETTERS:">
    public int getId() 
    {
        return id;
    }
    public Hashtag getHashtag() 
    {
        return hashtag;
    }
    public Producto getProducto() 
    {
        return producto;
    }
    public boolean getActivo() 
    {
        return activo;
    }

    //SET
    public void setId( int id ) 
    {
        this.id = id;
    }
    public void setHashtag( Hashtag hashtag ) 
    {
        this.hashtag = hashtag;
    }
    public void setProducto( Producto producto ) 
    {
        this.producto = producto;
    }
    public void setActivo( boolean activo ) 
    {
        this.activo = activo;
    }
    //</editor-fold>
    
    //@Override
    public String toString()
    {
        String str = "{";
        str += "id:" + id + ", ";
        str += "hashtag:" + hashtag + ", ";
        str += "producto:" + producto + ", ";
        str += "activo:" + activo + ", ";
        
        str += "}";
        
        return str;
    }

 
        
    
    //DYN:

    
    public int compareTo(RelProductoHash otro)
    {
        return 1;
    }
    
}
