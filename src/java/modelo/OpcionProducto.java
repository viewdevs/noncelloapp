package modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.*;
import javax.persistence.*;

@Entity @Table(name = "opcionesproductos")
public class OpcionProducto implements Comparable<OpcionProducto>
{
    //ATRIBUTOS:
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nombre;
    private String subEncabezado;
    private double precio;
    private String descripcion;
    @ManyToOne() @JoinColumn(name = "fkProductoPadre") @JsonIgnore
    private Producto productoPadre;
    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "opcionProducto")
    private List<FotoOpcProducto> fotosOpcProductosList;
    private boolean visible;
    @Transient
    private int cantidadLlevo;
    
    
    //CONTRUCTOR VACIO:
    public OpcionProducto() 
    {
        fotosOpcProductosList = new ArrayList<>();
    }
    
    //CONTRUCTOR PARAMETROS SIN LISTAS:
    public OpcionProducto(String nombre,String subEncabezado,double precio,String descripcion,Producto productoPadre,boolean visible)
    {
        this.nombre = nombre;
        this.subEncabezado = subEncabezado;
        this.precio = precio;
        this.descripcion = descripcion;
        this.productoPadre = productoPadre;
        this.fotosOpcProductosList = new ArrayList<>();
        this.visible = visible;
    }
    
    //CONTRUCTOR PARAMETROS CON LISTAS:
    public OpcionProducto(String nombre,String subEncabezado,double precio,String descripcion,Producto productoPadre,List<FotoOpcProducto> fotosOpcProductosList,boolean visible)
    {
        this.nombre = nombre;
        this.subEncabezado = subEncabezado;
        this.precio = precio;
        this.descripcion = descripcion;
        this.productoPadre = productoPadre;
        this.fotosOpcProductosList = new ArrayList<>();
        this.visible = visible;
    }


    //<editor-fold desc="GETTERS Y SETTERS:">
    public int getId() 
    {
        return id;
    }
    public String getNombre() 
    {
        return nombre;
    }
    public String getSubEncabezado() 
    {
        return subEncabezado;
    }
    public double getPrecio() 
    {
        return precio;
    }
    public String getDescripcion() 
    {
        return descripcion;
    }
    public Producto getProductoPadre() 
    {
        return productoPadre;
    }
    public List<FotoOpcProducto> getFotosOpcProductosList() 
    {
        return fotosOpcProductosList;
    }
    public boolean getVisible() 
    {
        return visible;
    }

    //SET
    public void setId( int id ) 
    {
        this.id = id;
    }
    public void setNombre( String nombre ) 
    {
        this.nombre = nombre;
    }
    public void setSubEncabezado( String subEncabezado ) 
    {
        this.subEncabezado = subEncabezado;
    }
    public void setPrecio( double precio ) 
    {
        this.precio = precio;
    }
    public void setDescripcion( String descripcion ) 
    {
        this.descripcion = descripcion;
    }
    public void setProductoPadre( Producto productoPadre ) 
    {
        this.productoPadre = productoPadre;
    }
    public void setFotosOpcProductosList( List<FotoOpcProducto> fotosOpcProductosList ) 
    {
        this.fotosOpcProductosList = fotosOpcProductosList;
        for(FotoOpcProducto fotoOpcProductoLoop : fotosOpcProductosList)
        {
            fotoOpcProductoLoop.setOpcionProducto(this);
        }
    }
   public boolean addFotoOpcProducto(FotoOpcProducto fotoOpcProducto)
   {
       boolean agregue = false;
       
       fotoOpcProducto.setOpcionProducto(this);
       this.fotosOpcProductosList.add(fotoOpcProducto);
    
       return agregue;
   }
    public void setVisible( boolean visible ) 
    {
        this.visible = visible;
    }
    //</editor-fold>
    
    //@Override
    public String toString()
    {
        String str = "{";
        str += "id:" + id + ", ";
        str += "nombre:" + nombre + ", ";
        str += "subEncabezado:" + subEncabezado + ", ";
        str += "precio:" + precio + ", ";
        str += "descripcion:" + descripcion + ", ";
        
        if( fotosOpcProductosList != null) 
        {
            str += "fotosOpcProductosList:" + fotosOpcProductosList.size() + ", ";
        }
        str += "visible:" + visible + ", ";
        
        str += "}";
        
        return str;
    }

 
        
    
    //DYN:

    
    public int compareTo(OpcionProducto otro)
    {
        return 1;
    }

    public int getCantidadLlevo()
    {
        cantidadLlevo = 1;
        return cantidadLlevo;
    }

    public void setCantidadLlevo(int cantidadLlevo)
    {
        this.cantidadLlevo = cantidadLlevo;
    }
    public FotoOpcProducto getFotoPrincipal()
    {
        FotoOpcProducto fotoPrincipal = null;
        
        if(getFotosOpcProductosList() != null)
        {
            for(FotoOpcProducto fotoLoop: getFotosOpcProductosList())
            {
                fotoPrincipal = fotoLoop;
                break;
            }
        }
        
        return fotoPrincipal;
    }
    
}
