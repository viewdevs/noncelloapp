$scope.arrProductos = [];
$scope.findProductos = function()
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../findProductos",
        data:
        {

        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.arrProductos = resultado;
            $scope.cargando = false;
            $scope.$evalAsync();
        }
    });
}
$scope.findProductosByHash = function(fkHash)
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../findProductosByHash",
        data:
        {
            "fkHash": fkHash
        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.arrProductos = resultado;
            $scope.cargando = false;
            $scope.$evalAsync();
        }
    });
}


//AJAX SAVE
$scope.guardarProducto = function()
{
    guardo = false;
    $scope.cargando = false;
    
    $.ajax(
    {
        url:"../../guardarProducto",
        async: false,
        data:
        {
            "strProducto": JSON.stringify($scope.producto)
        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            console.log("res WS -> guardarProducto : " + resultado);
            $scope.cargando = false;
            guardo = resultado;
            
            if(guardo)
            {
                $scope.snack("Guardado con exito!");
                $scope.findProductos();
            }
            $scope.$evalAsync();
        }

    });
    
    return guardo;
}
//AJAX GET
$scope.producto = {};
$scope.getProducto = function(idProducto)
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getProducto",
       async: false,
       data:
       {
            "idProducto":idProducto
       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.producto = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}

//AJAX GET - EMPTY
$scope.productoEmpty = {};
$scope.getProductoEmpty = function()
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getProductoEmpty",
       async: false,
       data:
       {

       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.productoEmpty = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}
//VALIDAR
$scope.validarProducto = function()
{
   $scope.todoOK = false;
   $scope.comprobarNombre = $scope.producto.nombre != null && $scope.producto.nombre.length > 1;
   if($scope.comprobarNombre)
   {
       $scope.producto.nombre = $scope.pasaraMayusPrimerLetra($scope.producto.nombre);
       $scope.$evalAsync();
   }
   
   $scope.comprobarSubEncabezado = $scope.producto.subEncabezado != null && $scope.producto.subEncabezado.length > 1;
   if($scope.comprobarSubEncabezado)
   {
       $scope.producto.subEncabezado = $scope.pasaraMayusPrimerLetra($scope.producto.subEncabezado);
       $scope.$evalAsync();
   }
   
   $scope.comprobarPrecio = $scope.producto.precio != null && $scope.producto.precio.length > 1;
   if($scope.comprobarPrecio)
   {
       $scope.producto.precio = $scope.pasaraMayusPrimerLetra($scope.producto.precio);
       $scope.$evalAsync();
   }
   
   $scope.comprobarDescripcion = $scope.producto.descripcion != null && $scope.producto.descripcion.length > 1;
   if($scope.comprobarDescripcion)
   {
       $scope.producto.descripcion = $scope.pasaraMayusPrimerLetra($scope.producto.descripcion);
       $scope.$evalAsync();
   }
   
   $scope.comprobarEsPromo = $scope.producto.esPromo != null && $scope.producto.esPromo.length > 1;
   if($scope.comprobarEsPromo)
   {
       $scope.producto.esPromo = $scope.pasaraMayusPrimerLetra($scope.producto.esPromo);
       $scope.$evalAsync();
   }
   
   $scope.comprobarVisible = $scope.producto.visible != null && $scope.producto.visible.length > 1;
   if($scope.comprobarVisible)
   {
       $scope.producto.visible = $scope.pasaraMayusPrimerLetra($scope.producto.visible);
       $scope.$evalAsync();
   }
   
   //COMPROBACION FINAL
   if( $scope.comprobarNombre && $scope.comprobarSubEncabezado && $scope.comprobarPrecio && $scope.comprobarDescripcion && $scope.comprobarEsPromo && $scope.comprobarVisible )
   {
       console.log("TODO VALIDADO");
       $scope.todoOK = true;
       $scope.$evalAsync();
   }
		
}
