$scope.arrHashtags = [];
$scope.findHashtags = function()
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../findHashtags",
        data:
        {

        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.arrHashtags = resultado;
            $scope.cargando = false;
            $scope.$evalAsync();
        }
    });
}
$scope.findHashtagsActivos = function()
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../findHashtagsActivos",
        data:
        {

        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.arrHashtags = resultado;
            $scope.cargando = false;
            $scope.$evalAsync();
        }
    });
}


//AJAX SAVE
$scope.guardarHashtag = function()
{
    guardo = false;
    $scope.cargando = false;
    
    $.ajax(
    {
        url:"../../guardarHashtag",
        async: false,
        data:
        {
            "strHashtag": JSON.stringify($scope.hashtag)
        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            console.log("res WS -> guardarHashtag : " + resultado);
            $scope.cargando = false;
            guardo = resultado;
            
            if(guardo)
            {
                $scope.snack("Guardado con exito!");
                $scope.findHashtags();
            }
            $scope.$evalAsync();
        }

    });
    
    return guardo;
}
//AJAX GET
$scope.hashtag = {};
$scope.getHashtag = function(idHashtag)
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getHashtag",
       async: false,
       data:
       {
            "idHashtag":idHashtag
       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.hashtag = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
           
       }
   });
}

//AJAX GET - EMPTY
$scope.hashtagEmpty = {};
$scope.getHashtagEmpty = function()
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getHashtagEmpty",
       async: false,
       data:
       {

       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.hashtagEmpty = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}
//VALIDAR
$scope.validarHashtag = function()
{
   $scope.todoOK = false;
   $scope.comprobarHash = $scope.hashtag.hash != null && $scope.hashtag.hash.length > 1;
   if($scope.comprobarHash)
   {
       $scope.hashtag.hash = $scope.pasaraMayusPrimerLetra($scope.hashtag.hash);
       $scope.$evalAsync();
   }
   
   $scope.comprobarSubTexto = $scope.hashtag.subTexto != null && $scope.hashtag.subTexto.length > 1;
   if($scope.comprobarSubTexto)
   {
       $scope.hashtag.subTexto = $scope.pasaraMayusPrimerLetra($scope.hashtag.subTexto);
       $scope.$evalAsync();
   }
   
   $scope.comprobarFoto = $scope.hashtag.foto != null && $scope.hashtag.foto.length > 1;
   if($scope.comprobarFoto)
   {
       $scope.hashtag.foto = $scope.pasaraMayusPrimerLetra($scope.hashtag.foto);
       $scope.$evalAsync();
   }
   
   //COMPROBACION FINAL
   if( $scope.comprobarHash && $scope.comprobarSubTexto && $scope.comprobarFoto )
   {
       console.log("TODO VALIDADO");
       $scope.todoOK = true;
       $scope.$evalAsync();
   }
		
}
