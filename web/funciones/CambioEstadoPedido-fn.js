$scope.arrCambioEstadoPedidos = [];
$scope.findCambioEstadoPedidos = function()
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../findCambioEstadoPedidos",
        data:
        {

        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.arrCambioEstadoPedidos = resultado;
            $scope.cargando = false;
            $scope.$evalAsync();
        }
    });
}


//AJAX SAVE
$scope.guardarCambioEstadoPedido = function()
{
    guardo = false;
    $scope.cargando = false;
    
    $.ajax(
    {
        url:"../../guardarCambioEstadoPedido",
        async: false,
        data:
        {
            "strCambioEstadoPedido": JSON.stringify($scope.cambioEstadoPedido)
        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            console.log("res WS -> guardarCambioEstadoPedido : " + resultado);
            $scope.cargando = false;
            guardo = resultado;
            
            if(guardo)
            {
                $scope.snack("Guardado con exito!");
                $scope.findCambioEstadoPedidos();
            }
            $scope.$evalAsync();
        }

    });
    
    return guardo;
}
//AJAX GET
$scope.cambioEstadoPedido = {};
$scope.getCambioEstadoPedido = function(idCambioEstadoPedido)
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getCambioEstadoPedido",
       async: false,
       data:
       {
            "idCambioEstadoPedido":idCambioEstadoPedido
       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.cambioEstadoPedido = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}

//AJAX GET - EMPTY
$scope.cambioEstadoPedidoEmpty = {};
$scope.getCambioEstadoPedidoEmpty = function()
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getCambioEstadoPedidoEmpty",
       async: false,
       data:
       {

       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.cambioEstadoPedidoEmpty = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}
//VALIDAR
$scope.validarCambioEstadoPedido = function()
{
   $scope.todoOK = false;
   $scope.comprobarPedido = $scope.cambioEstadoPedido.pedido != null && $scope.cambioEstadoPedido.pedido.length > 1;
   if($scope.comprobarPedido)
   {
       $scope.cambioEstadoPedido.pedido = $scope.pasaraMayusPrimerLetra($scope.cambioEstadoPedido.pedido);
       $scope.$evalAsync();
   }
   
   $scope.comprobarEstadoPedidoDisponible = $scope.cambioEstadoPedido.estadoPedidoDisponible != null && $scope.cambioEstadoPedido.estadoPedidoDisponible.length > 1;
   if($scope.comprobarEstadoPedidoDisponible)
   {
       $scope.cambioEstadoPedido.estadoPedidoDisponible = $scope.pasaraMayusPrimerLetra($scope.cambioEstadoPedido.estadoPedidoDisponible);
       $scope.$evalAsync();
   }
   
   $scope.comprobarCuando = $scope.cambioEstadoPedido.cuando != null && $scope.cambioEstadoPedido.cuando.length > 1;
   if($scope.comprobarCuando)
   {
       $scope.cambioEstadoPedido.cuando = $scope.pasaraMayusPrimerLetra($scope.cambioEstadoPedido.cuando);
       $scope.$evalAsync();
   }
   
   $scope.comprobarOperador = $scope.cambioEstadoPedido.operador != null && $scope.cambioEstadoPedido.operador.length > 1;
   if($scope.comprobarOperador)
   {
       $scope.cambioEstadoPedido.operador = $scope.pasaraMayusPrimerLetra($scope.cambioEstadoPedido.operador);
       $scope.$evalAsync();
   }
   
   //COMPROBACION FINAL
   if( $scope.comprobarPedido && $scope.comprobarEstadoPedidoDisponible && $scope.comprobarCuando && $scope.comprobarOperador )
   {
       console.log("TODO VALIDADO");
       $scope.todoOK = true;
       $scope.$evalAsync();
   }
		
}
