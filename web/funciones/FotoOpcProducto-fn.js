$scope.arrFotoOpcProductos = [];
$scope.findFotoOpcProductos = function()
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../findFotoOpcProductos",
        data:
        {

        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.arrFotoOpcProductos = resultado;
            $scope.cargando = false;
            $scope.$evalAsync();
        }
    });
}


//AJAX SAVE
$scope.guardarFotoOpcProducto = function()
{
    guardo = false;
    $scope.cargando = false;
    
    $.ajax(
    {
        url:"../../guardarFotoOpcProducto",
        async: false,
        data:
        {
            "strFotoOpcProducto": JSON.stringify($scope.fotoOpcProducto)
        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            console.log("res WS -> guardarFotoOpcProducto : " + resultado);
            $scope.cargando = false;
            guardo = resultado;
            
            if(guardo)
            {
                $scope.snack("Guardado con exito!");
                $scope.findFotoOpcProductos();
            }
            $scope.$evalAsync();
        }

    });
    
    return guardo;
}
//AJAX GET
$scope.fotoOpcProducto = {};
$scope.getFotoOpcProducto = function(idFotoOpcProducto)
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getFotoOpcProducto",
       async: false,
       data:
       {
            "idFotoOpcProducto":idFotoOpcProducto
       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.fotoOpcProducto = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}

//AJAX GET - EMPTY
$scope.fotoOpcProductoEmpty = {};
$scope.getFotoOpcProductoEmpty = function()
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getFotoOpcProductoEmpty",
       async: false,
       data:
       {

       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.fotoOpcProductoEmpty = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}
//VALIDAR
$scope.validarFotoOpcProducto = function()
{
   $scope.todoOK = false;
   $scope.comprobarFoto = $scope.fotoOpcProducto.foto != null && $scope.fotoOpcProducto.foto.length > 1;
   if($scope.comprobarFoto)
   {
       $scope.fotoOpcProducto.foto = $scope.pasaraMayusPrimerLetra($scope.fotoOpcProducto.foto);
       $scope.$evalAsync();
   }
   
   $scope.comprobarOpcionProducto = $scope.fotoOpcProducto.opcionProducto != null && $scope.fotoOpcProducto.opcionProducto.length > 1;
   if($scope.comprobarOpcionProducto)
   {
       $scope.fotoOpcProducto.opcionProducto = $scope.pasaraMayusPrimerLetra($scope.fotoOpcProducto.opcionProducto);
       $scope.$evalAsync();
   }
   
   $scope.comprobarVisible = $scope.fotoOpcProducto.visible != null && $scope.fotoOpcProducto.visible.length > 1;
   if($scope.comprobarVisible)
   {
       $scope.fotoOpcProducto.visible = $scope.pasaraMayusPrimerLetra($scope.fotoOpcProducto.visible);
       $scope.$evalAsync();
   }
   
   //COMPROBACION FINAL
   if( $scope.comprobarFoto && $scope.comprobarOpcionProducto && $scope.comprobarVisible )
   {
       console.log("TODO VALIDADO");
       $scope.todoOK = true;
       $scope.$evalAsync();
   }
		
}
