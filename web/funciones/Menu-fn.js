$scope.arrMenus = [];
$scope.findMenus = function()
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../findMenus",
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.arrMenus = resultado;
            $scope.cargando = false;
            
            $scope.pintarDondeEstoy();
            $scope.generarCodigoSesion();
//            $scope.damePersonaLogeada();
            
            $scope.$evalAsync();
        }
    });
}
$scope.generarCodigoSesion = function()
{
    $scope.cargandoSesion = false;
    $.ajax(
    {
        url:"../../generarCodigoSesion",
        beforeSend: function (xhr) 
        {
            $scope.cargandoSesion = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.codigoSesion = resultado;
            $scope.cargandoSesion = false;
            
            if(resultado != null)
            {
                $scope.dameCarritoSegunCodigoDeSesion();
            }
            
            $scope.$evalAsync();
        }
    });
}
$scope.dameCarritoSegunCodigoDeSesion = function()
{
    $scope.cargandoMicarrito = false;
    $.ajax(
    {
        url:"../../dameCarritoSegunCodigoDeSesion",
        beforeSend: function (xhr) 
        {
            $scope.cargandoMicarrito = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.miCarrito = resultado;
            $scope.cargandoMicarrito = false;
            
            $scope.$evalAsync();
        }
    });
}
$scope.pintarDondeEstoy = function()
{
    url = window.location.href;

    console.log("estoy en: " + url);
    
    for(i = 0 ; i < $scope.arrMenus.length; i++ )
    {
        actual = $scope.arrMenus[i];
        urlLoop = actual.url.substring(2,actual.url.length);
//        console.log("urlLoop = " + urlLoop);
//        console.log("url = " + url);
//        console.log("---");
        
        if(url.includes(urlLoop))
        {
            actual.selected = true;
            break;
        }
    }
    
    $scope.$evalAsync();
}

//AJAX SAVE
$scope.guardarMenu = function()
{
    guardo = false;
    $scope.cargando = false;
    
    $.ajax(
    {
        url:"../../guardarMenu",
        async: false,
        data:
        {
            "strMenu": JSON.stringify($scope.menu)
        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            console.log("res WS -> guardarMenu : " + resultado);
            $scope.cargando = false;
            guardo = resultado;
            
            if(guardo)
            {
                $scope.snack("Guardado con exito!");
                $scope.findMenus();
            }
            $scope.$evalAsync();
        }

    });
    
    return guardo;
}
//AJAX GET
$scope.menu = {};
$scope.getMenu = function(idMenu)
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getMenu",
       async: false,
       data:
       {
            "idMenu":idMenu
       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.menu = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}

//AJAX GET - EMPTY
$scope.menuEmpty = {};
$scope.getMenuEmpty = function()
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getMenuEmpty",
       async: false,
       data:
       {

       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.menuEmpty = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}
//VALIDAR
$scope.validarMenu = function()
{
   $scope.todoOK = false;
   $scope.comprobarNombre = $scope.menu.nombre != null && $scope.menu.nombre.length > 1;
   if($scope.comprobarNombre)
   {
       $scope.menu.nombre = $scope.pasaraMayusPrimerLetra($scope.menu.nombre);
       $scope.$evalAsync();
   }
   
   $scope.comprobarUrl = $scope.menu.url != null && $scope.menu.url.length > 1;
   if($scope.comprobarUrl)
   {
       $scope.menu.url = $scope.pasaraMayusPrimerLetra($scope.menu.url);
       $scope.$evalAsync();
   }
   
   //COMPROBACION FINAL
   if( $scope.comprobarNombre && $scope.comprobarUrl )
   {
       console.log("TODO VALIDADO");
       $scope.todoOK = true;
       $scope.$evalAsync();
   }
		
}
