$scope.arrEstadoPedidoDisponibles = [];
$scope.findEstadoPedidoDisponibles = function()
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../findEstadoPedidoDisponibles",
        data:
        {

        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.arrEstadoPedidoDisponibles = resultado;
            $scope.cargando = false;
            $scope.$evalAsync();
        }
    });
}


//AJAX SAVE
$scope.guardarEstadoPedidoDisponible = function()
{
    guardo = false;
    $scope.cargando = false;
    
    $.ajax(
    {
        url:"../../guardarEstadoPedidoDisponible",
        async: false,
        data:
        {
            "strEstadoPedidoDisponible": JSON.stringify($scope.estadoPedidoDisponible)
        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            console.log("res WS -> guardarEstadoPedidoDisponible : " + resultado);
            $scope.cargando = false;
            guardo = resultado;
            
            if(guardo)
            {
                $scope.snack("Guardado con exito!");
                $scope.findEstadoPedidoDisponibles();
            }
            $scope.$evalAsync();
        }

    });
    
    return guardo;
}
//AJAX GET
$scope.estadoPedidoDisponible = {};
$scope.getEstadoPedidoDisponible = function(idEstadoPedidoDisponible)
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getEstadoPedidoDisponible",
       async: false,
       data:
       {
            "idEstadoPedidoDisponible":idEstadoPedidoDisponible
       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.estadoPedidoDisponible = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}

//AJAX GET - EMPTY
$scope.estadoPedidoDisponibleEmpty = {};
$scope.getEstadoPedidoDisponibleEmpty = function()
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getEstadoPedidoDisponibleEmpty",
       async: false,
       data:
       {

       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.estadoPedidoDisponibleEmpty = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}
//VALIDAR
$scope.validarEstadoPedidoDisponible = function()
{
   $scope.todoOK = false;
   $scope.comprobarNombr = $scope.estadoPedidoDisponible.nombr != null && $scope.estadoPedidoDisponible.nombr.length > 1;
   if($scope.comprobarNombr)
   {
       $scope.estadoPedidoDisponible.nombr = $scope.pasaraMayusPrimerLetra($scope.estadoPedidoDisponible.nombr);
       $scope.$evalAsync();
   }
   
   //COMPROBACION FINAL
   if( $scope.comprobarNombr )
   {
       console.log("TODO VALIDADO");
       $scope.todoOK = true;
       $scope.$evalAsync();
   }
		
}
