$scope.arrPedidos = [];
$scope.findPedidos = function()
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../findPedidos",
        data:
        {

        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.arrPedidos = resultado;
            $scope.cargando = false;
            $scope.$evalAsync();
        }
    });
}
$scope.agregarOpcionProductoMiCarrito = function(opcionProductoSeleccionada , cantidadLlevo , seLaAumento)
{
    console.log("agregarOpcionProductoMiCarrito: " + opcionProductoSeleccionada.id + " | " +  cantidadLlevo );
    $.ajax(
    {
        url:"../../agregarOpcionProductoMiCarrito",
        data:
        {
            "idOpcionProducto":opcionProductoSeleccionada.id,
            "cantidad":cantidadLlevo,
            "seLaAumento":seLaAumento
        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.miCarrito = resultado;
            
            if(resultado != null)
            {
                $("#modal-producto").modal('hide');
                $scope.snack("Producto agregado al carrito!")
            }
            
            $scope.cargando = false;
            $scope.$evalAsync();
        }
    });
}