$scope.limitacionConEllipsisInicial = 150;
$scope.limitacionConEllipsisMaxima = 9999;
$scope.limitacionConEllipsis = $scope.limitacionConEllipsisInicial;
$scope.fotoCollapsible = 'http://viewdevscompany.com:8081/upload/noncello/backgrounds/Banner9.jpg';
$scope.h1Collapsible = "Noncello";
$scope.h2Collapsible ="Fabricamos el Chocolate mas rico!";
$scope.linkCollapsible ="../home/home.jsp";
$scope.quieroBarraLateral = false;
$scope.urlBG = "http://viewdevscompany.com:8081/upload/noncello/bg-app.jpg";

$scope.verMas = function()
{
    if($scope.limitacionConEllipsis == $scope.limitacionConEllipsisInicial)
    {
        $scope.limitacionConEllipsis = $scope.limitacionConEllipsisMaxima;
    }
    else
    {
        $scope.limitacionConEllipsis = $scope.limitacionConEllipsisInicial;
    }
    $scope.$evalAsync();
}

$scope.damePersonaLogeada = function()
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../damePersonaLogeada",
        beforeSend: function (xhr) 
        {
            $scope.personaLogeada = {"id": -1,"nombre": null,"apellido": null,"email": null};
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.personaLogeada = resultado;
            $scope.cargando = false;
            $scope.damePedidoEstoyHaciendo();
            $scope.$evalAsync();
        }
    });
}
$scope.logearPersona = function()
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../logearPersona",
        data:
        {
            "email":$scope.emailLogin,
            "pass":$scope.passLogin
        },
        beforeSend: function (xhr) 
        {
            $scope.cargandoLogin = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.cargandoLogin = false;
            $scope.$evalAsync();
            $scope.damePersonaLogeada();
            $("#modal-login").modal('hide');
        }
    });
}
$scope.exit = function()
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../exit",
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.damePersonaLogeada();
        }
    });
}
$scope.abrirModalLogin = function()
{
    $("#modal-producto").modal('hide');
    $("#modal-login").modal();
}
$scope.personaEmpty = {};
$scope.getPersonaEmpty = function()
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getPersonaEmpty",
       async: false,
       data:
       {

       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.persona = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}
$scope.abrirModalAltaCliente = function()
{
    $scope.getPersonaEmpty();
    $("#modal-login").modal('hide');
    $("#modal-alta-cliente").modal();
}
$scope.altaCliente = function()
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../altaCliente",
        type: 'POST',
        data:
        {
            "nombre":$scope.persona.nombre,
            "apellido":$scope.persona.apellido,
            "email":$scope.persona.email,
            "pass":$scope.persona.pass,
            "telefono":$scope.persona.telefono,
            "hotel":$scope.persona.hotel,
            "calle":$scope.persona.calle,
            "numero":$scope.persona.numero,
            "piso":$scope.persona.piso,
            "habitacion":$scope.persona.habitacion,
            "observacion":$scope.persona.observacion
        },
        beforeSend: function (xhr) 
        {
            $scope.cargandoLogin = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.cargandoLogin = false;
            $scope.$evalAsync();
            $scope.damePersonaLogeada();
            $("#modal-alta-cliente").modal('hide');
        }
    });
}
//$scope.dameBg = function(bg)
//{
//    salida = "";
//    $.ajax(
//    {
//        url:"../../dameBg",
//        async: false,
//        data:
//        {
//            "bg":bg
//        },
//        success: function (resultado, textStatus, jqXHR) 
//        {
//            salida = resultado;
//            $scope.$evalAsync();
//        }
//    });
//    
//    return salida;
//}
$scope.clickLupaBuscar = function()
{
    if($scope.quieroBuscar)
    {
        $scope.quieroBuscar = false;
    }
    else
    {
        $scope.quieroBuscar = true;
    }
    $scope.$evalAsync();
}
$scope.clickHamb = function()
{
    if($scope.quieroBarraLateral)
    {
        $scope.quieroBarraLateral = false;
    }
    else
    {
        $scope.quieroBarraLateral = true;
    }
    $scope.$evalAsync();
}
$scope.clickCarrito = function()
{
    window.location.href = "../carrito/carrito.jsp";
}

$scope.dameParametroGet = function(parametro)
{
    var strUrl = window.location.href;
    var url = new URL(strUrl);
    var valor = url.searchParams.get(parametro);
    
    return valor;
}


$scope.irA = function(url)
{
    window.location.href = url;
}
$scope.irEspecial = function(url)
{
    if(url.includes("home"))
    {
        $scope.irA(url);
    }
    else
    {
        $scope.irNew(url);
    }
}
$scope.irNew = function(url)
{
    window.open(url, '_blank');
}
$scope.back = function(url)
{
    window.history.back();
}
$scope.abrirCategoriaDeProductos = function(idHash)
{
    console.log("ABRIENDO CATEGORIA DE PRODUCTOS: "+ idHash);
    url = "../productos/productos.jsp?fkHash=" + idHash;
    
    $scope.irA(url);
}
$scope.snack = function(msg)
{
    // Get the snackbar DIV
    var x = document.getElementById("snackbar");

    $("#snackbar").html(msg)
    //x.html(msg);

    // Add the "show" class to DIV
    x.className = "show";

    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}



//LISTENER DE TAMAÑO PANTALLA
$(document).ready(function()
{
    // 1 - TOOL TIPS:
//    $('[data-toggle="tooltip"]').tooltip();
//
//     // 2 - LISTENER DE SCROLL:
//    $('body').scroll(function(event) 
//    {
//        var altura = document.body.scrollTop;
//        var alturaPar2 = $("#paralax1").position().top; 
//        //console.log("SCROLL " + altura + " - " + alturaPar2);
//        $scope.nivelScroll = altura;
//
//        $scope.pintarAreaSegunScroll();
//
//        $scope.$evalAsync();
//    });

    // 3 - LISTENER DE RESIZE EVENT:
    $scope.lg = true;
    $scope.md = false;
    $scope.sm = false;
    $scope.xs = false;

    $(window).on('resize', function()
    {
        var win = $(this); //this = window
        ancho = win.innerWidth();
        if (ancho > 1200) 
        {
            $scope.lg = true;
            $scope.md = false;
            $scope.sm = false;
            $scope.xs = false;
            
            $scope.quieroBarraLateral = true;
            $scope.$evalAsync();
        }
        else if(ancho < 1200 && ancho > 992)
        {
            $scope.lg = false;
            $scope.md = true;
            $scope.sm = false;
            $scope.xs = false;
            $scope.quieroBarraLateral = false;
        }
        else if(ancho < 992 && ancho > 768)
        {
            $scope.lg = false;
            $scope.md = false;
            $scope.sm = true;
            $scope.xs = false;
            
            $scope.quieroBarraLateral = false;
        }
        else if ( ancho < 768)
        {
            $scope.lg = false;
            $scope.md = false;
            $scope.sm = false;
            $scope.xs = true;
            
            $scope.quieroBarraLateral = false;

        }
        $scope.$evalAsync();
        console.log("resize event: ("+ancho+") LG:" + $scope.lg +"| md:" + $scope.md + "| sm:" + $scope.sm + "| xs:" + $scope.xs );
    });


    // 4 - FORCE ON-RESIZE EVENT - EN EL START:
    window.dispatchEvent(new Event('resize'));
});

$scope.mostrarFormularioAltaFull = function()
{
    if($scope.fullAlta)
    {
        $scope.fullAlta = false;
    }
    else
    {
        $scope.fullAlta = true;
    }
    
    $scope.$evalAsync();
}
