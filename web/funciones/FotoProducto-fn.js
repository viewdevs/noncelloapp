$scope.arrFotoProductos = [];
$scope.findFotoProductos = function()
{
    $scope.cargando = false;
    $.ajax(
    {
        url:"../../findFotoProductos",
        data:
        {

        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            $scope.arrFotoProductos = resultado;
            $scope.cargando = false;
            $scope.$evalAsync();
        }
    });
}


//AJAX SAVE
$scope.guardarFotoProducto = function()
{
    guardo = false;
    $scope.cargando = false;
    
    $.ajax(
    {
        url:"../../guardarFotoProducto",
        async: false,
        data:
        {
            "strFotoProducto": JSON.stringify($scope.fotoProducto)
        },
        beforeSend: function (xhr) 
        {
            $scope.cargando = true;
        },
        success: function (resultado, textStatus, jqXHR) 
        {
            console.log("res WS -> guardarFotoProducto : " + resultado);
            $scope.cargando = false;
            guardo = resultado;
            
            if(guardo)
            {
                $scope.snack("Guardado con exito!");
                $scope.findFotoProductos();
            }
            $scope.$evalAsync();
        }

    });
    
    return guardo;
}
//AJAX GET
$scope.fotoProducto = {};
$scope.getFotoProducto = function(idFotoProducto)
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getFotoProducto",
       async: false,
       data:
       {
            "idFotoProducto":idFotoProducto
       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.fotoProducto = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}

//AJAX GET - EMPTY
$scope.fotoProductoEmpty = {};
$scope.getFotoProductoEmpty = function()
{
   $scope.cargando = false;
   $.ajax(
   {
       url:"../../getFotoProductoEmpty",
       async: false,
       data:
       {

       },
       beforeSend: function (xhr) 
       {
           $scope.cargando = true;
       },
       success: function (resultado, textStatus, jqXHR) 
       {
           $scope.fotoProductoEmpty = resultado;
           $scope.cargando = false;
           $scope.$evalAsync();
       }
   });
}
//VALIDAR
$scope.validarFotoProducto = function()
{
   $scope.todoOK = false;
   $scope.comprobarFoto = $scope.fotoProducto.foto != null && $scope.fotoProducto.foto.length > 1;
   if($scope.comprobarFoto)
   {
       $scope.fotoProducto.foto = $scope.pasaraMayusPrimerLetra($scope.fotoProducto.foto);
       $scope.$evalAsync();
   }
   
   $scope.comprobarProducto = $scope.fotoProducto.producto != null && $scope.fotoProducto.producto.length > 1;
   if($scope.comprobarProducto)
   {
       $scope.fotoProducto.producto = $scope.pasaraMayusPrimerLetra($scope.fotoProducto.producto);
       $scope.$evalAsync();
   }
   
   $scope.comprobarVisible = $scope.fotoProducto.visible != null && $scope.fotoProducto.visible.length > 1;
   if($scope.comprobarVisible)
   {
       $scope.fotoProducto.visible = $scope.pasaraMayusPrimerLetra($scope.fotoProducto.visible);
       $scope.$evalAsync();
   }
   
   //COMPROBACION FINAL
   if( $scope.comprobarFoto && $scope.comprobarProducto && $scope.comprobarVisible )
   {
       console.log("TODO VALIDADO");
       $scope.todoOK = true;
       $scope.$evalAsync();
   }
		
}
