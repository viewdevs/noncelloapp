<div class="col-xs-12 sin-padding">
    
    <div class="form-group">
        <label>Nombre</label>
        <input type="text" class="form-control" placeholder="Juan..">
    </div>
    <div class="form-group">
        <label>Telefono</label>
        <input type="tel" class="form-control" placeholder="+54 294 154 54321">
    </div>
    
    <div class="form-group">
        <label>Consulta</label>
        <textarea class="form-control" rows="5" id="comment" placeholder="Queria consultar por envios al Hotel.."></textarea>
    </div>
    
    
    <button class="btn btn-default col-xs-12 sin-padding">
        <span class="glyphicon glyphicon-send"></span>
        Enviar Mensaje
    </button>
</div>