<div class="banner-categoria col-xs-12 sin-padding">
            
    <div class='foto-banner-categoria col-xs-12 sin-padding foto' style="background-image: url('{{hashtag.fullFoto}}')" >
        <h3 class='h1-banner-categoria col-xs-12'>{{hashtag.hash}}</h3>
        <h3 class='h2-banner-categoria'>{{hashtag.subTexto}}</h3>
    </div>
</div>

<h3 class='h-separador col-xs-12'>
    <button class='btn btn-default' style='position: absolute; left: 12px;top:8px;' ng-click="back()">
        <span class='glyphicon glyphicon-chevron-left'></span>
    </button>
    Productos ({{arrProductos.length}})
</h3>

<style>
    .banner-categoria
    {
        height: 250px;
        background-size: cover;
        
        background-position:  center center;
        background-repeat: no-repeat;
        margin-top: 25px;
        margin-bottom: 0px;
        cursor: pointer;
    }
    .foto-banner-categoria
    {
        height: 250px;
       
    }
    .h1-banner-categoria
    {
        color: white;
        font-size: 32px;
        text-shadow: 2px 2px #000;
        font-family: 'Permanent Marker', cursive;
        margin-top: 100px;
    }
    .h2-banner-categoria
    {
        color: white;
        text-shadow: 2px 2px #000;
        font-family: 'Permanent Marker', cursive;
        
    }
    </style>