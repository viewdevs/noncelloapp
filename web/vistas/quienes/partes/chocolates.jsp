<div class="col-xs-12 partes-nosotros">
    <h3 class="h-nosotros">
        CHOCOLATES
    </h3>
    <h5>
        PRODUCTO DE ALTA GAMA
    </h5>
    <p>
        El chocolates es nuestro mejor ingrediente a la hora de presentar nuestra empresa, en cuanto a CALIDAD, SABOR Y PRESENTACION
        ya que nuestras tabletas rellenas demuestran distincion con sus TRANSFERS de DIVERTIDOS Y ELEGANTES COLORES Y DIBUJOS, elaborados con pigmentos naturales
        , no con colorantes artificiales, con sus rellenos de amplia duracion y excelentisimos sabores, algunos combinados con la experiencia de CHEFS
        internacionales y maestros chocolateros tanto de BELGICA como de FRANCIA, los cuales estuvieron en nuestras plantas, asesorandonos e innovando con productos
        EXTRAORDINARIOS, dejando nuestros productos a niveles INTERNACIONALES.
    </p>
    
</div>