<!-- MODAL : -->
<div id="modal-promos" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content col-xs-12">
            
            <!-- HEADER MODAL: -->
            <div class="modal-header row">
                
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    <button type="button" data-dismiss="modal" class='btn btn-default'>
                        <span class='glyphicon glyphicon-chevron-left'></span>
                    </button>
                    
                    <span style='text-align: center;'>
                        {{productoSeleccionado.nombre}} - 
                        {{opcionProductoSeleccionada.nombre}}
                        
                        (Opcion {{opcionProductoSeleccionada.id}})
                    </span>
                </h4>
            </div>
            
            <!-- CUERPO - MODAL: -->
            <div class="modal-body col-xs-12 sin-padding">
                
                <!-- CARRUSEL DE FOTOS:-->
                <%@include file="partes/carrusel-modal.jsp" %>
                    
                <!-- ENCABEZADO PRODUCTO:-->
                <div class='contenedor-encabezados-modal-productos col-xs-12 sin-padding'>
                    
                    <div class='col-xs-12 sin-padding' style='margin-bottom: 12px; margin-top: 12px; '>
                        <div class='col-xs-8 sin-padding'>
                            <h3 class='h1-modal-productos'>{{opcionProductoSeleccionada.nombre}}</h3>
                        </div>
                        <div class="col-xs-4 sin-padding">
                            <h3 class='h-precio-producto' style='text-align: center; font-weight: bolder;'>
                                {{"$" + opcionProductoSeleccionada.precio}}
                            </h3>
                        </div>
                    </div>
                    <h4 class='h2-modal-productos' >{{opcionProductoSeleccionada.descripcion | limitWithEllipsis: limitacionConEllipsis}}</h4>
                    
                    <button class='btn-ver-mas btn btn-default center-block' ng-click='verMas()' ng-show="opcionProductoSeleccionada.descripcion.length  > limitacionConEllipsisInicial">
                        
                        <div class="btn-ver-mas" ng-show='opcionProductoSeleccionada.descripcion.length  > limitacionConEllipsis'>
                            <span class='glyphicon glyphicon-chevron-down'></span>
                            Ver mas
                        </div>
                        
                        <div class="btn-ver-mas" ng-hide='opcionProductoSeleccionada.descripcion.length  > limitacionConEllipsis'>
                            <span class='glyphicon glyphicon-chevron-up' ></span>
                            Ver menos
                        </div>
                        
                    </button>
                </div>
                
            </div>
            
            <!-- PRE - FOOTER - MODAL: -->
            <div class="modal-footer col-xs-12 sin-padding">
                
                <!-- COMBO DE OPCIONES PRODUCTOS: -->
                <select id="combo-opciones" class="form-control" ng-model="opcionProductoSeleccionada"
                        ng-options="opcion as opcion.nombre for opcion in productoSeleccionado.opcionesList track by opcion.nombre" >
                </select>

                
            </div>
            <!-- FOOTER - MODAL: -->
            <div class="modal-footer col-xs-12 sin-padding">
                
                <button type="button" class="btn-confirmar-modal btns-modal  col-xs-12 sin-padding">
                    <span class="glyphicon glyphicon-ok verde"></span>
                    Confirmar
                </button>
                
                <button type="button" class="btn-rechazar-modal btns-modal col-xs-12 sin-padding" data-dismiss="modal">
                    <span class="glyphicon glyphicon-remove rojo"></span>
                    Cancelar
                </button>

                
            </div>
        </div>
    </div>
</div>
<style>
    .h1-modal-productos
    {
        font-size: 36px;
        color: black;
        /*border: solid 1px red;*/
    }
    .h2-modal-productos
    {
       font-size: 14px;
       color: grey;
       font-family: 'Montserrat', sans-serif;
    }
</style>