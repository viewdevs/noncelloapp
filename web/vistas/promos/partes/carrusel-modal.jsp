<div id="carrousel-modal-productos" class="carousel slide" data-ride="carousel" >
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="{{$index}}"  ng-repeat='foto in opcionProductoSeleccionada.fotosOpcProductosList' ng-class="{'true':'active'}[$index == 0]"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item" ng-repeat='foto in opcionProductoSeleccionada.fotosOpcProductosList' ng-class="{'true':'active'}[$index == 0]" >
        <img class='img center-block'  style='width: 100%;;' ng-src="{{foto.foto.urlFull}}" alt="Los Angeles">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#carrousel-modal-productos" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carrousel-modal-productos" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
