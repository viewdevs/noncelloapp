<div class="banner col-xs-12">
    
    <!-- HAMB : -->
    <div class="col-xs-3 b-rojo" style="text-align: left;">
        <button class="btn btn-default btn-hamb" ng-click="clickHamb()">
            <div class="glyphicon glyphicon-menu-hamburger" ng-hide="quieroBarraLateral"></div>
            <div class="glyphicon glyphicon-chevron-left" ng-show="quieroBarraLateral"></div>
        </button> 
    </div>
    
    
    <!-- BANNER MID : -->
    <div class="col-xs-6 b-rojo center-block" style="text-align: center;padding-top:2px;padding-bottom: 2px;">
        
        <div class='img-logo-banner center-block' ng-click='irA("../home/home.jsp")' 
             style='background-image: url("http://viewdevscompany.com:8081/upload/noncello/logo-noncello.png")'
             style="padding-top:0px;">
                
        </div>
<!--        <h3 class="h-mid-banner" ng-hide="quieroBuscar">
            Noncello
        </h3>-->
        <input type="search" class="form-control" ng-show="quieroBuscar" placeholder="Busqueda..">
        
    </div>
    
    <!-- BANNER DER : -->
    <div class="col-xs-3 center-block b-rojo" style="text-align: right;">
        
        <!-- CARRITO : -->
        <div class="btn btn-default btn-hamb" ng-click="clickCarrito()" >
            <div class="glyphicon glyphicon-shopping-cart"></div>
        </div> 
        
        <!-- LUPA : -->
        <div class="btn btn-default btn-hamb" ng-click="clickLupaBuscar()" style="display: none;">
            <div class="glyphicon glyphicon-search"></div>
        </div> 
        
    </div>
    
</div>

<div class='responsive-data col-xs-1' hidden>
    XS:{{xs}}
    <br>
    SM:{{sm}}
    <br>
    md:{{md}}
    <br>
    lg:{{lg}}
</div>

<style>
    .img-logo-banner
    {
        cursor: pointer;
        height: 100%;;
        background-size: contain;
        background-repeat: no-repeat;
        background-position: center center;
        /*border: solid 1px red;*/
    }
    .responsive-data
    {
        position: fixed;
        right: 50px;
        top: 50px;
        border: solid 1px red;
        z-index: 2;
        text-align: center;
        background-color: white;
    }
    .banner
    {
/*        box-shadow: 1px 1px 25px black;
        z-index: 1;*/
        background-color: white;
        border-bottom: solid 1px lightgrey;
        height: 50px;
        position: fixed;
        top:0px;
        left:0px;
        right: 0px;
        z-index: 2;
    }
    .b-rojo
    {
        height: 50px;
        padding-top: 8px;
    }
    .btn-hamb
    {
        /*border: solid 1px lightgrey;*/
    }
    .h-mid-banner
    {
        margin: 0px;
        padding-top: 6px;
        font-size: 24px;
        font-family: 'Montserrat', sans-serif;
        /*font-family: 'misses';*/
    }
</style>