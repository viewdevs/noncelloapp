<!-- MODAL : -->
<div id="modal-alta-cliente" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content col-xs-12">
            
            <!-- HEADER MODAL: -->
            <div class="modal-header row">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Darme de Alta</h4>
            </div>
            
            <!-- CUERPO - MODAL: -->
            <div class="modal-body row">
                <div class="form-group" ng-hide="cargandoLogin">
                    <label>Nombre(*)</label>
                    <input type="text" ng-model="persona.nombre" class="form-control" placeholder="Juan">
                </div>
                <div class="form-group" ng-hide="cargandoLogin">
                    <label>Apellido(*)</label>
                    <input type="text" ng-model="persona.apellido" class="form-control" placeholder="Perez">
                </div>
                <div class="form-group" ng-hide="cargandoLogin">
                    <label>Email(*)</label>
                    <input type="email" ng-model="persona.email" class="form-control" placeholder="usuario@email.com">
                </div>
                <div class="form-group"  ng-hide="cargandoLogin">
                    <label>Clave(*)</label>
                    <input type="password" ng-model="persona.pass" class="form-control" >
                </div>
                <div class="form-group"  ng-hide="cargandoLogin">
                    <label>Telefono(*)</label>
                    <input type="text" ng-model="persona.telefono" class="form-control" placeholder="+54 294 154 530641" >
                </div>
                
                <div class="col-xs-12 sin-padding">
                    
                    <a href="#" ng-click="mostrarFormularioAltaFull()">
                        <span ng-hide="fullAlta">
                            <span class="glyphicon glyphicon-chevron-down"></span>
                            Cargar direccion de entrega
                        </span>
                        <span ng-show="fullAlta">
                            <span class="glyphicon glyphicon-chevron-up"></span>
                            Omitir direccion de entrega
                        </span>
                    </a>
                    <hr ng-show="fullAlta">
                </div>
                <div class="campos-no-obligatorios" ng-show="fullAlta">
                    
                    <h3 class="h">Datos para Entrega</h3>
                    <hr>
                    
                    <!--CALLE NUMERO-->
                    <div class='col-xs-12 sin-padding'>

                        <div class="form-group col-xs-6 sin-padding-left"  ng-hide="cargandoLogin">
                            <label>Calle(Opcional)</label>
                            <input type="text" ng-model="persona.calle" class="form-control" placeholder="Moreno" >
                        </div>
                        <div class="form-group col-xs-6 sin-padding-right"  ng-hide="cargandoLogin">
                            <label>Numero(Opcional)</label>
                            <input type="text" ng-model="persona.numero" class="form-control" placeholder="240" >
                        </div>
                    </div>
                    
                    <div class="form-group"  ng-hide="cargandoLogin">
                        <label>Hotel(Opcional)</label>
                        <input type="text" ng-model="persona.hotel" class="form-control" placeholder="Hotel .." >
                    </div>

                    <!--DATOS HOTEL:-->
                    <div class='col-xs-12 sin-padding'>

                        <div class="form-group col-xs-6 sin-padding-left"  ng-hide="cargandoLogin">
                            <label>Piso(Opcional)</label>
                            <input type="text" ng-model="persona.piso" class="form-control" placeholder="Hotel .." >
                        </div>
                        <div class="form-group col-xs-6 sin-padding-right"  ng-hide="cargandoLogin">
                            <label>Habitacion(Opcional)</label>
                            <input type="text" ng-model="persona.habitacion" class="form-control" placeholder="Hotel .." >
                        </div>
                    </div>
                    
                    <div class="form-group"  ng-hide="cargandoLogin">
                        <label>Aclaracion (Opcional)</label>
                        <textarea class="form-control" ng-model="persona.observacion" rows="5" id="comment"></textarea>
                    </div>
                </div>
                
                <!--IMG LOADING:-->
                <div class=" col-xs-12 sin-padding" ng-show="cargandoLogin">
                    <div class="foto img-loading img img" style="background-image: url('http://barivende.com:8081/upload/noncello/backgrounds/loading.gif')">
                    </div>
                </div>
            </div>
            
            <!-- FOOTER - MODAL: -->
            <div class="modal-footer col-xs-12 sin-padding">
                
                <button type="button" class="btn-confirmar-modal btns-modal  col-xs-12 sin-padding" ng-click="altaCliente()" ng-disabled="cargandoLogin">
                    <span class="glyphicon glyphicon-ok verde"></span>
                    Ingresar
                </button>
                
                <button type="button" class="btn-rechazar-modal btns-modal col-xs-12 sin-padding" data-dismiss="modal">
                    <span class="glyphicon glyphicon-remove rojo"></span>
                    Cancelar
                </button>

                
            </div>
        </div>
    </div>
</div>
<style>
    .img-loading
    {
        width: 100%;
        height: 250px;
    }
</style>