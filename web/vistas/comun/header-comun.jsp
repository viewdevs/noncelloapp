<%@include file="../comun/banner-tipo-movil.jsp" %>
<%@include file="../comun/barraLateral.jsp" %>

<div class="contenedor-pantalla-grande col-xs-12 col-xs-offset-0 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 sin-padding" >
    
    <div class="colapsible col-xs-12 sin-padding" ng-click="irEspecial(linkCollapsible)" style="background-image: url('{{fotoCollapsible}}')">
        <h3 class="h-principal-collapsible">{{h1Collapsible}}</h3>
        <h4 class="h-secundario-collapsible">{{h2Collapsible}}</h3>
    </div>
    
    <!--MODAL LOGIN:-->
    <%@include file="../comun/modal-login.jsp" %>
    <%@include file="../comun/modal-alta-cliente.jsp" %>
    
    <div class="contenido col-xs-12 sin-padding">


<style>
    body
    {
        background-color: #f3f3f3;
        background-image:url('http://viewdevscompany.com:8081/upload/noncello/backgrounds/background.png');
/*        background-image:url('http://viewdevscompany.com:8081/upload/noncello/backgrounds/background.jpg');
        background-repeat: no-repeat;
        background-size: contain;*/
    }
    .colapsible
    {
        margin-top: 50px;
        /*height: 185px;*/
        height: 225px;
        /*background-image: url('http://viewdevscompany.com:8081/upload/noncello/noncello-collapse.jpg');*/
        background-repeat: no-repeat;
        /*background-position: center 70%;*/
        background-position: center 60%;
        background-size: cover;
    }
    .contenido
    {
        background-color: white;
        margin-bottom: 50px;
        padding-bottom: 25px;
        border: solid 1px lightgrey;
    }
    .h-principal-collapsible
    {
        text-align: center;
        color: white;
        margin-top: 60px;
        font-size: 38px;
        /*font-family: 'misses';*/
        font-family: 'Montserrat', sans-serif;
        text-shadow: 2px 2px 2px black;
    }
    .h-secundario-collapsible
    {
        text-align: center;
        color: white;
        font-family: 'Montserrat', sans-serif;
        text-shadow: 2px 2px 2px black;
    }
    .contenedor-pantalla-grande
    {
        cursor: pointer;
    }
</style>
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>