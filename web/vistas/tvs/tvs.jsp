<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="windows-1252">
        <title>Noncello</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    </head>
    <style>
        body
        {
            background-image: url('http://viewdevscompany.com:8081/upload/noncello/bg-carrusel.png');
            background-position: center 20%;
            background-repeat: no-repeat;
        }
       
        #myCarousel 
        {
            height: 100%;
            width: 100%;
            border: solid 0px transparent;
        }
        .carousel-inner
        {
/*            position: absolute;
            top: 0px;
            bottom: 0px;
            left: 0px;
            right: 0px;
            width: 100%;*/
            height: 100%;
            /*border: solid 1px blue;*/
        }
        .item
        {
            /*border: solid 1px white;;*/
            height: 100%;
        }
        .img-cover
        {
            height: 100%;
            /*border: solid 1px yellow;*/
            background-position: center 20%;
            background-size: contain;
            background-repeat: no-repeat;
        }
        
    </style>
    <style>
        .banner
        {
            height: 50px;
            background-color: white;
        }
        .contenedor-carrusel
        {
            position: absolute;
            top: 100px;
            bottom: 50px;
            left: 0px;
            right: 0px;
            height: calc(100% - 150px);
            /*background-color: red;*/
        }
        .img-logo-banner
        {
            height: 50px;
            width: 150px;
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center center;
            /*border: solid 1px red;*/
        }
    </style>
    <body ng-app="app" ng-controller="ctrl" ng-init="init()" ng-cloack>
        
        <div class='banner col-xs-12 '>
            <!--<h3>Noncello</h3>-->
            <div class='img-logo-banner center-block' style='background-image: url("http://viewdevscompany.com:8081/upload/noncello/logo-noncello.png")'>
                
            </div>
        </div>
        
        <div class='contenedor-carrusel col-xs-10 col-xs-offset-1' >
            
            
            <!--CARROUSEL:--> 
           <div id="myCarousel" class="carousel slide" data-ride="carousel">

                <!--1 - BULLETS:--> 
<!--               <ol class="carousel-indicators" >
                   <li data-target="#myCarousel" data-slide-to="0" ng-class="{active:!$index}" ng-repeat="foto in arrFotos"></li>
               </ol>-->

                 <!--2 - IMGS-->  
                <div class="carousel-inner" >
                   <div class="item" ng-repeat="foto in arrFotos" ng-class="{active:!$index}"  >
                        <!--{{foto.urlFull}}-->
                        <div class="img-cover img center-block" style="background-image:url('{{foto.urlFull}}')">
                        </div>
                   </div>
               </div>

<!--               <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                   <span class="glyphicon glyphicon-chevron-left"></span>
                   <span class="sr-only">Previous</span>
               </a>
               <a class="right carousel-control" href="#myCarousel" data-slide="next">
                   <span class="glyphicon glyphicon-chevron-right"></span>
                   <span class="sr-only">Next</span>
               </a>-->
           </div>
        </div>
    </body>
    
    <script>
    app = angular.module('app', []);
    app.controller('ctrl', function($scope)
    {
//        $scope.arrFotos = 
//        [
//            {id: 2, urlFull: "http://viewdevscompany.com:8081/upload/noncello/noncello-logo-tv.png"},
//            {id: 1, urlFull: "http://viewdevscompany.com:8081/upload/noncello/backgrounds/1-1.jpg"}
//        ];
        $scope.init = function()
        {
            console.log("iniciando..");
            $scope.findFotos();
        }
        $scope.findFotos = function()
        {
            $.ajax(
            {
                url:"../../findFotos",
                data:
                {

                },
                beforeSend: function (xhr) 
                {
                    $scope.cargando = true;
                },
                success: function (resultado, textStatus, jqXHR) 
                {
                    $scope.arrFotos = resultado;
                    $scope.cargando = false;
                    $scope.$evalAsync();
                }

            });
        }
        
        $(document).ready(function()
        {
            $('#myCarousel').carousel(
            {
                interval: 2000,
                pause:true,
                cycle: true
            }); 
        });
        
        //http://viewdevscompany.com:8081/upload/noncello/backgrounds/
    });
    </script>
</html>
